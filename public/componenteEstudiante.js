

var postFixCode = "_ciudadmas";
new Vue({
    el: "#appestu",
    name: 'AutocompleteStatic',

    data: {

        inscripciones: 
        {
                nombre:'',
                apellido:'',
                doc_identidad:'',
                edad:'',
                num_telefono:'',
                ciudad:'',
                institucion_educatica:'',
                categoria:'',
                titulo_ponencia:'',
                sexo:'',
                alergia:'',
                housing:'',
                emergencia:'',
                usodatos:'',
                id_usuario_ingresado:'',
        },
        errorInscripcion:[],
        errorMensajeArray : [],
        isLoaded: false,


        selectedCountry: null,
        selectedEmployee: null,
        countries: [
          'Algeria',
          'Argentina',
          'Brazil',
          'Canada',
          'Italy',
          'Japan',
          'United Kingdom',
          'United States'
        ],
        employees: [
          'Jim Halpert',
          'Dwight Schrute',
          'Michael Scott',
          'Pam Beesly',
          'Angela Martin',
          'Kelly Kapoor',
          'Ryan Howard',
          'Kevin Malone',
          'Creed Bratton',
          'Oscar Nunez',
          'Toby Flenderson',
          'Stanley Hudson',
          'Meredith Palmer',
          'Phyllis Lapin-Vance'
        ]
  




    },
    methods: {
           
        registerInscripcionEstudiante()
        {       
            if(this.validarRegistro()){
                console.log('ook');
                return ; }
                this.isLoaded = true;


                    let me=this;
                axios.post('/inscripcionStudy/registerStudy', {
                  'nombre': this.inscripciones.nombre,
                  'apellido': this.inscripciones.apellido,
                  'doc_identidad': this.inscripciones.doc_identidad,
                  'edad': this.inscripciones.edad,
                  'num_telefono': this.inscripciones.num_telefono,
                  'ciudad': this.inscripciones.ciudad,
                  'institucion_educatica': this.inscripciones.institucion_educatica,
                  'categoria': this.inscripciones.categoria,
                  'titulo_ponencia': this.inscripciones.titulo_ponencia,
                  'sexo': this.inscripciones.sexo,
                  'alergia': this.inscripciones.alergia,
                  'housing': this.inscripciones.housing,
                  'emergencia': this.inscripciones.emergencia,
                  'usodatos': this.inscripciones.usodatos,
                  'id_usuario_ingresado' : this.inscripciones.id_usuario_ingresado
              })
              .then(function (response) {
                var respuesta=response.data;
                if(response.data.status=='ok'){
            //      me.listarurlVideo(); 
                  me.vaciarVariables();

                  Swal.fire(
                    'Exitoso?',
                    'Incripción guardado correctamente, revise su correo electronico',
                    'success'
                    )
            }

              }) 
              .catch(function (error) {
                  console.log(error);
              });
        },




        

        vaciarVariables()
        {
           
        this.inscripciones.nombre='';
        this.inscripciones.apellido='';
        this.inscripciones.doc_identidad='';
        this.inscripciones.edad='';
        this.inscripciones.num_telefono='';
        this.inscripciones.ciudad='';
        this.inscripciones.institucion_educatica='';
        this.inscripciones.categoria='';
        this.inscripciones.titulo_ponencia='';
        this.inscripciones.sexo='';
        this.inscripciones.alergia='';
        this.inscripciones.housing='';
        this.inscripciones.emergencia='';
        this.inscripciones.usodatos='';
        this.inscripciones.id_usuario_ingresado='';
        this.isLoaded = false;

        },


        validarRegistro(){
            this.errorInscripcion=0;
            this.errorMensajeArray=[];
            if(!this.inscripciones.nombre) this.errorMensajeArray.push("Ingrese nombre ");
            if(!this.inscripciones.apellido) this.errorMensajeArray.push("Ingrese apellido ");
            if(!this.inscripciones.doc_identidad) this.errorMensajeArray.push("Ingrese doc_identidad");
            if(!this.inscripciones.edad) this.errorMensajeArray.push("Ingrese edad");
            if(!this.inscripciones.num_telefono) this.errorMensajeArray.push("Ingrese num_telefono");
            if(!this.inscripciones.ciudad) this.errorMensajeArray.push("Ingrese ciudad");
            if(!this.inscripciones.institucion_educatica) this.errorMensajeArray.push("Ingrese institucion_educatica");
            if(!this.inscripciones.categoria) this.errorMensajeArray.push("Ingrese categoria");
           // if(!this.inscripciones.titulo_ponencia) this.errorMensajeArray.push("Ingrese titulo_ponencia");
            if(!this.inscripciones.sexo) this.errorMensajeArray.push("Ingrese sexo");
            if(!this.inscripciones.alergia) this.errorMensajeArray.push("Ingrese alergia");
            if(!this.inscripciones.housing) this.errorMensajeArray.push("Ingrese housing");
            if(!this.inscripciones.emergencia) this.errorMensajeArray.push("Ingrese emergencia");
            if(!this.inscripciones.usodatos) this.errorMensajeArray.push("Ingrese usodatos");


            if(this.errorMensajeArray.length) this.errorInscripcion=1;
            return this.errorInscripcion;
        },



        id_usuario(){
            let me=this;
             var url= '/inscripcionStudy/id_UserLogueado';
             axios.get(url).then(function (response) {
                var respuestaaa=response.data;
                me.inscripciones.id_usuario_ingresado= respuestaaa.id_userlogueado;           
               })
               .catch(function (error) {
                   console.log(error);
               });
         },


         


        
    },
    computed: {
        
    },
    watch: {
        
    },
    mounted() {
        this.id_usuario();
    },
});








