var postFixCode = "_ciudadmas";
new Vue({
    el: "#app",
    data: {
        name: '',
        email:'',
        phone:'',
        description:'',
        errorContact : 0,
        errorMensajeContacteArray : [],
        showimagenesarray:[],
        showurlarray:[],
        listBannerarray:[],



        enfoquee: '',
        imagen:'',

        enfoqueparticipar:'',
        imagenparticipar:'',

        enfoquepago:'',      
        imagenpago:'',

        
        inscripcionAsistente:'',
        inscripcionPonente:'',

    },
    methods: {
           
            registerContact(){
                    if(this.validateContact()){
                        return ;
                    }
                    let me=this;
                    axios.post('/contact/store', {
                      'name':  this.name,
                      'email': this.email,
                      'phone': this.phone,
                      'description': this.description
                  })
                  .then(function (response) {
                    var respuesta=response.data;
                    if(respuesta.status=='ok'){
                        me.clearVariable();
                        Swal.fire(
                        'Exitoso?',
                        'su mensaje se envio correctamente',
                        'success'
                        )
                }

                  }) 
                  .catch(function (error) {
                      console.log(error);
                  });
            },

            validateContact()
            {
                this.errorContact=0;
                 this.errorMensajeContacteArray=[];
                 if(!this.name) this.errorMensajeContacteArray.push("El campo nombre es obligatorio");
                 if(!this.email) this.errorMensajeContacteArray.push("El campo email es obligatorio");
                 if(!this.phone) this.errorMensajeContacteArray.push("El campo telefono es obligatorio");
                 if(!this.description) this.errorMensajeContacteArray.push("El campo descripción es obligatorio");

                 if(this.errorMensajeContacteArray.length) this.errorContact=1;
                 return this.errorContact;
            },

            clearVariable()
            {
               this.name= '';
                this.email='';
                this.phone='';
                this.description='';
            },




            listarurlVideo()
            {
            let me=this;
                var url= '/url/index';
                axios.get(url).then(function (response) {
                    var respuestaaa=response.data;
                    me.showurlarray= respuestaaa.urlvideo;
                })
                .catch(function (error) {
                    console.log(error);
                });
            },

            listarImagenesWeb()
            {
            let me=this;
                var url= '/imagenesweb/index';
                axios.get(url).then(function (response) {
                    var respuestaaa=response.data;
                    console.log(respuestaaa.imagenes);
                    me.showimagenesarray= respuestaaa.imagenes;
                })
                .catch(function (error) {
                    console.log(error);
                });
            },


            listarImagenesBanner()
            {

              let me=this;
              var url= '/webciudad/listarBanner';
              axios.get(url).then(function (response) {
                  var respuestaaa=response.data;
                  console.log(respuestaaa.imagenes);
                  me.listBannerarray= respuestaaa.imagebanner;
                })
                .catch(function (error) {
                    console.log(error);
                });

            },



            listarEnfoquepago(){
                let me=this;
                 var url= '/webciudad/index';
                 axios.get(url).then(function (response) {
                    var respuestaaa=response.data;
                    me.ciudadd= respuestaaa;
                     me.enfoquee= me.ciudadd.enfoque.enfoque;
                     me.enfoqueparticipar= me.ciudadd.participar.enfoque;
                     me.enfoquepago= me.ciudadd.pago.enfoque;
    
                     me.imagen=me.ciudadd.enfoque.imagen;
                     me.imagenparticipar=me.ciudadd.participar.imagen;
                     me.imagenpago=me.ciudadd.pago.imagen;
               
    
    
    
                   })
                   .catch(function (error) {
                       console.log(error);
                   });
             },


             
            verponecia()
            {
            let me=this;
                var url= '/url/showPonenciaWeb';
                axios.get(url).then(function (response) {
                    var respuestaaa=response.data;
                   console.log(respuestaaa.inscripcionAsistente.total);
                   me.inscripcionAsistente=respuestaaa.inscripcionAsistente.total;
                   me.inscripcionPonente=respuestaaa.inscripcionPonente.total;
                   
                })
                .catch(function (error) {
                    console.log(error);
                });
            }

       

  

        
    },
    computed: {
        
    },
    watch: {
        
    },
    mounted() {
        this.listarImagenesWeb();
        this.listarurlVideo();
        this.listarImagenesBanner();
        this.listarEnfoquepago();
        this.verponecia();
    },
});



