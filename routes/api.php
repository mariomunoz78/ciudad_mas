<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::post('register', 'appmovil\UserControllerr@register');
    Route::post('login', 'appmovil\UserControllerr@login');

    Route::get('showUrlVideos', 'appmovil\ShowController@showUrlVideos');
    Route::get('showImagenes', 'appmovil\ShowController@showImagenes');
    Route::get('showSchool', 'appmovil\InscripcionesController@showSchool');

    
    //ruta para poder restablecer la contraseña
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');


    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::get('logout', 'appmovil\UserControllerr@logout');
        Route::post('inscripciones', 'appmovil\InscripcionesController@inscripciones');
    });
    




