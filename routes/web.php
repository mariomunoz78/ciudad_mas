<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::view('index/', 'auth/login');  
Route::view('/nueva/cuenta', 'auth/register'); 
Route::view('/olvidocuenta', 'auth/passwords/email');

//rutas para mostrar estudiante, profesores, administradores y padres de familia
Route::get('/usuarios', 'HomeController@read');
Route::get('/padres', 'ContactoController@read');  
Route::get('/profesores', 'ContactoController@show');
Route::get('/admin', 'HomeController@show');
//ruta para que me mande al perfil del estudiante
//esta ruta es para poder guardar en la base de dato el formulario de inquietud que el usuario envia desde la pagina web
Route::resource('/contactame/ciudad','ContactoController');

//esta ruta es para poder guardar en la base de dato el formulario de inquietud que el usuario envia desde la pagina web
Route::resource('/mensajes','ContactoController');

Route::resource('/inscripcion/evento','IncripcionController');
//en el controalador de incripciones agregar index, delete etc
Route::resource('/inscripcion/estudiante','IncripcionController');
//en el controalador de incripciones agregar index, delete etc


Route::PATCH('/edit/{id}', 'IncripcionController@update')->name('edit.update');
//ruta para poder modificar la inscripcion desde el adminsitrador

//pdf
Route::get('user','IncripcionController@exportPdf')->name('users.pdf');
//excel
Route::get('user-li-excel','IncripcionController@exportExcel')->name('users.excel');

//pdf
Route::get('padrerpd','IncripcionController@PadrePdf')->name('padre.pdf');
//excel
Route::get('padre-list-exce','IncripcionController@PadreExcel')->name('padre.excel');

//pdf inscripiones
Route::get('proferpd','IncripcionController@ProfePdf')->name('profe.pdf');
//excel
Route::get('profe-list-exce','IncripcionController@ProfeExcel')->name('profe.excel');
 //pdf para generar un archivo pdf con todos los usuarios registrados en el foro ciudad mas
Route::get('inscripcionesp','IncripcionController@InscripcionesPdf')->name('inscripciones.pdf');
//excel para generar un archivo ecxel con todos los usuarios registrados en el foro ciudad mas
Route::get('inscripciones-list-exce','IncripcionController@InscripcionesExcel')->name('inscripciones.excel');


 
//rutas para verificar los padres de familia y profesores que faltan por pagar en el foro ciudad mas de monteria cordoba
Route::get('/estudiantespagos','UsuarioPagados@index');
//rutas para verificar los padres de familia y profesores que faltan por pagar en el foro ciudad mas de monteria cordoba
Route::get('/padrespagos','UsuarioPagados@read');   
Route::get('/profesorespagos','UsuarioPagados@store');

//RUTAS PARA LOS USUARIO QUE ESTAN EN MORA TODAVIA
Route::get('/estudiantesnopagos','UsuarioNoPagados@index');
//rutas para verificar los padres de familia y profesores que faltan por pagar en el foro ciudad mas de monteria cordoba
Route::get('/padresnopagos','UsuarioNoPagados@read');   
Route::get('/profesoresnopagos','UsuarioNoPagados@store');

//PDF PARA VER LOS USUARIOS QUE YA HAN PAGADO
Route::get('userpagaronpdf','UsuarioPagados@estupagarontPdf')->name('pagosestudiantes.pdf');
Route::get('padrerpagaronpdf','UsuarioPagados@PadrepagarontPdf')->name('pagospadre.pdf');
Route::get('proferpagaronpdf','UsuarioPagados@ProfepagarontPdf')->name('pagosprofe.pdf');

//PDF PARA VER LOS USUARIOS QUE NO HAN PAGADO EL FORO CIUDAD MAS
Route::get('userpdf','UsuarioNoPagados@estupagarontPdf')->name('pagosestudiantesnopago.pdf');
Route::get('padrerpdf','UsuarioNoPagados@PadrepagarontPdf')->name('pagospadrenopago.pdf');
Route::get('proferpdf','UsuarioNoPagados@ProfepagarontPdf')->name('pagosprofenopago.pdf');



//ruta para los usuarios que se han registrado y no se han inscrito al foro ciudad mas 
//rutas para verificar los padres de familia y profesores que faltan por pagar en el foro ciudad mas de monteria cordoba
Route::get('/estudiantesincompleta','UsuariosIncopletos@index');
Route::get('/padreincompleta','UsuariosIncopletos@store');  
Route::get('/profeincompleta','UsuariosIncopletos@read');



//RUTAS PARA VER LOS ESTUDIANTES QUE ESTAN EN LOS DEBATES UNO, DOS, TRES Y CUATRO
Route::get('/debateuno','Debate@index');
Route::get('/debatedos','Debate@read');
Route::get('/debatetres','Debate@show');
Route::get('/debatecuatro','Debate@store'); 


Route::get('debateuno/{idDebate}/{parametro}', array('as' => 'debateuno', 'uses' => 'Debate@editar'));
//pdf para descargar listado de los estudiantes por debates
Route::get('debateunopdf','Debate@DebateUnoPdf')->name('debateuno.pdf');
Route::get('debatedospdf','Debate@DebateDosPdf')->name('debatedos.pdf');
Route::get('debatetrespdf','Debate@DebateTresPdf')->name('debatetres.pdf');
Route::get('debatecuatropdf','Debate@DebateCuatroPdf')->name('debatecuatro.pdf');


//ruta para crear, editar y eliminar jurado
Route::resource('/jurado/ciudad','Jurado');

//Ruta en el cual voy a mandar para hacer las calificaciones
Route::resource('/calificaciones','ControllerCalificacion');

//para ver todoas las ponencias
Route::get('/ponenciatotal','Debate@Ponenciatotal');
//pdf de los ponentes
Route::get('ponentPdf','Debate@PonenciasPdf')->name('ponentesfoto.pdf');



//Redirecciona ciudad mas


Route::get('/', function () {
    return view('web/inicio');
});

Route::view('/ciudadmas', 'web/ciudad'); 
Route::view('/inicio', 'web/inicio'); 
Route::view('/contactos', 'web/contact'); 
Route::view('/gallery-img', 'web/galeria-img'); 
Route::view('/gallery-videos', 'web/galeria-video'); 
Route::view('/eventos', 'web/eventos'); 
Route::view('/eventos-det', 'web/eventos-detalles'); 




//rutas pagina web
Route::prefix('editarweb')->group(function() {
    Route::get('index', 'ControllerEditarweb@index');
});


Route::prefix('contact')->group(function() {
    Route::post('store', 'ContactoController@store');
});

Route::prefix('webciudad')->group(function() {
    Route::get('index', 'WebciudadController@index'); 
    Route::post('registerEnfoque', 'WebciudadController@registerEnfoque');
    Route::post('registerParticipar', 'WebciudadController@registerParticipar');
    Route::post('registerPago', 'WebciudadController@registerPago');

    Route::get('listarBanner', 'WebciudadController@listarBanner'); 
    Route::post('addImageBanner', 'WebciudadController@addImageBanner');
    Route::put('deleteImageBanne', 'WebciudadController@deleteImageBanne');
});


Route::prefix('url')->group(function() {
    Route::get('index', 'UrlvideoController@index');
    Route::post('registerUrl', 'UrlvideoController@registerUrl');
    Route::put('update', 'UrlvideoController@update');
    Route::put('remove', 'UrlvideoController@remove');
    Route::get('showPonenciaWeb', 'UrlvideoController@showPonenciaWeb');

});


Route::prefix('imagenesweb')->group(function() {
    Route::get('index', 'ImageswebController@index');
    Route::post('addImagenes', 'ImageswebController@addImagenes');
    Route::put('remove', 'ImageswebController@remove');    
});



Route::prefix('school')->group(function() {
    Route::get('index', 'SchoolController@index');
    Route::post('store', 'SchoolController@store');
    Route::post('updateSchool', 'SchoolController@updateSchool');

    Route::get('condition/{id}/{condition}', 'SchoolController@condition');

    
});



Route::prefix('inscripcionre')->group(function() {
    Route::get('delete/{id}', 'IncripcionController@deleteUsers');
    Route::get('viewEditUser/{id}', 'IncripcionController@viewEditUser');
    Route::post('editarUsers', 'IncripcionController@editarUsers');
    Route::get('school', 'SchoolController@index');
    Route::get('showStudentInscrito', 'IncripcionController@showStudentInscrito');    
});



Route::prefix('inscripcionStudy')->group(function() {
    Route::post('registerStudy', 'IncripcionController@registerStudy');  
    Route::get('id_UserLogueado', 'IncripcionController@id_UserLogueado'); 
    Route::get('showponencias', 'IncripcionController@showponencias');  
 
});








