<?php

namespace school\Exports;

use school\Incripciones;
use Maatwebsite\Excel\Concerns\FromCollection;

class InscripExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
 
         return Incripciones::select("doc_identidad","nombre","apellido","tipo_persona","edad","num_telefono","ciudad","institucion_educatica","titulo_ponencia")
       ->get();
    }
}
