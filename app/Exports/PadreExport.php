<?php

namespace school\Exports;

use school\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class PadreExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return User::select("num_documento","tipo_persona","name","apellido","direccion","telefono","email","ciudad","fecha_nacimiento","created_at")
        ->where('tipo_persona','=','padre')
       ->get();
    }
}
