<?php

namespace school\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageReceived extends Mailable
{
    use Queueable, SerializesModels;
  //PARA QUITAR EL ASUNTO
  public $subject='BIENVENIDO AL III FORO CIUDAD+';

    public $msg; // para que este dispoibe en la vista
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //vamos a crear una vista
        return $this->view('emails.message-received');
    }
}
