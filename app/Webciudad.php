<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class Webciudad extends Model
{
    protected $table='webciudad';
    protected $primaryKey='id';
    public $timestamps=false;
  
 
    protected $fillable =[
        'id',  
        'id_admin',
        'tipo',
        'enfoque',
        'imagen',
        'created_at',
        'updated_at' 
    ];

    public function usuario()
    {
        return $this->belongsTo('school\User', 'id_admin');
    }

}
