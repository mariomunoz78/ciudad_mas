<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;
// hacemos referencia al modelo inscripcion
use school\Pregunta;
use school\Respuestas;
use school\Incripciones; 
use school\DetalleRespuesta; 
use school\Http\Requests\RespuestaRequest;  
// hacemos referencia al modelo usuario
use school\User;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input; 

use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias

class ControllerCalificacion extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

//detalle_respuestas   preguntas  respuestas
    // funcion para ver las preguntas que estan en el entidad pregunta
       public function index(Request $request)
    { 
          $DetalleRespuesta=DB::table('respuestas as resp')
          ->join('incripcion as inc','inc.id','=','resp.idEstudiante')
          ->select('resp.id','resp.idEstudiante','inc.nombre as nomjurado','inc.apellido as apejurado','resp.idJurado','resp.nombre_jurado','resp.num_documento_jurado','resp.ponencia','inc.titulo_ponencia','resp.observacion','resp.total_puntuacion','resp.created_at','resp.updated_at') 
          ->orderBy('resp.id','desc')
             ->paginate('250');

             return view("Calificacion/index",["DetalleRespuesta"=>$DetalleRespuesta]);
        
     }

      //funcion que me devuelve para calificar a un estudiante
          public function create()
    {    
    	$estudiante=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','usu.name','usu.apellido','inc.titulo_ponencia') 
           ->where('inc.categoria','=','Ponente')
          ->orderBy('usu.id','asc')
             ->paginate('250');


             $preguntas=DB::table('preguntas as pre')
          ->select('pre.id','pre.pregunta','pre.created_at','pre.updated_at') 
          ->orderBy('pre.id','asc')
             ->paginate('250');
       
           return view("Calificacion.create",["estudiante"=>$estudiante,"preguntas"=>$preguntas]);
         
    }


    //almacenar todos esos datos paso por parametro lo que voy a validar 
    public function store (RespuestaRequest $request)
{
try {
    //inicio una transaccion
    DB::beginTransaction();
      $resp=new Respuestas; //INSTANCIO
     $resp->idEstudiante=$request->get('Estudiante');
     $resp->idJurado=$request->get('idjuradoo');
     $resp->nombre_jurado=$request->get('nombre_jurado');
     $resp->num_documento_jurado=$request->get('num_documento_jurado');
     $resp->ponencia=$request->get('Estudiante'); 
     $resp->observacion=$request->get('observacion');  
     $resp->total_puntuacion=$request->get('totalpuntuacion');
        //declaro una variable mitiempo con la funcion carbon que es la que me va a mostrar la hora exacta donde estoy ubicado
     $mitiempo=Carbon::now('America/Bogota');
     // lo paso con la funcion todatetime para convertirla en una formato de fecha y hora
     $resp->created_at=$mitiempo->toDateTimeString();
     $resp->save();
    // tambien enviar   con un array de controller recibo y mando
     $idpregunta=$request->get('idpregunta');  
     $cantida=$request->get('idPregunta');
     $puntuacion=$request->get('puntu');
     $cont=0; // para ir recorriendo el vector
  // recorro con un cilo while miestras el contador sea menor al tamaño de idpregunta 
     while($cont<count($idpregunta)){
     $detalleRespuesta = new DetalleRespuesta();  // instancio
     $detalleRespuesta->idRespuesta=$resp->id;  // le pasamos el idingreso autogenerado 
     $detalleRespuesta->idpregunta=$idpregunta[$cont]; 
     // envio idpregunta que este en la posicion cont
     $detalleRespuesta->puntaje=$puntuacion[$cont];
      $mitiempodos=Carbon::now('America/Bogota');
     // lo paso con la funcion todatetime para convertirla en una formato de fecha y hora
     $detalleRespuesta->created_at=$mitiempodos->toDateTimeString();
   
     $detalleRespuesta->save();  

    $cont=$cont+1;
     }
     // finalizo la transaccion
    DB::commit();
} catch (Exception $e) {

// si hay algun error anulo la transaccion
    DB::rollback();   
}
         return back()->with('mario','Calificacion realizada con exito');
}


//FUNCION PARA PODER VER LOS DETALLE DE CADA USUARIO QUE HA SIDO CALIFICADO
     public function show($id)
    {   
    	//consulta para mostrar los detalles de las calificaciones y criterios evaluados en el foro ciudadmas
        $detalleRespuesta=DB::table('detalle_respuestas as det')
          ->join('preguntas as preg','preg.id','=','det.idPregunta')
          ->select('det.id','det.idRespuesta','det.idPregunta','preg.pregunta','det.puntaje','det.created_at') 
           ->where('det.idRespuesta','=',$id)
          ->orderBy('det.id','asc')
             ->paginate('250');


             $respuesta=DB::table('respuestas as resp')
          ->join('incripcion as inc','inc.id','=','resp.idEstudiante')
          ->select('resp.id','resp.idEstudiante','inc.nombre as nomestudiante','inc.apellido as apeestudiante','resp.idJurado','resp.nombre_jurado','resp.num_documento_jurado','resp.ponencia','inc.titulo_ponencia','resp.observacion','resp.total_puntuacion','resp.created_at','resp.updated_at') 
            ->where('resp.id','=',$id)
          ->first();


           
           return view("Calificacion.detallecalificacion",["detalleRespuesta"=>$detalleRespuesta,"respuesta"=>$respuesta]);

    }













}
