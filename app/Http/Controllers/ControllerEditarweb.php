<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

class ControllerEditarweb extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
         { 
            return view("editarweb/index");
        }
    }
}
