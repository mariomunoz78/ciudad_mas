<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;
use school\Urlvideo;
use Illuminate\Support\Facades\Redirect;  
use DB;
use Carbon\Carbon;

class UrlvideoController extends Controller
{
     
    public function index()
    {
        $urlvideo = Urlvideo::with('usuario')->orderBy('id','desc')
        ->get();

        return response()->json([
            'status' => 'ok',
            'urlvideo' => $urlvideo,
        ], 200);

    }

    public function registerUrl(Request $request) 
    {

      $hora=Carbon::now('America/Bogota');
      $url = Urlvideo::create([
         'id_admin' => \Auth::user()->id,
         'url' => $request->get('url'),
         'created_at' => $hora,
         ]);

        return response()->json([
               'status' => 'ok',
         ], 200);

    }

    public function update(Request $request)
    {
        $url = Urlvideo::findOrFail($request->get('id'));
        $url->url = $request->get('url');
        $url->update();
        return response()->json([
               'status' => 'ok',
         ], 200);   

    }

    public function remove(Request $request)
    {
        $res=Urlvideo::where('id',$request->get('id'))->delete();
    }

    public function showPonenciaWeb()
    {

       $inscripcionAsistente = DB::table('incripcion')
       ->select(DB::raw('COUNT(titulo_ponencia ) AS total'))
      -> first();
        

        $inscripcionPonente = DB::table('incripcion')
       ->select(DB::raw('COUNT( DISTINCT titulo_ponencia ) AS total'))
       ->where ('categoria', 'Ponente')
        -> first();
       
        return response()->json([
             'status' => 'ok',
            'inscripcionAsistente' => $inscripcionAsistente,
            'inscripcionPonente' => $inscripcionPonente,
            
        ],
         200);

        
       }




   


}
