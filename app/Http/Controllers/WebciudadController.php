<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Webciudad;
use Illuminate\Support\Facades\Redirect;  
use DB;
use Carbon\Carbon;
class WebciudadController extends Controller
{

    public function index()
    {
        $enfoque = Webciudad::with('usuario')->where('tipo','enfoque')->first();
        $participar = Webciudad::with('usuario')->where('tipo','participar')->first();
        $pago = Webciudad::with('usuario')->where('tipo','pago')->first();

        return response()->json([
            'status' => 'ok',
            'enfoque' => $enfoque,
            'participar'=>$participar,
            'pago'=>$pago,
        ], 200);
    }

    public function registerEnfoque(Request $request) 
     {  

        $hora=Carbon::now('America/Bogota');
        $file = $request->file('image'); // guardamos la imagen en la variable file
        $imageName = $hora.$file->getClientOriginalName();
        $request->image->move(public_path('aplica/img/paginaweb'), $imageName);
        $idciudad=DB::table('webciudad as web')->select('*')->where('web.tipo','=','enfoque')->first();
        $ciudad= Webciudad::findOrFail($idciudad->id);
        $ciudad->id_admin=\Auth::user()->id;
        $ciudad->tipo='enfoque';
        $ciudad->enfoque=$request->get('enfoquee');

        if(!empty($file->getClientOriginalName())){
          $ciudad->imagen = $imageName;
        }else{
          $ciudad->imagen = 'vacio';
        }

        $ciudad->created_at = $hora;
        $ciudad->update();

        return response()->json([
               'status' => 'ok',
         ], 200);   
      }



      public function registerParticipar(Request $request)
      {

        $hora=Carbon::now('America/Bogota');
        $file = $request->file('image'); // guardamos la imagen en la variable file
        $imageName = $hora.$file->getClientOriginalName();
        $request->image->move(public_path('aplica/img/paginaweb'), $imageName);

        
        $idciudad=DB::table('webciudad as web')->select('*')->where('web.tipo','=','participar')->first();
        $ciudad= Webciudad::findOrFail($idciudad->id);
        $ciudad->id_admin=\Auth::user()->id;
        $ciudad->tipo='participar';
        $ciudad->enfoque=$request->get('enfoquee');
        if(!empty($file->getClientOriginalName())){
          $ciudad->imagen = $imageName;
        }
        else{
          $ciudad->imagen = 'vacio';
        }
        $ciudad->created_at = $hora;
        $ciudad->update();

        return response()->json([
               'status' => 'ok',
         ], 200);  
 
      } 



      public function registerPago(Request $request)
      { 




        $hora=Carbon::now('America/Bogota');
        $file = $request->file('image'); // guardamos la imagen en la variable file
        $imageName = $hora.$file->getClientOriginalName();
        $request->image->move(public_path('aplica/img/paginaweb'), $imageName);
        $idciudad=DB::table('webciudad as web')->select('*')->where('web.tipo','=','pago')->first();
        $ciudad= Webciudad::findOrFail($idciudad->id);
        $ciudad->id_admin=\Auth::user()->id;
        $ciudad->tipo='pago';
        $ciudad->enfoque=$request->get('enfoquee');
        if(!empty($file->getClientOriginalName())){
          $ciudad->imagen = $imageName;
        }else{
          $ciudad->imagen = 'vacio';
        }
        $ciudad->created_at = $hora;
        $ciudad->update();

        return response()->json([
               'status' => 'ok',
         ], 200);  

      } 


    //add image in webciudad db
      public function listarBanner()
      {
        $imagebanner = Webciudad::with('usuario')->where('tipo','banner')->orderBy('id','desc')->get();
        
        return response()->json([
            'status' => 'ok',
            'imagebanner' => $imagebanner,
        ], 200);

      }

      public function addImageBanner(Request $request)
      {
        
         $hora=Carbon::now('America/Bogota');
         $file = $request->file('image'); // guardamos la imagen en la variable file
         $imageName = $hora.$file->getClientOriginalName();

         $request->image->move(public_path('aplica/img/paginaweb'), $imageName);
  
  
         $bannerImage = new Webciudad; //instancio el modelo Barrio
         $bannerImage->id_admin = \Auth::user()->id;
         $bannerImage->tipo='banner';
         $bannerImage->imagen = $imageName;
         $bannerImage->created_at=$hora;
         $bannerImage->save();
  
         return response()->json([
             'status' => 'ok',
       ], 200);
       }

       public function deleteImageBanne(Request $request)
       {
        $imageBanner=Webciudad::where('id',$request->get('id'))->delete();
       }


}
