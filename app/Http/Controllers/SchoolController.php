<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;
use school\School;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;  


class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
       $school=School::with('usuario')->orderBy('id','desc')->get();
       return view('school',compact('school'));    
    }

    public function store(Request $request)
    {

        $message =$this->validate($request, [
            'colegio' => 'required',
            'logo'=>'required|image|mimes:jpg,jpeg,bmp,svg,png'
             ]);

            //  $hora=Carbon::now('America/Bogota');
            //  $file = $request->file('logo'); // guardamos la imagen en la variable file
            //  $imageName = $hora.$file->getClientOriginalName();
     
            //  $request->image->move(base_path('aplica/img/logocolegios'), $imageName);
     
             if ($request->hasFile('logo')) {      
                $file=$request->file('logo');
                $file->move(base_path().'/public/aplica/img/logocolegios', str_replace(" ", "_",$file->getClientOriginalName()));
                $imageName=str_replace(" ", "_",$file->getClientOriginalName());
           }


             


          $addSchool = School::create([
            'id_admin' => \Auth::user()->id,
            'name' => $request->get('colegio'),
            'logo'=>$imageName,
            ]);

            return back()->with('correcto','Colegio '.$request->get('colegio').'  ingresado con exito');

         
    }

    public function updateSchool(Request $request)
    {

        $message =$this->validate($request, [
            'colegio' => 'required',
            'user_id' => 'required',

             ]);

             if ($request->hasFile('logo')) {      
                $file=$request->file('logo');
                $file->move(base_path().'/public/aplica/img/logocolegios', str_replace(" ", "_",$file->getClientOriginalName()));
                $imageName=str_replace(" ", "_",$file->getClientOriginalName());
           }
        

        $school = School::findOrFail($request->get('user_id'));
        $school->name = $request->get('colegio');
        if($request->hasFile('logo')){
        $school->logo = $imageName;
        }
        $school->update();

        return back()->with('update','colegio actualizado correctamente');


    }


    public function condition($id, $condition)
    {
       
    if($condition==0){

        $school = School::findOrFail($id);
        $school->condition = '1';
        $school->save();
        
    }else{
        $school = School::findOrFail($id);
        $school->condition = '0';
        $school->save();
    }

    return back()->with('update','colegio actualizado correctamente');


    }
}
