<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion
use school\Incripciones;
// hacemos referencia al modelo usuario
use school\User;

use Illuminate\Support\Facades\Redirect;  
use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias

class UsuarioPagados extends Controller
{
    
      public function __construct()
    {
        $this->middleware('auth');
    }

  // METODO PARA MOSTRAR LOS USUARIOS PAGOS

     public function index()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Pago')
          ->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('200');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            return view("mostrar/estudiante/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }
   //METODO PARA MOSTRAR LOS PADRES DE FAMILIA 

     public function read()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Pago')
          ->where('usu.tipo_persona','=','padre')
          ->orderBy('inc.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            return view("mostrar/padre/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }


       public function store()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Pago')
          ->where('usu.tipo_persona','=','profe')
          ->orderBy('inc.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            return view("mostrar/profe/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }

     // FUNCION PARA EXPPORTAR PDF DE TODOS LOS USUARIOS REGISTRADOS EN EL FORO
    public function estupagarontPdf(){
      $incripcion=Incripciones::get();
      $pdf=PDF::loadView('mostrar.estudiante.pdf',compact('incripcion'));
     return $pdf->download('estudiante_Que_pagaron.pdf');
    }

      //funcion para imprimir en pdf a los padre de familia registrados
       public function PadrepagarontPdf(){
      $incripcion=Incripciones::get();
      $pdf=PDF::loadView('mostrar.padre.pdf',compact('incripcion'));
     return $pdf->download('padresdefamilia_Que_pagaron.pdf');
    }

       //funcion para imprimir en formato excel los todos los usuarios registrados en el foro ciudad mas
       public function ProfepagarontPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('mostrar.profe.pdf',compact('incripcion'));
      return $pdf->download('profesores_Que_pagaron.pdf');
    }
}
