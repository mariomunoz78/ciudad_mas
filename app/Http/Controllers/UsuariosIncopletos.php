<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion
use school\Incripciones;
// hacemos referencia al modelo usuario
use school\User;

use Illuminate\Support\Facades\Redirect;  
use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias

class UsuariosIncopletos extends Controller
{

	   public function __construct()
    {
        $this->middleware('auth');
    }
     public function index(Request $request)
    {
    	 $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 


    	$usuarioincompletos= DB::select("SELECT users.id ,users.tipo_persona, users.name,users.apellido,users.num_documento, users.tipo_documento, users.ciudad,users.direccion, users.telefono, users.email FROM users WHERE users.id NOT IN (SELECT incripcion.id_usuario_ingresado FROM incripcion)");

           return view("usuariosincompletos.estudiante.estudiantesincompletosm",["usuarioincompletos"=>$usuarioincompletos]);

        }
    }

    public function store(){

    	 $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 


    	$usuarioincompletos= DB::select("SELECT users.id ,users.tipo_persona, users.name,users.apellido,users.num_documento, users.tipo_documento, users.ciudad,users.direccion, users.telefono, users.email FROM users WHERE users.id NOT IN (SELECT incripcion.id_usuario_ingresado FROM incripcion)");

           return view("usuariosincompletos.estudiante.padresincompletosm",["usuarioincompletos"=>$usuarioincompletos]);

        }

    }

     public function read(){

    	 $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 

    	$usuarioincompletos= DB::select("SELECT users.id ,users.tipo_persona, users.name,users.apellido,users.num_documento, users.tipo_documento, users.ciudad,users.direccion, users.telefono, users.email FROM users WHERE users.id NOT IN (SELECT incripcion.id_usuario_ingresado FROM incripcion)");

           return view("usuariosincompletos.estudiante.profeincompletosm",["usuarioincompletos"=>$usuarioincompletos]);

        }

    }
  

  
}
