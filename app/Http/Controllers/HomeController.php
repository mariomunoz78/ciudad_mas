<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;
use school\User;
use school\School;
use school\Incripciones;


use DB;
//llamo a la base de datos

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
         { 
            //connsulta para saber cuantos estudiantes estan registrado
                  $estudiante=DB::table('users as usu')
                  ->select(DB::raw('count(*) as total'))
                  ->where('usu.tipo_persona','=','estudiante')
                  ->first();
          // declaro otro 

                  $administradores=DB::table('users as usu')
                  ->select(DB::raw('count(*) as total'))
                  ->where('usu.tipo_persona','=','admimario')
                  ->first();

                  $profesores=DB::table('users as usu')
                  ->select(DB::raw('count(*) as total'))
                  ->where('usu.tipo_persona','=','profe')
                  ->first();

                  $padre=DB::table('users as usu')
                  ->select(DB::raw('count(*) as total'))
                  ->where('usu.tipo_persona','=','padre')
                  ->first();


                  //-----------------------------------------------------------


                   $aministradordinero=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroadminis'))
                  ->where('insc.tipo_persona','=','admimario')
                  ->where('insc.estado','=','Pago')
                  ->first();

                  //connsulta para saber cuantos estudiantes estan registrado
                  $estudiantedinero=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroestudiante'))
                  ->where('insc.tipo_persona','=','estudiante')
                  ->where('insc.estado','=','Pago')
                  ->first();

                  $profedinero=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroadminis'))
                  ->where('insc.tipo_persona','=','profe')
                  ->where('insc.estado','=','Pago')
                  ->first();

                  //connsulta para saber cuantos estudiantes estan registrado
                  $padredinero=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroestudiante'))
                  ->where('insc.tipo_persona','=','padre')
                  ->where('insc.estado','=','Pago')
                  ->first();
          // declaro otro insc.tipo_persona','=','estudiante', 
                  //para ver qui

                  //--------------------inscripciones al foro ciudad mas
                    $inscritoadmin=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroadminis'))
                  ->where('insc.tipo_persona','=','admimario')
                  ->first();

                  //connsulta para saber cuantos estudiantes estan registrado
                  $inscritoestu=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroestudiante'))
                  ->where('insc.tipo_persona','=','estudiante')
                  ->first();

                   $inscritoprofe=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroadminis'))
                  ->where('insc.tipo_persona','=','profe')
                  ->first();

                  //connsulta para saber cuantos estudiantes estan registrado
                  $inscritopadre=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as dineroestudiante'))
                  ->where('insc.tipo_persona','=','padre')
                  ->first();
                  
                   //consulta para saber si el estudiante esta inscritoo en el foro ciudad mas
                  $mario=DB::table('incripcion as insc')
                  ->select('insc.id_usuario_ingresado as id') 
                  ->where('insc.id_usuario_ingresado','=',\Auth::user()->id)
                  ->first();
                  
                        //para saber cuantos estan inscrito en el prier debate
                  $debateuno=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as debate'))
                  ->where('insc.debate','=','Desarrollo Económico Vs Conservación Ambiental')
                  ->first();

                  //para saber cuantos estan inscrito en el segundo debate
                  $debatedos=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as debate'))
                  ->where('insc.debate','=','Observación de implementación en proceso de paz')
                  ->first();

                  //tercer debate que estan inscrito en el foro ciudad mas
                   $debatetres=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as debate'))
                  ->where('insc.debate','=','Acción por el Clima')
                  ->first();

                  //tercer debate que estan inscrito en el foro ciudad mas
                   $debatecuarto=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as debate'))
                  ->where('insc.debate','=','Igualdad de género')
                  ->first();
                 
                    //consulta para saber cuantos usuarios masculino estan inscritos
                 $sexomasculino=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as sexo'))
                  ->where('insc.sexo','=','hombre')
                  ->first();
                  
                  //consulta para saber cuantos usuarios femeninos estan inscritos en el foro ciudad mas
                  $sexofemenino=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as sexo'))
                  ->where('insc.sexo','=','mujer')
                  ->first();
                  
                   $ponente=DB::table('incripcion as insc')
                  ->select(DB::raw('count(*) as ponente'))
                  ->where('insc.categoria','=','Ponente')
                  ->first();
                  

                




                  $totallcolegiosIscritos = DB::table('incripcion as i')
                  ->join('school as cole','cole.id','=','i.institucion_educatica')
                  ->join('users as u','u.id','=','i.id_usuario_ingresado')
                 ->select(DB::raw('count(i.institucion_educatica) as total'),'i.id','cole.name')
                 ->where('u.tipo_persona','estudiante')
                 ->groupBy('i.institucion_educatica')
                 ->orderBy('i.id', 'desc')
                 ->get();




                 $totallpadreIscritos = DB::table('incripcion as i')
                 ->join('school as cole','cole.id','=','i.institucion_educatica')
                 ->join('users as u','u.id','=','i.id_usuario_ingresado')

                ->select(DB::raw('count(i.institucion_educatica) as total'),'i.id','cole.name')
                ->where('u.tipo_persona','padre')
                ->groupBy('i.institucion_educatica')
                ->orderBy('i.id', 'desc')
                ->get();



                $totallprofeIscritos = DB::table('incripcion as i')
                ->join('school as cole','cole.id','=','i.institucion_educatica')
                ->join('users as u','u.id','=','i.id_usuario_ingresado')

               ->select(DB::raw('count(i.institucion_educatica) as total'),'i.id','cole.name')
               ->where('u.tipo_persona','profe')
               ->groupBy('i.institucion_educatica')
               ->orderBy('i.id', 'desc')
               ->get();


                 


         //devuelto estas variable como parametroa la vista
         
          return view("home",["estudiante"=>$estudiante,"mario"=>$mario,"administradores"=>$administradores,"padre"=>$padre,"profesores"=>$profesores,"estudiantedinero"=>$estudiantedinero,"aministradordinero"=>$aministradordinero,"profedinero"=>$profedinero,"padredinero"=>$padredinero,"inscritoestu"=>$inscritoestu,"inscritoadmin"=>$inscritoadmin,"inscritopadre"=>$inscritopadre,"inscritoprofe"=>$inscritoprofe,"debateuno"=>$debateuno,"debatedos"=>$debatedos,"debatetres"=>$debatetres,"debatecuarto"=>$debatecuarto,"sexomasculino"=>$sexomasculino,"sexofemenino"=>$sexofemenino,"ponente"=>$ponente,"totallcolegiosIscritos"=>$totallcolegiosIscritos,"totallpadreIscritos"=>$totallpadreIscritos,"totallprofeIscritos"=>$totallprofeIscritos]);
         }
          
         if ($checkAuth =='estudiante'  || $checkAuth =='padre' || $checkAuth =='profe')
         {
              $marioo=DB::table('incripcion as insc')
                  ->select('insc.id_usuario_ingresado as id') 
                  ->where('insc.id_usuario_ingresado','=',\Auth::user()->id)
                  ->first();

          return view("estudiante",["marioo"=>$marioo]);
         
         }
          if($checkAuth =='jurado1124'){
         $preguntas=DB::table('preguntas as pre')
          ->select('pre.id','pre.pregunta','pre.created_at','pre.updated_at') 
          ->orderBy('pre.id','asc')
             ->paginate('250');

             return view("jurado",["preguntas"=>$preguntas]);
         }
          else
          {
            return abort(403);
          }
    }

    public function read(Request $request)
    {
         if ($request) {
          $query=trim($request->get('searchText'));
          $usuario=DB::table('users as usua')
          ->select('usua.id','usua.tipo_persona','usua.tipo_documento','usua.num_documento','usua.name','usua.apellido','usua.fecha_nacimiento','usua.ciudad','usua.direccion','usua.telefono','usua.email','foto','created_at') 
          ->where('usua.id','LIKE','%'.$query.'%')
          ->orwhere('usua.name','LIKE','%'.$query.'%')
          ->orwhere('usua.apellido','LIKE','%'.$query.'%')
          ->orwhere('usua.telefono','LIKE','%'.$query.'%')
          ->orwhere('usua.ciudad','LIKE','%'.$query.'%')
          ->orderBy('usua.id','desc')
          ->paginate('800');
          // declaro otro 

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("usuarios/estudiante/index",["usuario"=>$usuario]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }

          public function show(Request $request)
    {
         if ($request) {
          $query=trim($request->get('searchText'));
          $usuario=DB::table('users as usua')
          ->select('usua.id','usua.tipo_persona','usua.tipo_documento','usua.num_documento','usua.name','usua.apellido','usua.fecha_nacimiento','usua.ciudad','usua.direccion','usua.telefono','usua.email','foto','created_at') 
          ->where('usua.id','LIKE','%'.$query.'%')
          ->orwhere('usua.name','LIKE','%'.$query.'%')
          ->orwhere('usua.apellido','LIKE','%'.$query.'%')
          ->orwhere('usua.telefono','LIKE','%'.$query.'%')
          ->orwhere('usua.ciudad','LIKE','%'.$query.'%')
          ->orderBy('usua.id','desc')
          ->paginate('800');
          // declaro otro 

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("usuarios/admin/index",["usuario"=>$usuario]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }

}
