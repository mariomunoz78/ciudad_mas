<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
use school\Incripciones;
use school\User;
use school\School;
use school\Http\Requests\FotoRequest;  
use school\Mail\MessageReceived;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use school\Http\Requests\IncripcionFormRequest;   
use DB;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias
use Maatwebsite\Excel\Facades\Excel;
//modelo
use school\Exports\UsersExport;
use school\Exports\PadreExport;
use school\Exports\ProfeExport;
use school\Exports\InscripExport;
use ZipArchive;

//use App\Exports\UsersExport;
//use App\Imports\UsersImport;

class IncripcionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showStudentInscrito(Request $request)
    {
      $inscripcion=Incripciones::with('usuario','colegio')->orderBy('institucion_educatica','desc')->get();
      $checkAuth = \Auth::user()->tipo_persona;

      if ($checkAuth =='admimario')
     { 
        //devuelto estas variable como parametroa la vista
     return view("inscripcion/index",["inscripcion"=>$inscripcion]);
     }
      if ($checkAuth =='estudiante' || $checkAuth =='padre' || $checkAuth =='profe')
     {
       //devuelto estas variable como parametroa la vista
     return view("inscripcionestu/index",["inscripcion"=>$inscripcion]);
     }
      else
      {
        return abort(403);
      }
  

    }



       public function showponencias(){


        $inscripcion=Incripciones::with('usuario','colegio')
        ->where('titulo_ponencia','!=','no')
        ->where('categoria','Ponente' )
        ->orWhere('categoria','Sponsor' )
       ->groupBy('titulo_ponencia')
       ->orderBy('institucion_educatica','desc')
       ->get();



       return view("inscripcion/ponencias",["inscripcion"=>$inscripcion]);

       return $inscripcionPonencias;

 
       }
    
    public function index(Request $request)
    {
    	 if ($request) {
         

          $inscripcion=Incripciones::with('usuario','colegio')->orderBy('institucion_educatica','desc')->get();

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("inscripcion/index",["inscripcion"=>$inscripcion]);
         }
          if ($checkAuth =='estudiante' || $checkAuth =='padre' || $checkAuth =='profe')
         {
           //devuelto estas variable como parametroa la vista
         return view("inscripcionestu/index",["inscripcion"=>$inscripcion]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }

   //funcion para enviar a la vista para agregar una incripcion
     public function create()
    {    
        $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
           return view("inscripcion.create");
         }
          if ($checkAuth =='estudiante' || $checkAuth =='padre' || $checkAuth =='profe')
         {
             
            $school=School::where('condition',1)->get();

           //devuelto estas variable como parametroa la vista
            return view("inscripcionestu.create",["school"=>$school]);
         }
          else
          {
            return abort(403);
          }
    }



        public function store(Request $request) // paso por parametro la variable reques donde valido el formulario
    {  

         $message =$this->validate($request, [
           'nombre' => 'required',
            'apellido' => 'required',
            'doc_identidad' => 'required|numeric',
            'edad' => 'required|numeric',
            'id_usuario_ingresado' => 'required|unique:incripcion',
            'institucion_educatica' => 'required',
            'ciudad' => 'required',
            'categoria' => 'required',
            'titulo_ponencia' => 'required',
            'alergia' => 'required',
            'housing' => 'required',
            'emergencia' =>  'required|numeric', 
            'usodatos' =>  'required',
          //  'debate' =>  'required',
            'sexo' =>  'required'
            
            ]);

       Mail::to(\Auth::user()->email)->queue(new MessageReceived($message));


        $inscripcion=new Incripciones;  
        $inscripcion->id=$request->get('id');
        $inscripcion->nombre=$request->get('nombre');  
        $inscripcion->apellido=$request->get('apellido'); 
        $inscripcion->doc_identidad=$request->get('doc_identidad');
        $inscripcion->edad=$request->get('edad'); 
        $inscripcion->id_usuario_ingresado=$request->get('id_usuario_ingresado');
        $inscripcion->num_telefono=$request->get('num_telefono');
        $inscripcion->ciudad=$request->get('ciudad');
        $inscripcion->institucion_educatica=$request->get('institucion_educatica');
        $inscripcion->categoria=$request->get('categoria');
        $inscripcion->sexo=$request->get('sexo');
        $inscripcion->titulo_ponencia=$request->get('titulo_ponencia');
         $inscripcion->tipo_persona=\Auth::user()->tipo_persona;
        $inscripcion->alergia=$request->get('alergia');
     //   $inscripcion->debate=$request->get('debate'); 
        $inscripcion->housing=$request->get('housing');
        $inscripcion->estado='Proceso';
        $hora=Carbon::now('America/Bogota');
        $inscripcion->created_at=$hora->toDateTimeString();
        $inscripcion->updated_at=$hora->toDateTimeString();  
         $inscripcion->emergencia=$request->get('emergencia'); 
        $inscripcion->usodatos=$request->get('usodatos'); 

//foto_____________________________________________________________________________________________________________________
          $inscripcion->save();  // el objeto articulos/ que hemos almacenados con el metodo saves

             $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
               return Redirect::to('/inscripcion/evento')->with('exitoso','Inscripcion en el foro CIUDADMAS se ha realizado con Exito, verifique su correo electronico carpeta SPAM '.$request->get('nombre')); // hacemos una redireccion al listado de todas los 
         }
          if ($checkAuth =='estudiante' || $checkAuth =='padre' || $checkAuth =='profe')
         {
           //devuelto estas variable como parametroa la vista
               return Redirect::to('/inscripcion/estudiante')->with('exitoso','Inscripcion en el foro CIUDADMAS se ha realizado con Exito, verifique su correo electronico carpeta SPAM '.$request->get('nombre')); // hacemos una redireccion al listado de todas los 
         }
          else
          {
            return abort(403);
          }
      
    }

    
        public function edit($id)
    {   
         $checkAuth = \Auth::user()->tipo_persona;

          if ($checkAuth =='admimario')
         { 
          $school=School::where('condition',1)->get();

         return view("inscripcion.edit",["detalles"=>Incripciones::findOrFail($id),"school"=>$school]);
         
         }

          if ($checkAuth =='estudiante')
         {
           $detalles=DB::table('incripcion as inc')
          ->select("inc.id","inc.nombre","inc.tipo_persona","inc.apellido","inc.doc_identidad","inc.edad","inc.num_telefono","inc.ciudad","inc.institucion_educatica","inc.categoria","inc.titulo_ponencia","inc.alergia","inc.debate","inc.emergencia","inc.id_usuario_ingresado") 
          ->where('inc.id_usuario_ingresado','=',$id)
          ->first();

          $school=School::with('usuario')->orderBy('id','desc')->get();

         return view("inscripcionestu/edit",["detalles"=>$detalles,"school"=>$school]);
         }


    }
   

          public function update(FotoRequest $request,$id) 
    {   
        $usuario=Incripciones::findOrFail($id);
        $usuario->nombre=$request->get('nombre');
        $usuario->apellido=$request->get('apellido'); 
        $usuario->doc_identidad=$request->get('doc_identidad'); 
        $usuario->edad=$request->get('edad'); 
        $usuario->num_telefono=$request->get('num_telefono'); 
        $usuario->institucion_educatica=$request->get('institucion_educatica'); 
        $usuario->categoria=$request->get('categoria'); 
        $usuario->titulo_ponencia=$request->get('titulo_ponencia'); 
        $usuario->ciudad=$request->get('ciudad'); 
        $usuario->debate=$request->get('debate'); 

        $usuario->update();  // actualizo
         return back()->with('mario','Usuario actualizado con exito');
    }



        public function destroy($id) // recibo por parametro el id de la categoria que quiero modificar 
       {

                  $checkAuth = \Auth::user()->id;

                  if ($checkAuth=='27') {

                    $estadoss='Pago'; 
                    $estadonopago='Proceso'; 
                  //  $inscripcion=Incripciones::find($id); // paso el id
                        $inscripcionn = Incripciones::find($id);
                      if ($inscripcionn->estado=='Proceso') {
                          $inscripcionn->estado=$estadoss;
                          $hora=Carbon::now('America/Bogota');
                          // lo paso con la funcion todatetime para convertirla en una formato de fecha y hora
                          $inscripcionn->horamodificado=$hora->toDateTimeString();
                          $inscripcionn->modificadoidusers= \Auth::user()->id;
                          $inscripcionn->update();  // y actuaizo
                          return Redirect::to('/inscripcion/evento')->with('pago','CAMBIO DE ESTADO A PAGADO  '.$inscripcionn->nombre);
                      }
                      if ($inscripcionn->estado=='Pago') {

                          $inscripcionn->estado=$estadonopago;
                          // lo paso con la funcion todatetime para convertirla en una formato de fecha y hora
                          $hora=Carbon::now('America/Bogota');
                          $inscripcionn->horamodificado=$hora->toDateTimeString();
                          $inscripcionn->modificadoidusers= \Auth::user()->id;
                          $inscripcionn->update();  // y actuaizo

                            return Redirect::to('/inscripcion/evento')->with('nopagar','CAMBIO DE ESTADO A PENDIENTE  '.$inscripcionn->nombre);
                      }

                    }else{
                      return abort(403);
                    }
          
    }
    


      public function deleteUsers($id)
     {
            $userdelete=User::where('id',$id)->delete();
            $incriptionUsersdelete=Incripciones::where('id_usuario_ingresado',$id)->delete();
            if(empty($userdelete)){
              return back()->with('','');
            }

          return back()->with('delete','Usuario eliminado con exito');
      } 

     public function viewEditUser($id)
     {
       $user= User::where('id',$id)->firstOrFail();
       return view('usuarios.estudiante.edit',compact('user'));    

     }

     public function editarUsers(Request $request)
     {
          $users = User::find($request->get('id_user'));

          $users->name = $request->get('nombre');
          $users->apellido = $request->get('lastname');
          $users->num_documento = $request->get('num_documento');
          $users->email = $request->get('email');
          $users->tipo_persona = $request->get('tipo_persona');
          $users->update();
          return back()->with('update','Usuario actualizadado con exito');

          
     } 


     public function id_UserLogueado()
     {
         $user = \Auth::user()->id;
        
        return response()->json([
          'status' => 'ok',
          'id_userlogueado' => $user,
      ], 200);
 
     }

    




     public function registerStudy(Request $request) 
    {  
 
      $message =$this->validate($request, [
        'nombre' => 'required',
         'apellido' => 'required',
         'doc_identidad' => 'required|numeric',
         'edad' => 'required|numeric',
         'id_usuario_ingresado' => 'required|unique:incripcion',
         'institucion_educatica' => 'required',
         'ciudad' => 'required',
         'categoria' => 'required',
        // 'titulo_ponencia' => 'required',
         'alergia' => 'required',
         'housing' => 'required',
         'emergencia' =>  'required|numeric', 
         'usodatos' =>  'required',
       //  'debate' =>  'required',
         'sexo' =>  'required'
         
         ]);

        Mail::to(\Auth::user()->email)->queue(new MessageReceived($message));

         $inscripcion=new Incripciones;  
         $inscripcion->id=$request->get('id');
         $inscripcion->nombre=$request->get('nombre');  
         $inscripcion->apellido=$request->get('apellido'); 
         $inscripcion->doc_identidad=$request->get('doc_identidad');
         $inscripcion->edad=$request->get('edad'); 
         $inscripcion->id_usuario_ingresado=$request->get('id_usuario_ingresado');
         $inscripcion->num_telefono=$request->get('num_telefono');
         $inscripcion->ciudad=$request->get('ciudad');
         $inscripcion->institucion_educatica=$request->get('institucion_educatica');
         $inscripcion->categoria=$request->get('categoria');
         $inscripcion->sexo=$request->get('sexo');
         
         if($request->get('categoria')=='Asistente'){$inscripcion->titulo_ponencia='no'; 

        }else{$inscripcion->titulo_ponencia=strtoupper($request->get('titulo_ponencia'));}

          $inscripcion->tipo_persona=\Auth::user()->tipo_persona;
         $inscripcion->alergia=$request->get('alergia');
      //   $inscripcion->debate=$request->get('debate'); 
         $inscripcion->housing=$request->get('housing');
         $inscripcion->estado='Proceso';
         $hora=Carbon::now('America/Bogota');
 
          $inscripcion->emergencia=$request->get('emergencia'); 
         $inscripcion->usodatos=$request->get('usodatos'); 
 
         //return $request;
           $inscripcion->save(); 
           

           return response()->json([
            'status' => 'ok',
          ], 200);
      
       
     }
 
     




}







    
  //  // FUNCION PARA EXPPORTAR PDF DE TODOS LOS USUARIOS REGISTRADOS EN EL FORO
  //   public function exportPdf(){
  //     $users=User::get();
  //     $pdf=PDF::loadView('usuarios.estudiante.pdf',compact('users'));

  //    return $pdf->download('ejemplo.pdf');
  //   }
    
  //   //Funcion para exportar todos los estudiantes en un archivo .excel
  //   public function  exportExcel(){
  //      return Excel::download(new UsersExport, 'estudiantes.xlsx');
  //     }
      
  //         //funcion para imprimir en pdf a los padre de familia registrados
  //      public function ProfePdf(){
  //     $users=User::get();
  //     $pdf=PDF::loadView('usuarios.profe.pdf',compact('users'));

  //    return $pdf->download('profes.pdf');
  //   }

  //   //funcion para imprimir en formato excel los profesores regustrados
  //    public function  ProfeExcel(){
  //      return Excel::download(new ProfeExport, 'padresdefamilia.xlsx');
  //     }
      

  //     //funcion para imprimir en pdf a los padre de familia registrados
  //      public function PadrePdf(){
  //     $users=User::get();
  //     $pdf=PDF::loadView('usuarios.padre.pdf',compact('users'));

  //    return $pdf->download('padresdefamilia.pdf');
  //   }

  //   //funcion para imprimir en formato excel los profesores regustrados
  //    public function  PadreExcel(){
  //      return Excel::download(new PadreExport, 'padresdefamilia.xlsx');
  //     }

  //   //funcion para imprimir en formato excel los todos los usuarios registrados en el foro ciudad mas
  //      public function InscripcionesPdf(){
  //     $incripcion=Incripciones::get();
  //     $pdf=PDF::loadView('inscripcion.pdf',compact('incripcion'));

  //    return $pdf->download('Usuariosinscritos.pdf');
  //   }

  //   //funcion para imprimir en formato excel los todos los usuarios registrados en el foro ciudad mas
  //    public function  InscripcionesExcel(){
  //      return Excel::download(new InscripExport, 'usuariosInscritos.xlsx');
  //     }
