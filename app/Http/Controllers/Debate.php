<?php

namespace school\Http\Controllers;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion
use school\Incripciones;
// hacemos referencia al modelo usuario
use school\User;
use school\Http\Requests\FotoRequest;  
//importamos email
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input;
//ojo este es para poder subir la imagen desde la maquina del personal al servidor
use school\Http\Requests\IncripcionFormRequest;   

use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias

class Debate extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }


    // funcion para el debate uno
       public function index()
    {
         $checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
        { 
            
          $debateuno=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.debate','=','Desarrollo Económico Vs Conservación Ambiental')
         // ->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('250');

             return view("debate/debateuno",["debateuno"=>$debateuno]);
          
         }
          else
          {
            return abort(403);
          }
     }

    //funion para el Debate dos
      public function read()
    {

    	$checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
        { 
        	  $debatedos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.debate','=','Observación de implementación en proceso de paz')
         // ->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('250');
         
         return view("debate/debatedos",["debatedos"=>$debatedos]);
         
         }
          else
          {
            return abort(403);
          }
     }

      // funcion para el Debate tres
      public function show()
    {

    	$checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
        { 
        	 $debatetres=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.debate','=','Acción por el Clima')
          //->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('250');
         
         return view("debate/debatetres",["debatetres"=>$debatetres]);
          
         }
          else
          {
            return abort(403);
          }
     }


     //funcion para el debate cuatro

       public function store()
    {
    	$checkAuth = \Auth::user()->tipo_persona;
        
        if ($checkAuth =='admimario')
        { 
            
         $debatecuatro=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.debate','=','Igualdad de género')
         // ->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('250');
         
         return view("debate/debatecuatro",["debatecuatro"=>$debatecuatro]);
          
         }
          else
          {
            return abort(403);
          }
     }


  //FUNCION PARA PODEER EDITAR los debates
        public function editar($id, $parametro)
      {   
         $checkAuth = \Auth::user()->tipo_persona;

          if ($checkAuth =='admimario')
         { 
           
           if ($parametro==1) {
         return view("debate.edituno",["detalles"=>Incripciones::findOrFail($id)]);

           }
            if ($parametro==2) {
         return view("debate.editdos",["detalles"=>Incripciones::findOrFail($id)]);
         
           }
            if ($parametro==3) {
         return view("debate.edittres",["detalles"=>Incripciones::findOrFail($id)]);
         
           }
            if ($parametro==4) {
         return view("debate.editcuatro",["detalles"=>Incripciones::findOrFail($id)]);
         
           }
         
         }
         else{
          return abort(403);
         }


         }

         //FUNCION PARA IMPRIMIR LOS PDF DEL DEBATE UNO
        public function DebateUnoPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('debate.pdfuno',compact('incripcion'));
      return $pdf->download('debate1.pdf');
       }

        //FUNCION PARA IMPRIMIR LOS PDF DEL DEBATE DOS
        public function DebateDosPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('debate.pdfdos',compact('incripcion'));
      return $pdf->download('debate2.pdf');
       }

      //FUNCION PARA IMPRIMIR LOS PDF DEL DEBATE TRES
        public function DebateTresPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('debate.pdftres',compact('incripcion'));
      return $pdf->download('debate3.pdf');
       }

        //FUNCION PARA IMPRIMIR LOS PDF DEL DEBATE CUATRO
        public function DebateCuatroPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('debate.pdfcuatro',compact('incripcion'));
      return $pdf->download('debate4.pdf');
       }
      

  //funcion para ver los detalles de todas las ponencias
    public function Ponenciatotal()
    {
          $ponenciam=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.tipo_persona','inc.id_usuario_ingresado') 
         
          ->where('inc.categoria','=','Ponente')
          ->orderBy('inc.id','desc')
             ->paginate('200');
             return view("Calificacion/sub/ponencia",["ponenciam"=>$ponenciam]); 
 }
 
 //pdf de las ponencias
  public function PonenciasPdf(){
       $ponente=Incripciones::get();
       $pdf=PDF::loadView('Calificacion.sub.pdfponentes',compact('ponente'));
      return $pdf->download('ponentesdelForo.pdf');
       }

        

}
