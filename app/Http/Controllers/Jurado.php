<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion

use school\User;
use school\Http\Requests\JuradoFormRequest;  
//importamos email
use school\Mail\MessageReceived;
// para hacer algunas redireciones
use Illuminate\Support\Facades\Redirect;


use Illuminate\Support\Facades\Input;
//para el envio de correo electronicos
use Illuminate\Support\Facades\Mail;
//para enccriptar la contrtase
use Illuminate\Support\Facades\Hash;
  
use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;
//invoco las librerias

class Jurado extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
    	
          $jurado=DB::table('users as usu')
          ->select('usu.id','usu.tipo_persona','usu.tipo_documento','usu.salonjurado','usu.num_documento','usu.name','usu.apellido','usu.fecha_nacimiento','usu.ciudad','usu.direccion','usu.telefono','usu.email','usu.created_at')
          ->where('usu.tipo_persona','=','jurado1124')
          ->orderBy('usu.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
           return view("inscripcion/Jurado/index",["jurado"=>$jurado]);
         }
          else
          {
            return abort(403);
          }   
     }

     // funcion que nos manda a crear a un jurado esta parte solo la puede hacer el administrador
        public function create()
    {    
        $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
           return view("inscripcion.Jurado.create");
         }
          else
          {
            return abort(403);
          }
    }


     //FUNCION PARA CREAR UN NUEVO JURADO EN EL FORO CIUDAD MAS
         public function store (JuradoFormRequest $request) 
       {  
        $jurado=new User;
        $jurado->id=$request->get('id');
        $jurado->name=$request->get('name');
        $jurado->apellido=$request->get('apellido');
        $jurado->num_documento=$request->get('num_documento');
        $jurado->fecha_nacimiento=$request->get('fecha_nacimiento');
        $jurado->ciudad=$request->get('ciudad');
        $jurado->direccion=$request->get('direccion');
        $jurado->telefono=$request->get('telefono');
        $jurado->tipo_persona='jurado1124';
        $jurado->tipo_documento='cedula';
        $jurado->email=$request->get('email');
        $jurado->salonjurado=$request->get('salonjurado');
        $jurado->password=Hash::make($request->get('password'));

        $jurado->save();  // el objeto articulo que hemos almacenados con el metodo saves
     return back()->with('okmario','Jurado creado con exito');

    }


  //Funcion para eliminar jurado
      public function destroy($id) // recibo por parametro el id de la mensaje que quiero modificar 
    {
        $Jur=User::findOrFail($id); // paso el id
        $Jur->delete(); // la condicion la modifico con un 0 para no eliminarla
         return back()->with('eliminado','Jurado Eliminado con exito');
    }

}
