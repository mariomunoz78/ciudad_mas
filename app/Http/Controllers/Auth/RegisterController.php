<?php

namespace school\Http\Controllers\Auth;

use school\User;
use school\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
//importamos email
use school\Mail\mensajeBienvenida;
//para el envio de correo electronicos
use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'], 
            'num_documento' => ['required', 'numeric', 'min:9'],
            'fecha_nacimiento' => ['required'],
            'ciudad' => ['required', 'string', 'max:255'], 
            'direccion' => ['required', 'string', 'max:255'], 
          //  'foto' => ['required','mimes:jpeg,bmp,png'], 
            'telefono' => ['required', 'numeric', 'min:9'],  
            'tipo_persona' => ['required'],
            'tipo_documento' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed']

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \school\User
     */
    protected function create(array $data)
    {
             //envio de email con el facade fazas 
      //DEFINIMOS A QUIEN LE VAMOS A ENVIAR EL EMAIL
      //METODO SEND ES PARA COLOCAR QUE CONTENDRA EL EMAIL y por parametro le pasamos el mailable que en laravel se conoce como clase de php, le pasamos una instancia de MesageReceived PROCESO EN SEGUNDO PLANO
     Mail::to($data['email'])->queue(new mensajeBienvenida($data['email'],$data['password']));

     //  return new MessageReceived($message);
  //para revisarlo localmente como llega el email y como lo muestra en el navegador 
        return User::create([
            'tipo_documento' => $data['tipo_documento'],
            'name' => $data['name'],
            'apellido' => $data['apellido'],
            'num_documento' => $data['num_documento'],
            'fecha_nacimiento' => $data['fecha_nacimiento'],
            'ciudad' => $data['ciudad'],
            'direccion' => $data['direccion'],
            'telefono' => $data['telefono'],
            'tipo_persona' => $data['tipo_persona'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    


}
