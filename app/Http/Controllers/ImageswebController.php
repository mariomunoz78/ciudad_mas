<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;
use school\Imagenesweb;
use Illuminate\Support\Facades\Redirect;  
use DB;
use Carbon\Carbon;

class ImageswebController extends Controller
{
    
    public function index()
    {
        $imagenes = Imagenesweb::with('usuario')->orderBy('id','desc')
        ->get();

        return response()->json([
            'status' => 'ok',
            'imagenes' => $imagenes,
        ], 200);

    }

    
    public function addImagenes(Request $request)
    {
        $hora=Carbon::now('America/Bogota');
        $file = $request->file('image'); // guardamos la imagen en la variable file
        $imageName = $hora.$file->getClientOriginalName();

        $request->image->move(public_path('aplica/img/paginaweb'), $imageName);

        $imageswebb = new Imagenesweb; //instancio el modelo Barrio
        $imageswebb->id_admin = \Auth::user()->id;
        $imageswebb->imagen = $imageName;
        $imageswebb->created_at=$hora;
        $imageswebb->save();

        return response()->json([
            'status' => 'ok',
         ], 200);
    	// return response()->json(['success'=>'You have successfully upload image.']);
    }


    public function remove(Request $request)
    {
        $remove=Imagenesweb::where('id',$request->get('id'))->delete();
    }



    
}
