<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion
use school\Incripciones;
// hacemos referencia al modelo usuario
use school\User;
//para utilizar la zona horaria de donde estamos ubicados
use Barryvdh\DomPDF\Facade as PDF;

use Illuminate\Support\Facades\Redirect;  
use DB;
//llamo a la base de datos
use Carbon\Carbon;


class UsuarioNoPagados extends Controller
{
     
      public function __construct()
    {
        $this->middleware('auth');
    }

  // METODO PARA MOSTRAR LOS USUARIOS PAGOS

     public function index()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Proceso')
          ->where('usu.tipo_persona','=','estudiante')
          ->orderBy('inc.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
           return view("mostrar/nopagos/estudiante/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }
   //METODO PARA MOSTRAR LOS PADRES DE FAMILIA 

     public function read()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Proceso')
          ->where('usu.tipo_persona','=','padre')
          ->orderBy('inc.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
         	
         return view("mostrar/nopagos/padre/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }


       public function store()
    {
    	
          $pagos=DB::table('incripcion as inc')
          ->join('users as usu','usu.id','=','inc.id_usuario_ingresado')
          ->select('inc.id','inc.nombre','inc.apellido','inc.doc_identidad','inc.edad','inc.num_telefono','inc.ciudad','inc.institucion_educatica','inc.categoria','inc.titulo_ponencia','inc.alergia','inc.emergencia','inc.estado','inc.housing','usu.id as idusuario','inc.debate','usu.name as usuarioingresado','inc.created_at','inc.updated_at','usu.direccion','usu.foto','usu.tipo_persona','inc.id_usuario_ingresado') 
          ->where('inc.estado','=','Proceso')
          ->where('usu.tipo_persona','=','profe')
          ->orderBy('inc.id','desc')
             ->paginate('100');
          // declaro otro 
         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            return view("mostrar/nopagos/profe/index",["pagos"=>$pagos]);
         }
          else
          {
            return abort(403);
          }
         
     }
     
            // FUNCION PARA EXPPORTAR PDF DE TODOS LOS USUARIOS QUE NO HAN PAGADO EN EL FOORO CIUDAD MAS
    public function estupagarontPdf(){
      $incripcion=Incripciones::get();
      $pdf=PDF::loadView('mostrar.nopagos.estudiante.pdf',compact('incripcion'));
     return $pdf->download('estudiante_No_pagaron.pdf');
    }

      //funcion para imprimir en pdf a los padre de familia registrados
       public function PadrepagarontPdf(){
      $incripcion=Incripciones::get();
      $pdf=PDF::loadView('mostrar.nopagos.padre.pdf',compact('incripcion'));
     return $pdf->download('padresdefamilia_No_pagaron.pdf');
    }

       //funcion para imprimir en formato excel los todos los usuarios registrados en el foro ciudad mas
       public function ProfepagarontPdf(){
       $incripcion=Incripciones::get();
       $pdf=PDF::loadView('mostrar.nopagos.profe.pdf',compact('incripcion'));
      return $pdf->download('profesores_No_pagaron.pdf');
    }
}
