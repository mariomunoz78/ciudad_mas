<?php

namespace school\Http\Controllers\appmovil;

use Illuminate\Http\Request;
use school\Http\Controllers\Controller;
use school\User;
use school\Imagenesweb;
use school\Urlvideo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ShowController extends Controller
{
    
    public function showUrlVideos()
    {
        $videos = Urlvideo::orderBy('id','desc')
        ->get();
        
        if($videos)
        {
            return response()->json([
                'status' => 'ok',
                'videos' => $videos,
            ], 200);
        }else{
            return response()->json([
                'status' => 'error',
            ], 401);
        }
    
    }

     
    public function showImagenes()
    {
        $imagenes = Imagenesweb::with('usuario')->orderBy('id','desc')
        ->get();

        return response()->json([
            'status' => 'ok',
            'imagenes' => $imagenes,
        ], 200);

    }


    
}
