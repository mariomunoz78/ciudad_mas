<?php

namespace school\Http\Controllers\appmovil;

use Illuminate\Http\Request;
use school\Http\Controllers\Controller;
use school\Incripciones;
use school\User;
use school\School;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use school\Mail\MessageReceived;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class InscripcionesController extends Controller
{
    
    public function inscripciones(Request $request) // paso por parametro la variable reques donde valido el formulario
    {  

         $message =Validator::make($request->all(), [
            'edad' => 'required|numeric',
            'institucion_educatica' => 'required',
            'categoria' => 'required',
            'titulo_ponencia' => 'required',
            'alergia' => 'required',
            'emergencia' =>  'required|numeric', 
            'usodatos' =>  'required',
            'sexo' =>  'required',
            ]);
            

            if($message->fails()){

                return response()->json([
                    'status'=>false,  
                    'message'=>   $message->errors()->toJson()              
                ],200);
                }
           

        // Mail::to(\Auth::user()->email)->queue(new MessageReceived($message));
         $usuario = \Auth::user();
          
            $estad='Proceso';
            $housing='no';
            $hora=Carbon::now('America/Bogota');

         $inscripcion = Incripciones::create([
            'nombre' => $usuario->name,
            'tipo_persona' => $usuario->tipo_persona,
            'apellido' => $usuario->apellido,
            'doc_identidad' => $usuario->num_documento,
            'edad' => $request->get('edad'),
            'id_usuario_ingresado' => $usuario->id,
            'num_telefono' => $usuario->telefono,
            'ciudad' => $usuario->ciudad,
            'institucion_educatica' => $request->get('institucion_educatica'),
            'categoria' => $request->get('categoria'),
            'titulo_ponencia' => $request->get('titulo_ponencia'),
            'alergia' => $request->get('alergia'),
            'estado' => $estad,
            'sexo' => $request->get('sexo'),
            'housing' => $housing,
            'usodatos' => $request->get('usodatos'),
            'emergencia' => $request->get('emergencia'),
            'created_at'=>$hora->toDateTimeString(),
    
            ]);

            if($inscripcion){
                return response()->json([
                    'status' => true,
              ], 200);
            }else{
                return response()->json([
                    'status' => false,
              ], 200);
            }
    }


    public function showSchool(Request $request){

        $showColegiosInscritos = DB::table('incripcion as i')
        ->join('school as cole','cole.id','=','i.institucion_educatica')
        ->join('users as u','u.id','=','i.id_usuario_ingresado')
       ->select(DB::raw('count(i.institucion_educatica) as total'),'i.id','cole.name')
       ->where('u.tipo_persona','estudiante')
       ->groupBy('i.institucion_educatica')
       ->orderBy('cole.name', 'asc')
       ->get();

       $totalAsistentesInscritos = DB::table('incripcion')
       ->select(DB::raw('COUNT(titulo_ponencia ) AS total'))
      -> first();

      $totalColegiosInscritos = DB::table('incripcion')
      ->select(DB::raw('COUNT( DISTINCT institucion_educatica ) AS total'))
     // ->where('tipo_persona','estudiante')
       -> first();

        $totalPonentesInscritos = DB::table('incripcion')
       ->select(DB::raw('COUNT( DISTINCT titulo_ponencia ) AS total'))
       ->where ('categoria', 'Ponente')
        -> first();

        

               return response()->json([
                    'status' => true,
                    'showColegiosInscritos'=>$showColegiosInscritos,
                 //   'totalAsistentesInscritos'=>$totalAsistentesInscritos,
                 //   'totalColegiosInscritos' => $totalColegiosInscritos,
                 //   'totalPonentesInscritos'=>$totalPonentesInscritos
              ], 200);
    }

}
