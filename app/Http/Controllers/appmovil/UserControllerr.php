<?php

namespace school\Http\Controllers\appmovil;

use Illuminate\Http\Request;
use school\Http\Controllers\Controller;
use school\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use school\Mail\mensajeBienvenida;

use DB;

class UserControllerr extends Controller
{

//php artisan serv --host 192.168.1.103

        public function login(Request $request)
        {
            $credentials = $request->only('email', 'password');
            
            try {

                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'status' => false,
                        'message' => 'credenciales invalidas'
                ], 400);
                }
            } catch (JWTException $e) {
                    return response()->json([
                        'status' => false,
                        'message' => 'no se pudo crear el token'
                        
                    ], 500);

            }

            $user = User::where('email', '=', $request->get('email'))->first();


            if($token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'status' => true,
                        'token' => $token,
                        'idusers'=>$user->id,
                        'name'=>$user->name,
                        'lastname'=>$user->apellido,
                        'email'=>$user->email,
                        'tipo_persona'=>$user->tipo_persona,
                        'tipo_documento'=>$user->tipo_documento,
                        'num_documento'=>$user->num_documento,
                        'direccion'=>$user->direccion,
                        'ciudad'=>$user->ciudad

                    ], 200);
                    
                }

        }

    
        public function register(Request $request)
        {
                $validator = Validator::make($request->all(), [
                'tipo_documento'=>'required|string',   
                'name' => 'required|string|max:255',
                'apellido' => 'required|string|max:255',
                'num_documento' => 'required|string|max:255|unique:users',
                'fecha_nacimiento' => 'required|string|max:255',
                'ciudad' => 'required|string|max:255',
                'direccion' => 'required|string|max:255',
                'telefono' => 'required|string|max:255',
                'tipopersona' => 'required',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);


            if($validator->fails()){

                    return response()->json([
                        'status'=>false,  
                        'message'=>   $validator->errors()->toJson()              
                    ],400);
             
            }
            if($request->get('tipopersona')=='Padre de familia'){
                $tipo_persona='padre';
            }if($request->get('tipopersona')=='Estudiante'){
                $tipo_persona='estudiante';
            }if($request->get('tipopersona')=='Profesor'){
                $tipo_persona='profe';
            }

            $userr = User::create([
                'tipo_documento' => $request->get('tipo_documento'),
                'name' => $request->get('name'),
                'apellido' => $request->get('apellido'),
                'num_documento' => $request->get('num_documento'),
                'fecha_nacimiento' => $request->get('fecha_nacimiento'),
                'ciudad' => $request->get('ciudad'),
                'direccion' => $request->get('direccion'),
                'telefono' => $request->get('telefono'),
                'tipo_persona' => $tipo_persona,
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);
            Mail::to($request->get('email'))->queue(new mensajeBienvenida($request->get('email'),$request->get('password')));

            $token = JWTAuth::fromUser($userr);
            $mario = User::where('email', '=', $request->get('email'))->first();



                return response()->json([
                    'status'=>true,  
                    'token'=>   $token,
                    'idusers'=>$mario->id,
                    'name'=>$mario->name,
                    'lastname'=>$mario->apellido,
                    'email'=>$mario->email,
                    'tipo_persona'=>$mario->tipo_persona,
                    'tipo_documento'=>$mario->tipo_documento,
                    'num_documento'=>$mario->num_documento,
                    'direccion'=>$mario->direccion,
                    'ciudad'=>$mario->ciudad
                ],200);

        }



            // Añadimos las respuestas JSON, ya que el Frontend solo recibe JSON
        
        
            public function logout(Request $request)
            {

                $this->validate($request, [
                    'token' => 'required',
                ]);

                try {
                    JWTAuth::invalidate($request->token);

                    return response()->json([
                        'status' => 'ok',
                        'message' => 'cierre de sesion exitoso',
                    ]);
                } catch (JWTExeption $exception) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Al usuario no se le puede cerrar la session',
                    ], 500);
                }
            }

            public function getAuthUser(Request $request)
            {
                $this->validate($request, [
                    'token' => 'required',
                ]);
                $user = JWTAuth::authenticate($request->token);
                return response()->json(['user' => $user]);
            }

            protected function jsonResponse($data, $code = 200)
            {
                return response()->json($data, $code,
                    ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
            }



            // public function sendResetLinkEmail(Request $request)
            // {
            //     $this->validate($request, ['email' => 'required|email']);
        
            //     $response = $this->broker()->sendResetLink(
            //         $request->only('email')
            //     );
          
            //     switch ($response) {
            //         case \Password::INVALID_USER:
        
            //             return response()->json([
            //                 'status'=>false,  
            //                 'message'=>   $response           
            //             ],422);
        
            //             break;
        
            //         case \Password::INVALID_PASSWORD:
        
            //             return response()->json([
            //                 'status'=>false,  
            //                 'message'=>   $response           
            //             ],422);
                        
            //             break;
        
            //         case \Password::INVALID_TOKEN:
        
            //             return response()->json([
            //                 'status'=>false,  
            //                 'message'=>   $response           
            //             ],422);
        
            //             break;
            //         default: 
        
            //         return response()->json([
            //             'status'=>true,  
            //             'message'=>   $response           
            //         ],200);        }
            // }
        
        
          




}
