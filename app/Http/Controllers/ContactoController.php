<?php

namespace school\Http\Controllers;

use Illuminate\Http\Request;

use school\Http\Requests;
// hacemos referencia al modelo inscripcion
use school\contacto;
use school\User;
// hacemos referencia al modelo usuario
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\input;
//ojo este es para poder subir la imagen desde la maquina del personal al servidor
use school\Http\Requests\ContactoFormRequest;   
use DB;
//llamo a la base de datos
use Carbon\Carbon;
//para utilizar la zona horaria de donde estamos ubicados


class ContactoController extends Controller
{
   

    public function index(Request $request)
    {
         if ($request) {
          $query=trim($request->get('searchText'));
          $contacto=DB::table('contactos as cont')
          ->select('cont.id','cont.nombre','cont.correo','cont.telefono','cont.descripcion','cont.created_at') 
          ->where('cont.id','LIKE','%'.$query.'%')
          ->orwhere('cont.nombre','LIKE','%'.$query.'%')
          ->orwhere('cont.correo','LIKE','%'.$query.'%')
          ->orwhere('cont.telefono','LIKE','%'.$query.'%')
          ->orwhere('cont.descripcion','LIKE','%'.$query.'%')
          ->orderBy('cont.id','desc')
          ->paginate('800');
          // declaro otro 

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("mensaje/index",["contacto"=>$contacto]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }
     public function store (Request $request) 
     {  
      $hora=Carbon::now('America/Bogota');

      $categoria = Contacto::create([
         'nombre' => $request->get('name'),
         'correo' => $request->get('email'),
         'telefono' => $request->get('phone'),
         'descripcion' => $request->get('description'),
         'created_at' => $hora,
         ]);

        return response()->json([
               'status' => 'ok',
         ], 200);

   
      }

       public function destroy($id) // recibo por parametro el id de la categoria que quiero modificar 
    {
     
     $usuario = DB::table('contactos')->where('id','=',$id)->delete();

     return back()->with('eliminado','Mensaje eliminado correctamente '.\Auth::user()->name);
    } 


       public function read(Request $request)
    {
         if ($request) {
          $query=trim($request->get('searchText'));
          $usuario=DB::table('users as usua')
          ->select('usua.id','usua.tipo_persona','usua.tipo_documento','usua.num_documento','usua.name','usua.apellido','usua.fecha_nacimiento','usua.ciudad','usua.direccion','usua.telefono','usua.email','foto','created_at') 
          ->where('usua.id','LIKE','%'.$query.'%')
          ->orwhere('usua.name','LIKE','%'.$query.'%')
          ->orwhere('usua.apellido','LIKE','%'.$query.'%')
          ->orwhere('usua.telefono','LIKE','%'.$query.'%')
          ->orwhere('usua.ciudad','LIKE','%'.$query.'%')
          ->orderBy('usua.id','desc')
          ->paginate('800');
          // declaro otro 

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("usuarios/padre/index",["usuario"=>$usuario]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }

      public function show(Request $request)
    {
         if ($request) {
          $query=trim($request->get('searchText'));
          $usuario=DB::table('users as usua')
          ->select('usua.id','usua.tipo_persona','usua.tipo_documento','usua.num_documento','usua.name','usua.apellido','usua.fecha_nacimiento','usua.ciudad','usua.direccion','usua.telefono','usua.email','foto','created_at') 
          ->where('usua.id','LIKE','%'.$query.'%')
          ->orwhere('usua.name','LIKE','%'.$query.'%')
          ->orwhere('usua.apellido','LIKE','%'.$query.'%')
          ->orwhere('usua.telefono','LIKE','%'.$query.'%')
          ->orwhere('usua.ciudad','LIKE','%'.$query.'%')
          ->orderBy('usua.id','desc')
          ->paginate('800');
          // declaro otro 

         $checkAuth = \Auth::user()->tipo_persona;
          if ($checkAuth =='admimario')
         { 
            //devuelto estas variable como parametroa la vista
         return view("usuarios/profe/index",["usuario"=>$usuario]);
         }
          else
          {
            return abort(403);
          }
         
     }
    }
}
