<?php

namespace school\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IncripcionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'nombre' => 'required',
            'apellido' => 'required',
            'doc_identidad' => 'required|numeric',
            'edad' => 'required|numeric',
            'num_telefono' => 'required|numeric',
            'institucion_educatica' => 'required',
            'ciudad' => 'required',
            'categoria' => 'required',
            'titulo_ponencia' => 'required',
            'alergia' => 'required',
            'housing' => 'required'
        ];
    }
}