<?php

namespace school\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JuradoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
  public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'], 
            'num_documento' => ['required'],
            'fecha_nacimiento' => ['required'],
            'salonjurado'=> ['required'],
            'ciudad' => ['required', 'string', 'max:255'], 
            'direccion' => ['required', 'string', 'max:255'],
            'telefono' => ['required', 'numeric', 'min:9'],  
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ];
}

}
