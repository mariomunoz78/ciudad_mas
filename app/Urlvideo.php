<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class Urlvideo extends Model
{
    
    protected $table='urlvideos';
    protected $primaryKey='id';
    public $timestamps=false;
  
 
    protected $fillable =[
        'id',  
        'id_admin',
        'url',
        'condicion',
        'created_at',
        'updated_at' 
    ];

    public function usuario()
    {
        return $this->belongsTo('school\User', 'id_admin');
    }
}
