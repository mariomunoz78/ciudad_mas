<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    protected $table='school';
    protected $primaryKey='id';
    public $timestamps=false;
  
 
    protected $fillable =[
        'id',  
        'id_admin',
        'name',
        'condition',
        'logo'
    ];

    public function usuario()
    {
        return $this->belongsTo('school\User', 'id_admin');
    }

}
