<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class Imagenesweb extends Model
{
    
    protected $table='imagenesweb';
    protected $primaryKey='id';
    public $timestamps=false;
  
 
    protected $fillable =[
        'id',  
        'id_admin',
        'imagen',
        'condicion',
        'created_at',
        'updated_at' 
    ];

    public function usuario()
    {
        return $this->belongsTo('school\User', 'id_admin');
    }

}
