<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class contacto extends Model
{
        // v hacer referencia de la table de persona
    protected $table='contactos';

//llave primaria de la table persona
    protected $primaryKey='id';
   

   //cuando ha sido creado o actualizado el regitro 
    public $timestamps=false;
  
  // los campos que van a resivir un valor con fililable

    protected $fillable =[
    	'id',  
    	'nombre',
    	'correo',
    	'telefono',
    	'descripcion',
    	'created_at',
    	'updated_at'

       
    ];

  //tambien podemos agregar atributos de tipos guarded    se especica cuando no queremos que se agregauen al mode locd 
    protected $guarded =[

    ];
}
