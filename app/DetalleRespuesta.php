<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class DetalleRespuesta extends Model
{
        // v hacer referencia de la table de detalle_respuestas
    protected $table='detalle_respuestas';

//llave primaria de la table detalle_respuestas
    protected $primaryKey='id';
   

   //cuando ha sido creado o actualizado el regitro 
    public $timestamps=false;
  
  // los campos que van a resivir un valor con fililable

    protected $fillable =[
    	'id',  
    	'idRespuesta',
    	'idPregunta',
    	'puntaje',
    	'created_at',
    	'updated_at'
    ];

  //tambien podemos agregar atributos de tipos guarded    se especica cuando no queremos que se agregauen al mode locd 
    protected $guarded =[

    ];
}
