<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
        // v hacer referencia de la table de respuestas
    protected $table='respuestas';

//llave primaria de la table respuestas
    protected $primaryKey='id';
   

   //cuando ha sido creado o actualizado el regitro 
    public $timestamps=false;
  
  // los campos que van a resivir un valor con fililable

    protected $fillable =[
    	'id',  
    	'idEstudiante',
        'nombre_jurado'.
        'num_documento_jurado',
        'observacion',
    	'idJurado',
    	'ponencia',
        'total_puntuacion',
    	'created_at',
    	'updated_at'
    ];

  //tambien podemos agregar atributos de tipos guarded    se especica cuando no queremos que se agregauen al mode locd 
    protected $guarded =[

    ];
}