<?php

namespace school;

use Illuminate\Database\Eloquent\Model;

class Incripciones extends Model
{
    // v hacer referencia de la table de persona
    protected $table='incripcion';

//llave primaria de la table persona
    protected $primaryKey='id';
   

   //cuando ha sido creado o actualizado el regitro 
    public $timestamps=false;
  
  // los campos que van a resivir un valor con fililable

    protected $fillable =[
    	'id',  
    	'nombre',
    	'apellido',
		'tipo_persona',
    	'doc_identidad',
    	'edad', 
    	'num_telefono',
    	'ciudad',
    	'institucion_educatica',
    	'categoria',
    	'titulo_ponencia',
    	'alergia',
    	'housing',
    	'emergencia',
		'sexo',
		'estado',
    	'usodatos',
    	'id_usuario_ingresado',
    	'created_at',
    	'updated_at'

       
    ];
	

	public function colegio()
    {
        return $this->belongsTo('school\School', 'institucion_educatica');
    }

	public function usuario()
    {
        return $this->belongsTo('school\User', 'id_usuario_ingresado');
    }

	

  //tambien podemos agregar atributos de tipos guarded    se especica cuando no queremos que se agregauen al mode locd 
    protected $guarded =[

    ];
}
