<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incripcion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->integer('doc_identidad');
            $table->integer('edad');
            $table->integer('num_telefono');
            $table->string('ciudad');
            $table->string('institucion_educatica');
            $table->string('categoria');
            $table->string('titulo_ponencia');
            $table->string('alergia');
            $table->string('housing');
            $table->integer('id_usuario_ingresado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incripcion');
    }
}
