@extends('layouts.juradohome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea peque0Š9o que me muestre todo la pantalla completa-->
		<h4><b>FICHA DE OBSERVACIÓN PARA UNA EXPOSICIÓN ORAL</b></h4>
		<h4>Se asignaran calificaciones de 1 a 5 en donde: <b style="color: green">5- Excelente</b> <b style="color: blue">4-Destacado.</b><b style="color: orange"> 3-Mínimo</b> <b style="color: fuchsia">2-Insuficiente</b> <b style="color: red"> 1- No cumple. 	</b></h4>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover table-dark">
				<thead>
					<th>#</th>
					<th>CONTENIDO</th>
					<th>Puntaje</th>
				</thead>
               @foreach ($preguntas as $pre)
					<tr> 
				    @if ($pre->id>=1 && $pre->id<=6)
				    <td>{{ $pre->id}}</td>
					<td>{{ $pre->pregunta}}</td>
					<td>X</td>
					@endif
				</tr>
				@endforeach
			</table>
		</div>
		{{$preguntas->render()}}  <!-- estom es lo que me va a mostrar la lista por categoria-->
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover table-dark">
				<thead>
					<th>#</th>
					<th>MANEJO DE AUDITORIO</th>
					<th>Puntaje</th>
				</thead>
               @foreach ($preguntas as $pre)
					<tr> 
				    @if ($pre->id>=7 && $pre->id<=13)
				    <td>{{ $pre->id}}</td>
					<td>{{ $pre->pregunta}}</td>
					<td>X</td>
					@endif
				</tr>
				@endforeach
			</table>
		</div>
		{{$preguntas->render()}}  <!-- estom es lo que me va a mostrar la lista por categoria-->
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover table-dark">
				<thead>
					<th>#</th>
					<th>ORIGINALIDAD</th>
					<th>Puntaje</th>
				</thead>
               @foreach ($preguntas as $pre)
					<tr> 
				    @if ($pre->id>=14 && $pre->id<=16)
				    <td>{{ $pre->id}}</td>
					<td>{{ $pre->pregunta}}</td>
					<td>X</td>
					@endif
				</tr>
				@endforeach
			</table>
		</div>
		{{$preguntas->render()}}  <!-- estom es lo que me va a mostrar la lista por categoria-->
	</div>
</div>

@endsection  
<!--aqui finaliza la session-->