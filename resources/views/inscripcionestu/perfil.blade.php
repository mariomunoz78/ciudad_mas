@extends('layouts.estudiantehome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
 
 	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="alert alert-danger">
            <h3 class="alert-link">Perfil</h3>
      </div>
     </div>
	</div><!--cierro la columna del div-->
          <!-- agrego una fila -->
      <div class="row">

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Nombre</label>
                         <h4> {{ \Auth::user()->name }}</h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Apellido</label>
                         <h4> {{ \Auth::user()->apellido }}</h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Documento</label>
                         <h4>{{ \Auth::user()->tipo_documento }} : {{ \Auth::user()->num_documento }} </h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Ciudad</label>
                         <h4> {{ \Auth::user()->ciudad }}</h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Direccion</label>
                         <h4> {{ \Auth::user()->direccion }}</h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Telefono</label>
                         <h4> {{ \Auth::user()->telefono }}</h4>
                       </div>
                      </div>

                       <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Correo Electronico</label>
                         <h4> {{ \Auth::user()->email }}</h4>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Fecha de nacimiento</label>
                         <h4> {{ \Auth::user()->fecha_nacimiento }}</h4>
                       </div>
                      </div>
                         </div>
          
          
                   {!!Form::model($usuario,['method'=>'PATCH','route'=>['perfil.update',$usuario->id],'files'=>'true'])!!}
                   {{Form::token()}}


                          <div class="col-lg-6 col-md-6 col-xs-12">
                         <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                          <label for="foto">Subir foto</label>
                          <input type="file" name="foto" class="form-control">

                                    @if ($errors->has('foto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('foto') }}</strong>
                                    </span>
                                @endif  
                           </div>
                           </div>

                             <div class="col-lg-6 col-md-6 col-xs-12">
                             <div class="form-group">
                               <button class="btn btn-primary" type="submit">Guardar</button>
                            </div>
                            </div>

                            {!!Form::close()!!} 
                    </div>
          
      </div>
            <div class="col-lg-6">
            <div class="col-lg-6 col-md-6 col-xs-12">
            <div class="form-group">
            	 <a href="/inscripcion/estudiante"><button class="btn btn-warning">Atras</button></a> 
            </div>
            </div>
@endsection  
<!--aqui finaliza la session-->