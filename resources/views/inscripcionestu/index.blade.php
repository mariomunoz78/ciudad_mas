@extends('layouts.estudiantehome')  
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->

	<div  class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3>Incripciones <a href="/inscripcion/evento/create"><button class="btn btn-outline-success" title="Incripcion Nueva">Nuevo</button></a></h3>
	</div>
	
</div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Predeterminado ponente</th>
					
				<!--	<th>Estado</th> -->
					<th>Detalle</th>
					<th>Editar</th>
				</thead>
               @foreach ($inscripcion as $incrip)
				
                @if ($incrip->id_usuario_ingresado== \Auth::user()->id)  
                   	<tr> 
                   	@if( Auth::user()->id==$incrip->id_usuario_ingresado)	
				    <td>{{ $incrip->id}}</td>
					<td>{{ $incrip->nombre}}</td>
					<td>{{ $incrip->apellido}}</td>
					<td>{{ $incrip->categoria }}</td>
				
					 <td>
					 	<a  class="btn btn-outline-info" href="" data-target="#modal-estado-{{$incrip->id}}" data-toggle="modal" style="color:blue"><i class="far fa-eye"></i></a>
					 </td>
					 	 <td>

						<a class="btn btn-outline-success"  href="{{URL::action('IncripcionController@edit',Auth::user()->id)}}" style="color:green"><i class="fas fa-edit"></i></a>

					 </td>

				</tr>
				





                @endif
                 @endif
                 @include('inscripcionestu.modal')
                  @include('inscripcionestu.comprobante')
				@endforeach
			</table>
		</div>
	</div>
</div>

@endsection  
<!--aqui finaliza la session-->