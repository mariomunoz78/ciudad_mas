@extends('layouts.estudiantehome')  
@section ('contenido')
@if ($detalles->id_usuario_ingresado==Auth::user()->id)
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3  class="alert alert-success"><b>Modificar inscripcion al foro ciudad mas {{ $detalles->nombre}}</b></h3>
		

            @if(session('mario'))
               <div class="alert alert-success" role="alert">
               {{session('mario')}}
              </div>
           @endif

            
            			
            {!!Form::model($detalles,['method'=>'PATCH','route'=>['edit.update',$detalles->id],'files'=>'true'])!!}
                   {{Form::token()}}
                          

            <div class="row">  

                         <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                         <label for="name">Nombre</label>
                          <input type="text" name="nombre" class="form-control"  value="{{ $detalles->nombre }}" placeholder="Nombre...">
                           @if ($errors->has('nombre'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('nombre') }}</strong>
                      </span>
                    @endif
                       </div>
                      </div>



                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                         <label for="name">Apellido</label>
                           <input type="text" name="apellido" class="form-control"  value="{{ $detalles->apellido }}" placeholder="Apellido...">
                            @if ($errors->has('apellido'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('apellido') }}</strong>
                      </span>
                    @endif
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('doc_identidad') ? ' has-error' : '' }}">
                         <label for="name">Documento</label>
                         <input type="number" name="doc_identidad" class="form-control"  value="{{ $detalles->doc_identidad }}" placeholder="Numero documento...">
                        @if ($errors->has('doc_identidad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('doc_identidad') }}</strong>
                      </span>
                    @endif
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
                         <label for="name">Ciudad</label>
                         <input type="text" name="ciudad" class="form-control"  value="{{ $detalles->ciudad }}" placeholder="Ciudad...">
                          @if ($errors->has('ciudad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('ciudad') }}</strong>
                      </span>
                    @endif
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
                         <label for="name">Edad</label>
                         <input type="number" name="edad" class="form-control" value="{{ $detalles->edad }}" placeholder="Edad...">
                          @if ($errors->has('edad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('edad') }}</strong>
                      </span>
                    @endif
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('num_telefono') ? ' has-error' : '' }}">
                         <label for="name">Telefono</label>
                         <input type="number" name="num_telefono" class="form-control" value="{{$detalles->num_telefono }}"  placeholder="Telefono...">
                          @if ($errors->has('num_telefono'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('num_telefono') }}</strong>
                      </span>
                       @endif
                       </div>
                      </div>





                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Institucion Educativa</label>

                      <select name="institucion_educatica"  class="form-control" style="width: 100%;">
                      <option value="">Seleccione</option>

                           @foreach($school as $school)
                            @if ($school->id==$detalles->institucion_educatica)
                        <option value="{{ $school->id }}" selected="selected">{{ $school->name }}</option>
                          @else
                        <option value="{{ $school->id }}">{{ $school->name }}</option>
                          @endif
                          @endforeach


                  </select>

                   </div>
                   </div>




      <div class="col-lg-6 col-md-6 col-xs-12">
         <div class="form-group">
          <label>Predeterminado ponente</label>
          <select v-model="inscripciones.categoria" name="categoria" class="form-control">
             @if($detalles->categoria=='Asistente')  
             <option value="Asistente" selected>Asistente</option>
             <option value="Ponente">Ponente</option>
             <option value="Sponsor">Sponsor</option>

            @elseif ($detalles->categoria=='Ponente')
            <option value="Asistente">Asistente</option>
             <option value="Ponente" selected>Ponente</option>
             <option value="Sponsor">Sponsor</option> 

            @elseif ($detalles->categoria=='Sponsor')
             <option value="Asistente">Asistente</option>
             <option value="Ponente">Ponente</option>
             <option value="Sponsor" selected>Sponsor</option> 

            @else
              <option">Seleccione</option>            
             <option value="Asistente">Asistente</option>
             <option value="Ponente">Ponente</option>
             <option value="Sponsor">Sponsor</option> 
            @endif
           </select>
         </div>
      </div>



                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group{{ $errors->has('titulo_ponencia') ? ' has-error' : '' }}">
                         <label for="name">Ponencia</label>
                          <input type="text" name="titulo_ponencia" class="form-control" value="{{$detalles->titulo_ponencia }}" placeholder="Titulo de la ponencia"  >
                           @if ($errors->has('titulo_ponencia'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('titulo_ponencia') }}</strong>
                      </span>
                       @endif
                       </div>
                      </div>




                        

        <div class="col-lg-6 col-md-6 col-xs-12">
         <div class="form-group">
          <label>Debate</label>
          <select name="debate" class="form-control">
             @if($detalles->debate=='Desarrollo Económico Vs Conservación Ambiental')  
            <option value="Desarrollo Económico Vs Conservación Ambiental" selected>Desarrollo Económico Vs Conservación Ambiental</option>
            <option value="Observación de implementación en proceso de paz">Observación de implementación en proceso de paz</option>
            <option value="Acción por el Clima">Acción por el Clima</option>
            <option value="Igualdad de género">Igualdad de género</option>

            @elseif ($detalles->debate=='Observación de implementación en proceso de paz')
            <option value="Desarrollo Económico Vs Conservación Ambiental">Desarrollo Económico Vs Conservación Ambiental</option>
            <option value="Observación de implementación en proceso de paz" selected>Observación de implementación en proceso de paz</option>
            <option value="Acción por el Clima">Acción por el Clima</option>
            <option value="Igualdad de género">Igualdad de género</option>  

            @elseif ($detalles->debate=='Acción por el Clima')
            <option value="Desarrollo Económico Vs Conservación Ambiental">Desarrollo Económico Vs Conservación Ambiental</option>
            <option value="Observación de implementación en proceso de paz">Observación de implementación en proceso de paz</option>
            <option value="Acción por el Clima" selected>Acción por el Clima</option>
            <option value="Igualdad de género">Igualdad de género</option>  

             @elseif ($detalles->debate=='Igualdad de género')
            <option value="Desarrollo Económico Vs Conservación Ambiental">Desarrollo Económico Vs Conservación Ambiental</option>
            <option value="Observación de implementación en proceso de paz">Observación de implementación en proceso de paz</option>
            <option value="Acción por el Clima">Acción por el Clima</option>
            <option value="Igualdad de género" selected>Igualdad de género</option>  

            @else
              <option">Seleccione</option>            
             <option value="Desarrollo Económico Vs Conservación Ambiental">Desarrollo Económico Vs Conservación Ambiental</option>
            <option value="Observación de implementación en proceso de paz">Observación de implementación en proceso de paz</option>
            <option value="Acción por el Clima">Acción por el Clima</option>
            <option value="Igualdad de género">Igualdad de género</option> 
            @endif
           </select>
         </div>
      </div>

                            

            <div class="form-group"><br><br><br>
            	<button class="btn btn-primary" type="submit">Actualizar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
     {!!Form::close()!!}  	
            
		</div>
	</div>
	 @endif
	@endsection