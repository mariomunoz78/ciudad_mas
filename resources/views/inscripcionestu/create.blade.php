@extends('layouts.estudiantehome')  
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3><b>Formulario de Inscripcion</b></h3>
            <p>{{ Auth::user()->name }} esta informacion se enviara a su correo electronico {{ Auth::user()->email }}</p>
			   @if ($errors->has('id_usuario_ingresado'))
                        <span class="help-block">
                                    <strong style="color: red"><H1>Ya usted esta inscrito en ciudadMas verifique</H1></strong>
                      </span>
                    @endif
                    
            
      <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">

      {{csrf_field ()}}


            <div class="panel-body">
                        <div class="row">

                        

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div  class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                     <label for="nombre">Nombre<span style="color: red; font-weight: bold;" v-show="inscripciones.nombre==0">*</span></label>
                     <input  type="text" onkeyup="mayusculas(this);" v-model="inscripciones.nombre" name="nombre" class="form-control"  value="{{\Auth::user()->name}}" placeholder="Nombre..." >
                    <p style="color: red; font-weight: bold;" v-show="inscripciones.nombre==0">nombre es obligatorio</p>
                   </div>
                  </div>

                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                     <label for="apellido">Apellido<span style="color: red; font-weight: bold;" v-show="inscripciones.apellido==0">*</span></label>
                     <input type="text" onkeyup="mayusculas(this);"  v-model="inscripciones.apellido" name="apellido" class="form-control"  value="{{\Auth::user()->apellido}}" placeholder="Apellido..." >
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.apellido==0">apellido es obligatorio</p>

                   </div>
                  </div>

               



                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('doc_identidad') ? ' has-error' : '' }}">
                     <label for="doc_identidad">Numero del documento<span style="color: red; font-weight: bold;" v-show="inscripciones.doc_identidad==0">*</span></label>
                     <input type="number"  v-model="inscripciones.doc_identidad" name="doc_identidad" class="form-control"  value="{{\Auth::user()->num_documento}}" placeholder="Numero Documento..." >
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.doc_identidad==0">num documento es obligatorio</p>

                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
                     <label for="edad">Edad<span style="color: red; font-weight: bold;" v-show="inscripciones.edad==0">*</span></label>
                     <input type="number" v-model="inscripciones.edad" name="edad" class="form-control"  value="{{old('edad')}}" placeholder="Edad..." >
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.edad==0">edad es obligatorio</p>

                   </div>
                  </div>


                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('num_telefono') ? ' has-error' : '' }}">
                     <label for="num_telefono">Numero de telefono<span style="color: red; font-weight: bold;" v-show="inscripciones.num_telefono==0">*</span></label>
                     <input type="number" v-model="inscripciones.num_telefono" name="num_telefono" class="form-control"  value="{{\Auth::user()->telefono}}" placeholder="Numero de telefono..." >
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.num_telefono==0">telefono es obligatorio</p>

                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
                     <label for="ciudad">Ciudad<span style="color: red; font-weight: bold;" v-show="inscripciones.ciudad==0">*</span></label>
                     <input type="text" onkeyup="mayusculas(this);" v-model="inscripciones.ciudad" name="ciudad" class="form-control"  value="{{\Auth::user()->ciudad}}" placeholder="Ciudad de origen..." >
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.ciudad==0">ciudad es obligatorio</p>

                   </div>
                  </div>




                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Institucion Educativa<span style="color: red; font-weight: bold;" v-show="inscripciones.institucion_educatica==0">*</span></label>

                      <select v-model="inscripciones.institucion_educatica" name="institucion_educatica"  class="form-control" style="width: 100%;" >
                      <option selected="selected" value="">Seleccione</option>

                           @foreach($school as $school)
                          <option value="{{ $school->id }}">{{ $school->name }}</option>
                          @endforeach


                  </select>
                  <p style="color: red; font-weight: bold;" v-show="inscripciones.institucion_educatica==0">institución es obligatorio</p>

                   </div>
                   </div>



                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Predeterminado ponente <span style="color: red; font-weight: bold;" v-show="inscripciones.categoria==0">*</span></label>
                        <select v-model="inscripciones.categoria" name="categoria" class="form-control">
                        <option value="">Seleccione</option>
                        <option value="Asistente">Asistente</option>
                        <option value="Ponente">Ponente</option>
                        <option value="Sponsor">Sponsor</option>
                        </select>
                        <p style="color: red; font-weight: bold;" v-show="inscripciones.categoria==0">predeterminado ponente es obligatorio</p>

                   </div>
                   </div>


             <div class="col-lg-6 col-md-6 col-xs-12" v-if="inscripciones.categoria=='Ponente' || inscripciones.categoria=='Sponsor'">
                    <div class="form-group{{ $errors->has('titulo_ponencia') ? ' has-error' : '' }}">
                     <label for="titulo_ponencia">Titulo de ponente<span style="color: red; font-weight: bold;" v-show="inscripciones.titulo_ponencia==0">*</span></label>
                     <input type="text" onkeyup="mayusculas(this);" v-model="inscripciones.titulo_ponencia" name="titulo_ponencia" class="form-control"   value="{{old('titulo_ponencia')}}" placeholder="Titulo de ponencia...">
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.titulo_ponencia==0">Titulo de ponenencia es obligatorio</p>

                   </div>
                  </div>
                   
                        
                   
                     <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Tipo de sexo<span style="color: red; font-weight: bold;" v-show="inscripciones.sexo==0">*</span></label>
                        <select v-model="inscripciones.sexo" name="sexo" class="form-control">
                        <option value="">Seleccione</option>
                        <option value="mujer">MUJER</option>
                        <option value="hombre">HOMBRE</option>
                        </select>
                        <p style="color: red; font-weight: bold;" v-show="inscripciones.sexo==0">sexo es obligatorio</p>

                   </div>
                   </div>

                   



                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('alergia') ? ' has-error' : '' }}">
                     <label for="alergia" >Sufre alguna alergia<span style="color: red; font-weight: bold;" v-show="inscripciones.alergia==0">*</span></label>
                     <input v-model="inscripciones.alergia"  type="text" name="alergia" class="form-control"   value="{{old('alergia')}}" placeholder="Sufre alguna alergia...">
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.alergia==0">alergia es obligatorio</p>

                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>¿Requiere Housing? <span style="color: red; font-weight: bold;" v-show="inscripciones.housing==0">*</span></label>
                        <select v-model="inscripciones.housing" name="housing" class="form-control">
                        <option value="">Seleccione</option>
                        <option value="No">No</option>
                        <option value="Si">Si</option>
                        </select>
                        <p style="color: red; font-weight: bold;" v-show="inscripciones.housing==0">campo es obligatorio</p>

                   </div>
                   </div>
                   
                     <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('alergia') ? ' has-error' : '' }}">
                     <label for="emergencia">Numero Emergencia<span style="color: red; font-weight: bold;" v-show="inscripciones.emergencia==0">*</span></label>
                     <input v-model="inscripciones.emergencia"  type="number" name="emergencia" class="form-control"  value="{{old('emergencia')}}" placeholder="numero de un familiar...">
                     <p style="color: red; font-weight: bold;" v-show="inscripciones.emergencia==0">emergencia es obligatorio</p>

                   </div>
                  </div>
                
                <div class="col-lg-12 col-md-6 col-xs-12">
                   <div class="form-group{{ $errors->has('usodatos') ? ' has-error' : '' }}">
                    <p>
                    <span style="color: red; font-weight: bold;" v-show="inscripciones.usodatos==0">*</span>
                    Yo {{ Auth::user()->name }} {{ Auth::user()->apellido }} me hago responsable del uso de mis datos personales para el foro ciudad mas, autoriza el uso de mi imagen para publicaciones en redes sociales y sitio del evento.<br>
                    <input v-model="inscripciones.usodatos" type="checkbox" name="usodatos"  value="acepto">
                    
                </div>
              </div>
              
                   </div>
           <input v-model="inscripciones.id_usuario_ingresado" name="id_usuario_ingresado" type="hidden" >



            <div class="form-group">
            <img v-if="isLoaded==true" src="https://miro.medium.com/max/882/1*9EBHIOzhE1XfMYoKz1JcsQ.gif" alt="logo" width="220" height="110">
            <div v-if="isLoaded==false">

              <button class="btn btn-outline-info" @click="registerInscripcionEstudiante()" type="button">Guardar</button>
              <button class="btn btn-outline-danger" type="reset">Cancelar</button>
            </div>

            </form>            
    </div>


<script>
function mayusculas(e) {
    e.value = e.value.toUpperCase();
}


      </script>
	</div>
@endsection

