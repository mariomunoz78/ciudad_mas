<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-estado-{{$incrip->id}}">
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"><b>Datos de la inscripción</b></h3>
			</div>
			<div class="modal-body">
				<h4 class="modal-title"><b>Nombre: </b> {{ $incrip->nombre }}</h4>
				<h4 class="modal-title"><b>Apellido: </b> {{ $incrip->apellido }}</h4>
				<h4 class="modal-title"><b>Predeterminado ponente: </b>{{ $incrip->categoria }}</h4>
				<h4 class="modal-title"><b>Estado: </b>{{ $incrip->estado }}</h4>
				<h4 class="modal-title"><b>Tipo Documento: </b>{{ $incrip->doc_identidad }}</h4>
				<h4 class="modal-title"><b>Edad: </b>{{ $incrip->edad }}</h4>
				<h4 class="modal-title"><b>Telefono: </b>{{ $incrip->num_telefono }}</h4>
				<h4 class="modal-title"><b>Ciudad: </b>{{ $incrip->ciudad }}</h4>
				<h4 class="modal-title"><b>Institucion: </b>{{ $incrip->colegio->name }}</h4>
				<h4 class="modal-title"><b>Titulo de ponencia: </b>{{ $incrip->titulo_ponencia }}</h4>
				<h4 class="modal-title"><b>Alergia: </b>{{ $incrip->alergia }}</h4>
				<h4 class="modal-title"><b>Hosting: </b>{{ $incrip->housing }}</h4>
				<h4 class="modal-title"><b>Fecha de inscripción: </b>{{ $incrip->created_at }}</h4>
			</div>
			<hr/><hr/>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>

</div>