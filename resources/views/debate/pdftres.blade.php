<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Acción por el Clima</title>
</head>
<body>


	<div class="container">
     <div class="row row-cols-4">
          <div class="col"><b style="color: green">II Foro Ciudad más  Debates</b></div>
          <div class="col"><b>Monteria Cordoba  Debates</b></div>
          <div class="col"><b>Acción por el Clima<b></div>
       </div>
    </div><br>


	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					
					
				</thead>
				<tbody>
                   @foreach ($incripcion as $insc)          
                   	@if($insc->debate=='Acción por el Clima') 
                   	<tr> 
				    <td>{{ $insc->doc_identidad}}</td> 
					<td>{{ $insc->nombre}}</td>
					<td>{{ $insc->apellido}}</td>
					<td>{{ $insc->num_telefono}}</td>
					<td>{{ $insc->institucion_educatica}}</td>
					<td>{{ $insc->ciudad}}</td>
				   
				@endif
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>