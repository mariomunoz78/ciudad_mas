@extends('layouts.adminhome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3><b>Igualdad de género</b></h3>
		<a href="{{ route('debatecuatro.pdf') }}"><img data-toggle="tooltip" title="Descargar archivo PDF" src="{{asset('aplica/img/pdf.png')}}" height="50" width="50"></a>
		<div class="form-group">
       <input type="text" class="form-control pull-right" style="width:70%" id="search" placeholder="Buscar...">
    </div>

	</div>
</div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table-bordered table pull-right" id="mytable" cellspacing="0" style="width: 100%;">
				<thead>
					<th>Id</th>
					<th>Identidad</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Telefono</th>
					<th>Direccion</th>
					<th>Opcion</th>
				</thead>
               @foreach ($debatecuatro as $usu)
                   	<tr> 
				    <td>{{ $usu->id}}</td>
				    <td>{{ $usu->doc_identidad}}</td>
					<td>{{ $usu->nombre}}</td>
					<td>{{ $usu->apellido}}</td>
					<td>{{ $usu->num_telefono}}</td>
					<td>{{ $usu->direccion}}</td>

					<td>
						<a href="https://api.whatsapp.com/send?phone=57{{ $usu->num_telefono}}&text="> <img src="{{asset('/aplica/img/whatsapp.jpg')}} " style="width: 50px; height: 50px"  ></a>
						<a  href="{{ route('debateuno',array($usu->id, $parametro=4)) }}"><img data-toggle="tooltip" title="{{ $usu->nombre}} {{ $usu->apellido}}" src="{{asset('/aplica/img/editar.png')}}"  width="50" height="50"></a>
						
					</td>
					</tr>
				 
                
				@endforeach
			</table>
		</div>
		{{$debatecuatro->render()}}  <!-- estom es lo que me va a mostrar la lista por categoria-->
	</div>
</div>
<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>

@endsection  
<!--aqui finaliza la session-->