@extends('layouts.adminhome')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3><b>Formulario de Inscripcion</b></h3>
            <p>{{ Auth::user()->name }} esta informacion se enviara a su correo electronico {{ Auth::user()->email }}</p>
            
             @if ($errors->has('id_usuario_ingresado'))
                        <span class="help-block">
                                    <strong style="color: red"><H1>Ya usted esta inscrito en ciudadMas verifique</H1></strong>
                      </span>
                    @endif
                    
			

			{!!Form::open(array('url'=>'/inscripcion/evento','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
           

            <div class="panel-body">
                        <div class="row">

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div  class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                     <label for="nombre">Nombre</label>
                     <input type="text" name="nombre" class="form-control"  value="{{\Auth::user()->name}}" placeholder="Nombre...">

                      @if ($errors->has('nombre'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('nombre') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>

                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                     <label for="apellido">Apellido</label>
                     <input type="text" name="apellido" class="form-control"  value="{{\Auth::user()->apellido}}" placeholder="Apellido...">

                      @if ($errors->has('apellido'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('apellido') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('doc_identidad') ? ' has-error' : '' }}">
                     <label for="doc_identidad">Numero del documento</label>
                     <input type="number" name="doc_identidad" class="form-control"  value="{{old('doc_identidad')}}" placeholder="Numero Documento...">

                      @if ($errors->has('doc_identidad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('doc_identidad') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
                     <label for="edad">Edad</label>
                     <input type="number" name="edad" class="form-control"  value="{{old('edad')}}" placeholder="Edad...">

                      @if ($errors->has('edad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('edad') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>


                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('num_telefono') ? ' has-error' : '' }}">
                     <label for="num_telefono">Numero de telefono</label>
                     <input type="number" name="num_telefono" class="form-control"  value="{{old('num_telefono')}}" placeholder="Numero de telefono...">

                      @if ($errors->has('num_telefono'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('num_telefono') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
                     <label for="ciudad">Ciudad</label>
                     <input type="text" name="ciudad" class="form-control"  value="{{old('ciudad')}}" placeholder="Ciudad de origen...">

                      @if ($errors->has('ciudad'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('ciudad') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('institucion_educatica') ? ' has-error' : '' }}">
                     <label for="institucion_educatica">Institucion Educativa</label>
                     <input type="text" name="institucion_educatica" class="form-control"  value="{{old('institucion_educatica')}}" placeholder="Institucion Educativa...">

                      @if ($errors->has('institucion_educatica'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('institucion_educatica') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>

                   <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>Categoria</label>
                        <select name="categoria" class="form-control">
                        <option value="Asistente">Asistente</option>
                        <option value="Ponente">Ponente</option>
                        <option value="Sponsor">Sponsor</option>
                        </select>
                   </div>
                   </div>
              
                   
                   


             <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('titulo_ponencia') ? ' has-error' : '' }}">
                     <label for="titulo_ponencia">Titulo de ponente</label>
                     <input type="text" name="titulo_ponencia" class="form-control"  value="{{old('titulo_ponencia')}}" placeholder="Titulo de ponencia...">

                      @if ($errors->has('titulo_ponencia'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('titulo_ponencia') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('alergia') ? ' has-error' : '' }}">
                     <label for="alergia">Sufre alguna alergia</label>
                     <input type="text" name="alergia" class="form-control"  value="{{old('alergia')}}" placeholder="Sufre alguna alergia...">
                     
                      @if ($errors->has('alergia'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('alergia') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>
                  


                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group">
                      <label>¿Requiere Housing?</label>
                        <select name="housing" class="form-control">
                        <option value="No">No</option>
                        <option value="Si">Si</option>
                        </select>
                   </div>
                   </div>
                   
                     <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group{{ $errors->has('alergia') ? ' has-error' : '' }}">
                     <label for="emergencia">Numero Emergencia</label>
                     <input type="text" name="emergencia" class="form-control"  value="{{old('emergencia')}}" placeholder="numero de un familiar...">
                      @if ($errors->has('emergencia'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('emergencia') }}</strong>
                      </span>
                    @endif
                   </div>
                  </div>
                
                <div class="col-lg-6 col-md-6 col-xs-12">
                   <div class="form-group{{ $errors->has('usodatos') ? ' has-error' : '' }}">
                  <p>Yo {{ Auth::user()->name }} {{ Auth::user()->apellido }} me hago responsable del uso de mis datos personales para el foro ciudad mas, autoriza el uso de mi imagen para publicaciones en redes sociales y sitio del evento.
                    <input type="checkbox" name="usodatos" value="acepto">
                  </p>
                 
                    @if ($errors->has('usodatos'))
                        <span class="help-block">
                                    <strong style="color: red">{{ $errors->first('usodatos') }}</strong>
                      </span>
                    @endif
                </div>
              </div>
              
                   </div>

                <input name="id_usuario_ingresado" type="hidden" value="{{ \Auth::user()->id }}">

            <div class="form-group">
              <button class="btn btn-info" type="submit">Guardar</button>
              <button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

      {!!Form::close()!!}   
            
    </div>
	</div>
@endsection