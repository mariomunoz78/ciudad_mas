<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-confirm-{{$incrip->id}}"> 
	{{Form::Open(array('action'=>array('IncripcionController@destroy',$incrip->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <b><h4 class="modal-title">{{$incrip->nombre}} {{$incrip->apellido}} </h4></b>
			</div>
			<div class="modal-body">
				@if ($incrip->estado=='Pago')
				<h3>Confirme si desea modificar el estado de la cuenta de pago a <B>MORA</B></h3>
				@endif
				@if ($incrip->estado=='Proceso')
				<h3>confirmar estado de la cuenta, en mora a <B>PAGADO</B></h3>
				@endif
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-success">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>