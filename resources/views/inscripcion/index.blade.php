@extends('layouts.adminsinvue')  
@section ('contenido')

<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea peque�0�9o que me muestre todo la pantalla completa-->
		
		<h3>Incripciones <a href="/inscripcion/evento/create"><button class="btn btn-success">Nuevo</button></a></h3>
	</div>
</div>



@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif

@if(session('pago'))
<div class="alert alert-success" role="alert">
  {{session('pago')}}
</div>
@endif

@if(session('nopagar'))
<div class="alert alert-danger" role="alert">
  {{session('nopagar')}}
</div>
@endif




<div class="container">
        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>

							<th>Id</th>
							<th>Documento</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Categoria</th>
							<th>Ponencia</th>
							
							<th>whatsApp</th>
							<!--
							<th>Estado</th>
							-->
							<td>Colegio</td>
							<th>Fecha de inscripcion</th>
							<th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
						@foreach ($inscripcion as $incrip)
                 <!--hago una condicion que solo puede ver todas las personas registrada el administrador -->
					<tr> 
					<td>{{ $incrip->id_usuario_ingresado}}</td>
				    <td>{{ $incrip->doc_identidad}}</td>
					<td>{{ $incrip->nombre}}</td>
					<td>{{ $incrip->apellido}}</td>
					<td>{{ $incrip->categoria }}</td>
					<td>{{ $incrip->titulo_ponencia }}</td>
					<td>
						<a href="https://api.whatsapp.com/send?phone=57{{ $incrip->emergencia}}&text="> <img src="{{asset('/aplica/img/whatsapp.jpg')}} " style="width: 50px; height: 50px"  ></a>
						
					</td>
					<!--
					@if ($incrip->estado=='Pago')
					 <td><a href="" data-target="#modal-confirm-{{$incrip->id}}" data-toggle="modal"><button data-toggle="tooltip" title="Presione el boton para desactivar Articulo" class="btn btn-success">Pagado</button></a></td>
					 @include('inscripcion.modalconfirmar')
                     @endif
                     @if ($incrip->estado=='Proceso')
					 <td><a href="" data-target="#modal-confirm-{{$incrip->id}}" data-toggle="modal"><button data-toggle="tooltip" title="Presione el boton para activar Articulo" class="btn btn-danger">Sin pagar</button></a></td>
					 @include('inscripcion.modalconfirmar')
                     @endif
					 -->
					 <td>{{ $incrip->colegio->name }}</td>
					<td>{{ $incrip->created_at}}</td>
					<td>
					<a  class="btn btn-outline-info" href="" data-target="#modal-estado-{{$incrip->id}}" data-toggle="modal" style="color:blue"><i class="far fa-eye"></i></a>
						<a class="btn btn-outline-success"  href="{{URL::action('IncripcionController@edit',$incrip->id)}}" style="color:green"><i class="fas fa-edit"></i></a>
					</td>
				</tr>
               @include('inscripcion.modal')
				@endforeach
			                     
                        </tbody>        
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>



	@endsection  

<!--aqui finaliza la session-->