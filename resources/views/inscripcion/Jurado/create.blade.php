@extends('layouts.adminhome')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3><b>Crear Jurado Foro ciudad mas</b></h3>
 			
         @if(session('okmario'))
            <div class="alert alert-success" role="alert">
            {{session('okmario')}}
            </div>
            @endif

			 {!!Form::open(array('url'=>'/jurado/ciudad','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
           

           <div class="form-group row">
                   <div class="col-md-6">
                    <label for="name" >Nombre</label>
                                <input id="name" type="text" class="form-control form-control-user{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  autofocus placeholder="Nombre...">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>


                  <div class="col-sm-6">
                    <label>Apellido</label>
                    <input id="apellido" type="text" class="form-control form-control-user{{ $errors->has('apellido') ? ' is-invalid' : '' }}" name="apellido" value="{{ old('apellido') }}"  autofocus placeholder="Apellido...">

                    @if ($errors->has('apellido'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                @endif
                  </div>
                </div>
                 

                <div class="form-group row">

                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <label>Email</label>
                      <input id="email" type="email" class="form-control form-control-user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus placeholder="Email...">
                         @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                  </div>

                   <div class="col-sm-6">
                    <label>Telefono</label>
                    <input id="telefono" type="number" class="form-control form-control-user{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}"  autofocus placeholder="Telefono...">
                    @if ($errors->has('telefono'))
                            <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                      @endif
              </div>

           
            </div>
          

            <div class="form-group row">
              <div class="col-sm-6">
                    <label>Numero de Documento</label>
                    <input id="num_documento" type="number" class="form-control form-control-user{{ $errors->has('num_documento') ? ' is-invalid' : '' }}" name="num_documento" value="{{ old('num_documento') }}"  autofocus placeholder="Numero de Identidad...">

                    @if ($errors->has('num_documento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('num_documento') }}</strong>
                                    </span>
                             @endif
              </div>

                <div class="col-sm-6">
                    <label>Fecha de Nacimiento</label>
                    <input id="fecha_nacimiento" type="date" class="form-control form-control-user{{ $errors->has('fecha_nacimiento') ? ' is-invalid' : '' }}" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}"  autofocus >
                    

                    @if ($errors->has('fecha_nacimiento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                    </span>
                             @endif
              </div>
              </div>

               <div class="form-group row">
              <div class="col-sm-6">
                    <label>Ciudad</label>
                     <input id="ciudad" type="text" class="form-control form-control-user{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="ciudad" value="{{ old('ciudad') }}"  autofocus placeholder="Ciudad...">

                    @if ($errors->has('ciudad'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ciudad') }}</strong>
                                    </span>
                             @endif
              </div>

                <div class="col-sm-6">
                    <label>Direcci��n</label>
                    <input id="direccion" type="text" class="form-control form-control-user{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}"  autofocus placeholder="Direcci��n...">

                    @if ($errors->has('direccion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                             @endif
              </div>
              </div>


               <div class="form-group row">

                    <div class="col-md-6">
                    <label for="name" >Salon del profesor</label>
                                <input id="salonjurado" type="text" class="form-control form-control-user{{ $errors->has('salonjurado') ? ' is-invalid' : '' }}" name="salonjurado" value="{{ old('salonjurado') }}"  autofocus placeholder="Salon 1-2-3-4...">

                                @if ($errors->has('salonjurado'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('salonjurado') }}</strong>
                                    </span>
                                @endif
                            </div>


              <div class="col-sm-6">
                    <label>Password</label>
                   <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password...">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              </div>

                <div class="col-sm-6">
                    <label>Confirmar su password</label>
                   <input id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" required placeholder="Repita su Password">
              </div>
              </div>
       
              <button type="submit" class="btn btn-primary btn-user btn-block">Registrar Jurado</button>

                <hr>

      {!!Form::close()!!}   
            
    </div>
	</div>
@endsection