<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$jura->id}}">
	{{Form::Open(array('action'=>array('Jurado@destroy',$jura->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body">
				<b><h3>Confirme si desea Eliminar al Jurado {{ $jura->name }} {{ $jura->apellido }} </h3></b>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>