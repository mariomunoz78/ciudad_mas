@extends('layouts.adminhome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3><b>Jurados Del Foro Ciudad+ 27, 28 y 29 de febrero</b></h3>
	</div>
</div> <!--jurado/ciudad/create -->
<h3><a href="/jurado/ciudad/create"><button class="btn btn-success">Nuevo</button></a></h3>
<div class="alert alert-danger" role="alert">
  MODULO DE JURADO PARA CALIFICACIONES EN PERIODO DE PRUEBA
</div>
<div class="form-group">
       <input type="text" class="form-control pull-right" style="width:70%" id="search" placeholder="Buscar...">
    </div>

@if(session('mario'))
<div class="alert alert-success" role="alert">
  {{session('mario')}}
</div>
@endif

 @if(session('eliminado'))
            <div class="alert alert-danger" role="alert">
            {{session('eliminado')}}
            </div>
       @endif

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table-bordered table pull-right" id="mytable" cellspacing="0" style="width: 100%;">
				<thead>
					<th>Cedula</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Telefono</th>
					<th>Salon</th>
					<th>Opciones</th>
				</thead>
               @foreach ($jurado as $jura)
                   	<tr> 
				    <td>{{ $jura->num_documento}}</td>
					<td>{{ $jura->name}}</td>
					<td>{{ $jura->apellido}}</td>
					<td>{{ $jura->telefono}}</td>
					<td>{{ $jura->salonjurado}}</td>
					

					 <td>
					 	<a href="" data-target="#modal-estado-{{$jura->id}}" data-toggle="modal"><img  data-toggle="tooltip" title="Detalle del Jurado" src="{{asset('/aplica/img/detalle.png')}}"  width="30" height="30"></a>
					 		<a  href=""><img data-toggle="tooltip" title="Editar Jurado" src="{{asset('/aplica/img/editar.png')}}"  width="30" height="30"></a>
					 	<a href="" data-target="#modal-delete-{{$jura->id}}" data-toggle="modal"><img data-toggle="tooltip" title="Eliminar  Jurado" src="{{asset('/aplica/img/mmmmm.png')}}"  width="30" height="30"></a>
					 </td>
				</tr>
				@include('inscripcion.Jurado.modal')
               @include('inscripcion.Jurado.detalle')
				@endforeach
			</table>
		</div>
	</div>
</div>

<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>


@endsection  
<!--aqui finaliza la session-->