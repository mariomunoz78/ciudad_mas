<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-estado-{{$jura->id}}">
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"><b>Datos del Jurado</b></h3>
			</div>
			<div class="modal-body">
			    
				<h4 class="modal-title"><b>Salón asignado: </b> {{ $jura->salonjurado }}</h4>
				<h4 class="modal-title"><b>Nombre: </b> {{ $jura->name }}</h4>
				<h4 class="modal-title"><b>Apellido: </b> {{ $jura->apellido }}</h4>
				<h4 class="modal-title"><b>Documento {{ $jura->tipo_documento }}: </b> {{ $jura->num_documento }}</h4>
				<h4 class="modal-title"><b>Ciudad: </b> {{ $jura->ciudad }}</h4>
				<h4 class="modal-title"><b>Fecha de Nacimiento: </b> {{ $jura->fecha_nacimiento }}</h4>
				<h4 class="modal-title"><b>Direccion: </b> {{ $jura->direccion }}</h4>
				<h4 class="modal-title"><b>Correo: </b> {{ $jura->email }}</h4>
            </div>
			<hr/><hr/>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>

</div>