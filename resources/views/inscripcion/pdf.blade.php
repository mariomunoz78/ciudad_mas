<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>estudiante pdf</title>
</head>
<body>


	<div class="container">
     <div class="row row-cols-4">
          <div class="col"><b style="color: green">II Foro Ciudad más</b></div>
          <div class="col"><b>Monteria Cordoba</b></div>
          <div class="col"><b>Estudiantes Registrados<b></div>
       </div>
    </div><br>


	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Identidad</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Tipo usuario</th>
					<th>Edad</th>
					<th>Telefono</th>
					<th>Ciudad</th>
					<td>Institucion</td>
					<td>Categoria</td>
					<td>Tituloponencia</td>
					
					
				</thead>
				<tbody>
               @foreach ($incripcion as $inc)          
                   	<tr> 
					<td>{{ $inc->doc_identidad}}</td>
				    <td>{{ $inc->nombre}}</td> 
					<td>{{ $inc->apellido}}</td>
					<td>{{ $inc->tipo_persona}}</td>
					<td>{{ $inc->edad}}</td>
					<td>{{ $inc->num_telefono}}</td>
					<td>{{ $inc->ciudad}}</td>
					<td>{{ $inc->institucion_educatica}}</td>
					<td>{{ $inc->categoria}}</td> 
					<td>{{ $inc->titulo_ponencia}}</td>
				
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>