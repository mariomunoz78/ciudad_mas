<!DOCTYPE html>
<html lang="en">
 <link href="{{asset('aplica/img/escudo.png')}}" type="image/x-icon" rel="shortcut icon" />
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>III FORO CIUDAD - MONTERÍA - CÓRDOBA</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('aplica/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('aplica/css/sb-admin-2.min.css')}}" rel="stylesheet">
  <!--
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center">
       
        <div class="sidebar-brand-text mx-3"><img src="https://static.wixstatic.com/media/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.png/v1/fill/w_259,h_80,al_c,q_85,usm_0.66_1.00_0.01/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.webp" height="75px" width="220px"></div>
      </a>






      <!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('aplica/datatables/bootstrap/css/bootstrap.min.css')}}">
    <!-- CSS personalizado --> 
    <link rel="stylesheet" href="{{asset('aplica/datatables/new/main.css')}}">  
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="{{asset('aplica/datatables/datatables/datatables.min.css')}}"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="{{asset('aplica/datatables/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
           
    <!--font awesome con CDN-->  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">  
      





    

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
          <a class="nav-link" href="{{url('/home')}}">
          <i class="fas fa-home"></i>
          <span>Inicio</span></a>
      </li>

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('/inscripcionre/showStudentInscrito')}}">
          <i class="fas fa-users"></i>
          <span>Usuarios Inscritos al foro</span>
        </a>
      </li>



      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('/inscripcionStudy/showponencias')}}">
          <i class="fas fa-book"></i>
          <span>Ponencia</span>
        </a>
      </li>



       <hr class="sidebar-divider">
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users" aria-hidden="true"></i>
          <span>Usuarios Registrados</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Selecciona:</h6>
            <a class="collapse-item" href="{{url('usuarios')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padres')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesores')}}">Profesores</a>
            <a class="collapse-item" href="{{url('admin')}}">Administradores</a>
          </div>
        </div>
      </li>
            <!-- Divider -->
      <hr class="sidebar-divider">

        <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-money-check-alt text-gray-900"></i>
          <span>Usuarios Pagos</span>
        </a>
        <div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantespagos')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padrespagos')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesorespagos')}}">Profesores</a>
          </div>
        </div>
      </li>
      <!--

       <hr class="sidebar-divider">
        <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemario" aria-expanded="true" aria-controls="collapsemario">
          <i class="fas fa-money-check-alt text-gray-900"></i> 
          <span>Usuario en mora</span>
        </a>
        <div id="collapsemario" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantesnopagos')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padresnopagos')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesoresnopagos')}}">Profesores</a>
          </div>
        </div>
      </li>
-->
      
      <hr class="sidebar-divider">
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemar" aria-expanded="true" aria-controls="collapsemar">
         <i class="fas fa-ban"></i>
          <span>Proceso  Incompleto</span>
        </a>
        <div id="collapsemar" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantesincompleta')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padreincompleta')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profeincompleta')}}">Profesores</a>
          </div>
        </div>
      </li>
      
      <hr class="sidebar-divider">
          <!-- Divider -->
      <!--
        <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('jurado/ciudad')}}">
          <i class="fas fa-street-view"></i>
          <span>Jurados</span>
        </a>
      </li>
      -->


      <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('editarweb/index')}}">
         <i class="far fa-edit"></i>
          <span>Editar Pagina web</span>
        </a>
      </li>

      <hr class="sidebar-divider">
          <!-- Divider -->
         <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('inscripcionre/school')}}">
          <i class="fas fa-school"></i>
          <span>School</span>
        </a>
      </li>



      <hr class="sidebar-divider">
          <!-- Divider -->
         <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('mensajes')}}">
          <i class="fas fa-comments"></i>
          <span>Mensajes</span>
        </a>
      </li>
       

    </ul>
    
       <hr class="sidebar-divider">
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
        

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                <img class="img-profile rounded-circle" src="{{asset('aplica/img/escudo.png')}}">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" data-toggle="modal" data-target="#exampleModaladmin">


                  <i class="fas fa-camera"></i>
                  Foto
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt"></i>
                  Salir
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

      
          <!-- Content Row -->
                  <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>George's Noble School Monteria - Cordoba</b></h3>
                 
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                              <!--Contenido-->




                          <!-- Modal -->
                          <div class="modal fade" id="exampleModaladmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="documente">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Agregar foto de perfil</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                <div class="form-group">
                                              <label class="col-md-12 form-control-label" for="text-input">Foto de perfil</label>
                                            <div class="col-md-12">
                                                <input type="file"  class="form-control">
                                            </div>
                                        </div>

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>
                                  <button type="button" class="btn btn-primary">Guardar</button>
                                </div>
                              </div>
                            </div>
                          </div>





                              
                              @yield('contenido')
                              <!--Fin Contenido-->
                              
                           </div>
                        </div>
                        
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
             
          <div class="row">
          <!-- Content Row -->
          <div class="row">

            <div class="col-lg-6 mb-4">
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer --><br><br><br><br><br><br><br><br><br>
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Mario Muñoz</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Desea cerrar sesion  <h4><b>&nbsp;&nbsp; {{ Auth::user()->name }}&nbsp;</b></h4>?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">{{ Auth::user()->name }} Seleccione  "Salir" a continuación si está listo para finalizar su sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir</a>

           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>


        </div>
      </div>
    </div>
  </div>




<!-- jQuery, Popper.js, Bootstrap JS -->
<script src="{{asset('aplica/datatables/jquery/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('aplica/datatables/popper/popper.min.js')}}"></script>
    <script src="{{asset('aplica/datatables/bootstrap/js/bootstrap.min.js')}}"></script>
      
    <!-- datatables JS -->
    <script type="text/javascript" src="{{asset('aplica/datatables/datatables/datatables.min.js')}}"></script>    
     
    <!-- para usar botones en datatables JS -->  
    <script src="{{asset('aplica/datatables/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js')}}"></script>  
    <script src="{{asset('aplica/datatables/datatables/JSZip-2.5.0/jszip.min.js')}}"></script>    
    <script src="{{asset('aplica/datatables/datatables/pdfmake-0.1.36/pdfmake.min.js')}}"></script>    
    <script src="{{asset('aplica/datatables/datatables/pdfmake-0.1.36/vfs_fonts.js')}}"></script>
    <script src="{{asset('aplica/datatables/datatables/Buttons-1.5.6/js/buttons.html5.min.js')}}"></script>
     
    <!-- código JS propìo-->    
    <script type="text/javascript" src="{{asset('aplica/datatables/new/main.js')}}"></script>  
    










  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('aplica/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('aplica/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('aplica/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  <script src="{{asset('aplica/vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts 

  -->

</body>

</html>

   