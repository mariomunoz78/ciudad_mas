<!DOCTYPE html>
<html lang="en">
 <link href="{{asset('aplica/img/escudo.png')}}" type="image/x-icon" rel="shortcut icon" />
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>George's Noble School</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('aplica/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('aplica/css/sb-admin-2.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center">
       
        <div class="sidebar-brand-text mx-3"><img src="https://static.wixstatic.com/media/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.png/v1/fill/w_259,h_80,al_c,q_85,usm_0.66_1.00_0.01/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.webp" height="75px" width="220px"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/home')}}">
          <i class="fas fa-home"></i>
          <span>Inicio</span></a>
      </li>

      <hr class="sidebar-divider">

          <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('/inscripcion/estudiante')}}">
          <i class="fas fa-users"></i>
          <span>Incripciones</span>
        </a>
      </li>

       <hr class="sidebar-divider">
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" title="Actividades proximas">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">0</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                 Proximas Actividades
                </h6>
             
                <a class="dropdown-item text-center small text-gray-500" href="#">No tiene actividades {{ Auth::user()->name }}</a>
               
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter" data-toggle="tooltip" title="Eventos proximos">1</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Eventos proximos 
                </h6>

                <a class="dropdown-item text-center small text-gray-500" href="#">{{ Auth::user()->name }} III foro<b> ciudad+</b><br>19 de marzo -2021 Monteria Cordoba</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
             

             
               @if (Auth::user()->foto!="")
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>       
                <img class="img-profile rounded-circle" src="{{asset('/aplica/img/fotos/'.Auth::user()->foto)}}">
              </a>
              @elseif(Auth::user()->foto=="")


                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>       
                <img class="img-profile rounded-circle" src="https://static.wixstatic.com/media/926009_74d7a1adc9aa40dea86a231853552ea8~mv2_d_1506_1523_s_2.png/v1/fill/w_398,h_400,al_c,q_85,usm_0.66_1.00_0.01/LOGO%20CIUDAD%20%2B.webp">
              </a>
              @endif
             

              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              

              <a class="dropdown-item" href="#">
                  <i class="fas fa-camera"></i>
                  Foto
                </a>


                


                

                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" style="color: red">
                  <i class="fas fa-sign-out-alt"></i>
                  Salir
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

      
          <!-- Content Row -->
                  <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>George's Noble School Monteria - Cordoba</b></h3>
                 
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="appestu">
                    <div class="row">
                      <div class="col-md-12">
                              <!--Contenido-->
                              
                              @yield('contenido')



                              
                              <!--Fin Contenido-->
                           </div>
                        </div>
                        
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
          <div class="row">
          <!-- Content Row -->
          <div class="row">


            <div class="col-lg-6 mb-4">

          

           

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


      <!-- Footer --><br><br><br><br><br><br><br><br><br>
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Mario Muñoz</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Desea cerrar sesion  <h4><b>&nbsp;&nbsp; {{ Auth::user()->name }}&nbsp;</b></h4>?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">{{ Auth::user()->name }} Seleccione  "Salir" a continuación si está listo para finalizar su sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir</a>

           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>


        </div>
      </div>
    </div>
  </div>



  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
  <script src="/componenteEstudiante.js"></script>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('aplica/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('aplica/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('aplica/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('aplica/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  <script src="{{asset('aplica/vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts 
  <script src="{{asset('aplica/js/demo/chart-area-demo.js')}}"></script>
  <script src="{{asset('aplica/js/demo/chart-pie-demo.js')}}"></script>
  -->

</body>

</html>

   