<!DOCTYPE html>
<html lang="en">
 <link href="{{asset('aplica/img/escudo.png')}}" type="image/x-icon" rel="shortcut icon" />
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>George's Noble School</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('aplica/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('aplica/css/sb-admin-2.min.css')}}" rel="stylesheet">
  <!--
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  -->

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center">
       
        <div class="sidebar-brand-text mx-3"><img src="https://static.wixstatic.com/media/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.png/v1/fill/w_259,h_80,al_c,q_85,usm_0.66_1.00_0.01/926009_30742654d77f48b996d6f426b9bdc218~mv2_d_4135_1433_s_2.webp" height="75px" width="220px"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
          <a class="nav-link" href="{{url('/home')}}">
          <i class="fas fa-home"></i>
          <span>Inicio</span></a>
      </li>

      <hr class="sidebar-divider">

          <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('/inscripcionre/showStudentInscrito')}}">
          <i class="fas fa-users"></i>
          <span>Usuarios Inscritos al foro</span>
        </a>
      </li>

       <hr class="sidebar-divider">
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users" aria-hidden="true"></i>
          <span>Usuarios Registrados</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Selecciona:</h6>
            <a class="collapse-item" href="{{url('usuarios')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padres')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesores')}}">Profesores</a>
            <a class="collapse-item" href="{{url('admin')}}">Administradores</a>
          </div>
        </div>
      </li>
            <!-- Divider -->
      <hr class="sidebar-divider">

        <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-money-check-alt text-gray-900"></i>
          <span>Usuarios Pagos</span>
        </a>
        <div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantespagos')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padrespagos')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesorespagos')}}">Profesores</a>
          </div>
        </div>
      </li>

      <!--
       <hr class="sidebar-divider">

        <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemario" aria-expanded="true" aria-controls="collapsemario">
          <i class="fas fa-money-check-alt text-gray-900"></i> 
          <span>Usuario en mora</span>
        </a>
        <div id="collapsemario" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantesnopagos')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padresnopagos')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profesoresnopagos')}}">Profesores</a>
          </div>
        </div>
      </li>

-->
      
      <hr class="sidebar-divider">
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemar" aria-expanded="true" aria-controls="collapsemar">
         <i class="fas fa-ban"></i>
          <span>Proceso  Incompleto</span>
        </a>
        <div id="collapsemar" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('estudiantesincompleta')}}">Estudiantes</a>
            <a class="collapse-item" href="{{url('padreincompleta')}}">Padres de familia</a>
            <a class="collapse-item" href="{{url('profeincompleta')}}">Profesores</a>
          </div>
        </div>
      </li>
      
      <hr class="sidebar-divider">
          <!-- Divider -->
      <!--
        <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('jurado/ciudad')}}">
          <i class="fas fa-street-view"></i>
          <span>Jurados</span>
        </a>
      </li>
      -->


      <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('editarweb/index')}}">
         <i class="far fa-edit"></i>
          <span>Editar Pagina web</span>
        </a>
      </li>

      <hr class="sidebar-divider">
          <!-- Divider -->
         <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('inscripcionre/school')}}">
          <i class="fas fa-comments"></i>
          <span>School</span>
        </a>
      </li>


      <hr class="sidebar-divider">
          <!-- Divider -->
         <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('mensajes')}}">
          <i class="fas fa-comments"></i>
          <span>Mensajes</span>
        </a>
      </li>
       

    </ul>
    
       <hr class="sidebar-divider">
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
        

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                <img class="img-profile rounded-circle" src="{{asset('aplica/img/escudo.png')}}">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" data-toggle="modal" data-target="#exampleModaladmin">


                  <i class="fas fa-camera"></i>
                  Foto
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt"></i>
                  Salir
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

      
          <!-- Content Row -->
                  <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>George's Noble School Monteria - Cordoba</b></h3>
                 
                </div>
                <!-- /.box-header -->
                <div class="box-body" >
                    <div class="row">
                      <div class="col-md-12">
                              <!--Contenido-->




                          <!-- Modal -->
                          <div class="modal fade" id="exampleModaladmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Agregar foto de perfil</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                <div class="form-group">
                                              <label class="col-md-12 form-control-label" for="text-input">Foto de perfil</label>
                                            <div class="col-md-12">
                                                <input type="file"  class="form-control">
                                            </div>
                                        </div>

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>
                                  <button type="button" class="btn btn-primary">Guardar</button>
                                </div>
                              </div>
                            </div>
                          </div>

<!--empieza-->



 <!-- Earnings (Monthly) Card Example -->
 @if (empty($mario->id))
                <div class="alert alert-danger" role="alert">
                <h2>  {{ Auth::user()->name }} {{ Auth::user()->apellido }} falta por inscribirte al foro ciudad mas, <b>INGRESE AL MODULO INSCRIPCIONES, NUEVO</b></h2>
                  </div>
                    
                     @elseif($mario->id==Auth::user()->id)
                   <div class="alert alert-success" role="alert">
                <h2>  {{ Auth::user()->name }} {{ Auth::user()->apellido }} Usted esta <b>Inscrito </b> en el foro ciudad mas</h2>
                  </div>
          @endif

  <div class="container-fluid">
          <!-- Content Row -->




          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Administradores registradores</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $administradores->total }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>


               <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Estudiantes registrados</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $estudiante->total }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        

               <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Padre de Familia registrados</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $padre->total }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

                <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Profesores Registrados</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $profesores->total }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>





         <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Recaudo administradores</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ ($aministradordinero->dineroadminis*150000) }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

               <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Recaudado estudiante</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $estudiantedinero->dineroestudiante*150000 }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        

               <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Recaudo padres de familia</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $padredinero->dineroestudiante*150000 }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Recaudo Profesores</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $profedinero->dineroadminis*150000 }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>


          <!--VERIFICAR CUANTOS ESTUDIANTES HAN PAGADO EL FORO-->
          
           <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1"># administradores Pagos</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ ($aministradordinero->dineroadminis) }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

               <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1"><a href="{{url('estudiantespagos')}}"># Estudiantes Pagos</a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $estudiantedinero->dineroestudiante }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        

               <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1"><a href="{{url('padrespagos')}}"># Padres de familia Pagos</a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $padredinero->dineroestudiante }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1"><a href="{{url('profesorespagos')}}"># Profesores Pagos</a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $profedinero->dineroadminis }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-money-check-alt text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

          <!-- FINAL DEL USUARIOS PAGOS-->


          <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Administradores inscrito al foro Ciudadmas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ ($inscritoadmin->dineroadminis) }}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-database text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

               <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Estudiantes inscritos al foro Ciudadmas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $inscritoestu->dineroestudiante}}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-database text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        

               <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">padres de familia Incrito al foro Ciudadmas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $inscritopadre->dineroestudiante}}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-database text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

               <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-black text-uppercase mb-1">Profesores Incrito al foro Ciudadmas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><h3><b>{{ $inscritoprofe->dineroadminis}}</b></h3></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-database text-gray-900"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>



                              
             <canvas id="myChart" width="400" height="400"></canvas>
                <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
                              <!--Fin Contenido-->
                              
                           </div>
                        </div>
                        
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
             
          <div class="row">
          <!-- Content Row -->
          <div class="row">

            <div class="col-lg-6 mb-4">
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer --><br><br><br><br><br><br><br><br><br>
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Mario Muñoz</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Desea cerrar sesion  <h4><b>&nbsp;&nbsp; {{ Auth::user()->name }}&nbsp;</b></h4>?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">{{ Auth::user()->name }} Seleccione  "Salir" a continuación si está listo para finalizar su sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir</a>

           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>


        </div>
      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
  <script src="/componente.js"></script>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('aplica/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('aplica/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('aplica/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('aplica/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  <script src="{{asset('aplica/vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts 

  -->

</body>

</html>

   
