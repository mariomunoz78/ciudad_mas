@extends('layouts.adminhome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3 class="btn btn-danger"><b>Padres de familia en mora</b></h3>
		<a href="{{ route('pagospadrenopago.pdf') }}"><img data-toggle="tooltip" title="Descargar archivo PDF" src="{{asset('aplica/img/pdf.png')}}" height="50" width="50"></a>
		<div class="form-group">
       <input type="text" class="form-control pull-right" style="width:70%" id="search" placeholder="Buscar...">
    </div>

	</div>
</div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif

 @if(session('mario'))
            <div class="alert alert-success" role="alert">
            {{session('mario')}}
            </div>
            @endif


<div class="container">
               <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
					    		<th>#</th>
								<th>Identidad</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Telefono</th>
								<th>Direccion</th>
								<th>WhatsApp</th>
                            </tr>
                        </thead>
						
                        <tbody>
						@foreach ($pagos as $usu)
								<tr> 
								<td>{{ $usu->id}}</td>
								<td>{{ $usu->doc_identidad}}</td>
								<td>{{ $usu->nombre}}</td>
								<td>{{ $usu->apellido}}</td>
								<td>{{ $usu->num_telefono}}</td>
								<td>{{ $usu->direccion}}</td>

								<td>
									<a href="https://api.whatsapp.com/send?phone=57{{ $usu->num_telefono}}&text="> <img src="{{asset('/aplica/img/whatsapp.jpg')}} " style="width: 50px; height: 50px"  ></a>
									
								</td>
										</tr>
										@include('mensaje.modal')

			             	@endforeach
                                                      
                        </tbody>   
						  
                       </table>                  
                    </div>
                </div>
        </div>  
    </div> 





@endsection  
