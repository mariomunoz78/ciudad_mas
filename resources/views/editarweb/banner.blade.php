<div class="row">
                                               
                                               <div class="col">
                                               <br><i class="fa fa-align-justify"></i> Listado de banner
                                               <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#myModal">Agregar Banner</button>

                                                   <table class="table my-4">
                                                       
                                                           <thead>
                                                               <tr>
                                                               <th scope="col">#</th>
                                                               <th scope="col">Imagen</th>
                                                               <th scope="col">Ingresado</th>   
                                                               <th scope="col">Opción</th>                                                                            
                                                                         
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                               <tr v-for="bannerimage in listarBannerarray" :key="bannerimage.id">
                                                               <th scope="row" v-text="bannerimage.id"></th>
                                                                <td>
                                                                <img style="height: 140px; width: 140px" :src="'/aplica/img/paginaweb/' + bannerimage.imagen" class="rounded-circle"/>
                                                                </td>
                                                               <td v-text="bannerimage.usuario.name"></td>

                                                               <td><button class="btn btn-outline-danger" @click="removeBannerImagen(bannerimage.id)" type="submit">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                                </td>

                                                               </tr>
                                                              
                                                           </tbody>
                                                   </table>                                                         
                                               </div>                                                            
                                           </div>




                                                                    <!-- Modal -->
                            <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>


                                <div class="card-body">
                                                <div v-if="success != ''" class="alert alert-success" role="alert">
                                                </div>
                                                <form @submit="formSubmit" enctype="multipart/form-data">
                                              <!--  <strong>Name:</strong>
                                                <input type="text" class="form-control" v-model="name">
                                                -->
                                                <strong>Image:</strong>
                                                <input type="file"v-model="validateimage" class="form-control" v-on:change="onImageBanner">
                        
                                                <div class="modal-footer">
                                                <button class="btn btn-outline-info">Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"></div>
                                                </div>
                                                </form>
                                            </div>


                                </div>

                            </div>
                            </div>
