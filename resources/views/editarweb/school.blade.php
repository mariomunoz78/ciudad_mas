


                            <div class="tab-pane fade" id="colegios" role="tabpanel" aria-labelledby="colegios-tab">
                                <div class="single-course-details">
                                                          <div class="row">
                                                              <div class="col">
                                                                <br>
                                                                <i class="fa fa-align-justify"></i> Colegios
                                                                  

                                                                                                                   <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-outline-success" @click="abrirModal('video','school')" data-toggle="modal" data-target="#ejemploModal">
                                                                    <i class="icon-plus"></i>&nbsp;Agregar Colegio
                                                                    </button>
          
          
                                                                  <table class="table my-4">
                                                                      
                                                                          <thead>
                                                                              <tr>
                                                                              <th scope="col">#</th>
                                                                              <th scope="col">Nombre</th>
                                                                              <th scope="col">Ingresado</th>
                                                                              <th scope="col">Estado</th>

                                                                              <th scope="col">Opción</th>
                                                                              </tr>
                                                                          </thead>
                                                                          <tbody>
                                                                              <tr v-for="school in urlArraySchool" :key="school.id">
                                                                              <th scope="row" v-text="school.id"></th>
                                                                              <td v-text="school.name"></td>
                                                                              <td v-text="school.usuario.name"></td>
                                                                              <td v-if="school.condition==1" style="color:white" class="badge bg-success" style="color:white">Activo</td>
                                                                              <td v-if="school.condition==0" class="badge bg-danger">Desactivado</td>

          
                                                                              <td><button class="btn btn-outline-danger" @click="conditionSchool(school.id, school.condition)" type="submit">
                                                                              <i class="fas fa-trash-alt"></i>
                                                                              </button>
                                                                              <!--
                                                                              <button class="btn btn-outline-info" class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" @click="abrirModal('video','actualizar',school)" type="submit">
                                                                           
                                                                              <i class="far fa-edit"></i></button>
                                                                              -->
          
          
                                                                              </td>
                                                                              <td></td>
                                                                          
                                                                              </tr>
                                                                          </tbody>
                                                                  </table>  


                                                              </div>                                                            
                                                          </div>










         

<!-- Modal -->
<div class="modal fade" id="ejemploModal" tabindex="-1" role="dialog" aria-labelledby="ejemploModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ejemploModalLabel"  v-text="tituloModal"></h5>
        <button type="button" @click="cerrarModal()" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


           <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-group">
                <label class="col-md-12 form-control-label" for="text-input">Nombre del colegio<span style="color:red" v-show="urlyoutube==0">  (*)</span></label>
                <div class="col-md-12">
                    <input type="text" v-model="nameSchool"  class="form-control" placeholder="Nombre del colegio">
                    <p v-if="validateSchool.length>=1" v-text="validateSchool" style="color:red"></p>

                </div>
            </div>
                                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" @click="registerSchool()">Guardar</button>
      </div>
      </form>                           

    </div>
  </div>
</div>





          
                                                          <hr/>
                                                   </div>
                                </div>