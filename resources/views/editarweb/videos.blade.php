<div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                                                  
                                                
                                                  <div class="single-course-details">
                                                          <div class="row">
                                                              <div class="col">
                                                              <br>
                                                              <i class="fa fa-align-justify"></i> Url de youtube
                                                                  <button type="button"  class="btn btn-outline-success" class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" @click="abrirModal('video','registrar')">
                                                                      <i class="icon-plus"></i>&nbsp;Agregar
                                                                  </button>
          
          
                                                                  <table class="table my-4">
                                                                      
                                                                          <thead>
                                                                              <tr>
                                                                              <th scope="col">#</th>
                                                                              <th scope="col">videos</th>
                                                                              <th scope="col">Ingresado</th>
                                                                              <th scope="col">Opción</th>
                                                                              </tr>
                                                                          </thead>
                                                                          <tbody>
                                                                              <tr v-for="video in urlarray" :key="video.id">
                                                                              <th scope="row" v-text="video.id"></th>
                                                                              <td>
                                                                              <iframe width="200" height="150" v-bind:src="video.url"
                                                                               frameborder="0" allow="autoplay;"
                                                                               allowfullscreen></iframe>
          
                                                                              </td>
                                                                              <td v-text="video.usuario.name"></td>
          
                                                                              <td><button class="btn btn-outline-danger" @click="remove(video.id)" type="submit">
                                                                              <i class="fas fa-trash-alt"></i>
                                                                              </button>
                                                                              <button class="btn btn-outline-info" class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" @click="abrirModal('video','actualizar',video)" type="submit">
                                                                           
                                                                              <i class="far fa-edit"></i></button>
          
          
                                                                              </td>
                                                                              <td></td>
                                                                          
                                                                              </tr>
                                                                          </tbody>
                                                                  </table>                                                         
                                                              </div>                                                            
                                                          </div>
          
                                                          <hr/>
          
          
          
          
          
          
          
          
            <!-- Logout Modal-->
            <div class="modal fade show" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel" v-text="tituloModal"><h4><b>&nbsp;&nbsp;&nbsp;</b></h4></h5>
                          <button class="close" type="button" @click="cerrarModal()" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body"> 
                        
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                          <div class="form-group row">
                                              <label class="col-md-3 form-control-label" for="text-input">Url<span style="color:red" v-show="urlyoutube==0">  (*)</span></label>
                                              <div class="col-md-9">
                                                  <input type="text" v-model="urlyoutube"  class="form-control" placeholder="ejemplo https://www.youtube.com/">
                                                  
                                              </div>
                                          </div>
                                           <!--
                                          <div v-show="errorCategoria==1" class="form-group row div-error">
                                              <div class="text-center text-error">
                                                  <div v-for="error in errorMensajeCategoriaArray" :key="error" v-text="error">
                                                     
                                                  </div>
                                              </div>
                                          </div>
                                          -->
                                      </form>
                                  </div>
                                  <div class="modal-footer">
                                  
                                <button class="btn btn-outline-danger" type="button" data-dismiss="modal">Cancel</button>
                                <button v-if="buttonn==1"  :disabled="!urlyoutube" class="btn btn-outline-info" type="button" data-dismiss="modal" @click="registrarUrlYoutube()">Guardar</button>
                                <button v-if="buttonn==2"  :disabled="!urlyoutube" class="btn btn-outline-info" type="button" data-dismiss="modal" @click="updateUrlYoutube()">Actualizar</button>
          
                                
                              
                    </form>
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">                      
          
                     </div>
                  </div>
                </div>
              </div>
            </div>
               
          
          
          
          
          
          
                                                   </div>
                                              </div>
          