<div class="row">    
  
  <h2 class="text-center fs-2 fw-bolder text-uppercase my-3">¿PAGOS?</h2>
    <div class="col-12">                                                                       
       <form @submit="registerPago" enctype="multipart/form-data">

              <div class="form-group">
                  <label for="FormControlTextarea1">Editar Texto</label>
                  <textarea class="ckeditor" id="body" rows="5" cols="70" name="body" v-model="enfoquepago" ></textarea>

                 </div>

                  <div class="form-group">
                  <input type="file"v-model="validateimage" class="form-control" v-on:change="onImagepago">
                  </div>

                  <div class="form-group">
                      <img  style="height: 140px; width: 140px" :src="'/aplica/img/paginaweb/' + imagenpago" class="rounded-circle"/>
                   </div> 

                  <div class="form-group">
                  <button class="btn btn-outline-info" >Guardar</button>
                  </div>

              </div>     
    </div>
      </form>

</div>
<script src="/aplica/ckeditor/ckeditor.js"></script>
