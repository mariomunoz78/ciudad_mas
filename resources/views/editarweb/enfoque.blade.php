<script src="/aplica/ckeditor/ckeditor.js"></script>

<div class="tab-pane fade " id="ciudad" role="tabpanel" aria-labelledby="ciudad-tab">
    <div class="row">



        <div class="single-course-details">
                        <div class="row">
                            <h2 class="text-center fs-2 fw-bolder text-uppercase my-3">Enfoque ?</h2>
                              <div class="col-12">                                                                       
                              <form @submit="registerEnfoque" enctype="multipart/form-data">

                                        <div class="form-group">
                                             
                                            <label for="FormControlTextarea1">Editar Texto</label>
                                            <textarea class="form-control ckeditor"v-model="enfoquee"  id="FormControlTextarea1" rows="3"></textarea>
                                            </div> 

                                            <div class="form-group">
                                            <input type="file"v-model="validateimage" class="form-control" v-on:change="onImageEnfoque">
                                            </div>
                                            <div class="form-group">
                                            <img  style="height: 140px; width: 140px" :src="'/aplica/img/paginaweb/' + imagen" class="rounded-circle"/>
                                            
                                        </div> 
                                        
                                        <button class="btn btn-outline-info" >Guardar</button>

                                  </div>
                                </form>

                            </div>     
                        </div>
                    </div>
     <hr/>  


     @include('editarweb.participar')   
    <hr/>
     @include('editarweb.pagos')   


                       
       </div>
     
       <hr/> 
<!--
       <div class="row"> 
       
            <h2 class="col-12 text-center fs-2 fw-bolder text-uppercase my-3">Ponentes Invitados</h2>
            <div class="col">                                               
            <form>
                
                <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputName4">Nombre</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Juan perez">
                     </div>
                     <div class="form-group col-md-6">
                            <label for="inputText5">Profesión/Cargo</label>
                            <input type="text" class="form-control" id="inputText5" placeholder="Emprendedor">
                        </div>
                </div>    
                
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Palabras por los invitados.</label>
                    <textarea class="form-control" placeholder="Su propósito es empoderar a las personas a través de la tecnología y el emprendimiento. En el 2006 fundó Medellín Digital, un programa de ciudad cuyo objetivo fue cerrar la brecha digital en Medellín, centrado principalmente en la educación y emprendimiento." id="exam1" rows="3"></textarea>
                </div>

                <div class="form-row">
                    <h5 class="col-12 text-center fs-2 fw-bolder text-uppercase my-3">Redes Sociales</h5>
                    <div class="form-group col-md-3">
                    <label for="inputZip">Facebook</label>
                    <input type="text" class="form-control" id="inputFacebook">
                    </div>
                    <div class="form-group col-md-3">
                    <label for="inputTwitter">Twitter</label>
                    <input type="text" class="form-control" id="inputTwitter">
                    </div>
                    <div class="form-group col-md-3">
                    <label for="inputGmail">Gmail</label>
                    <input type="text" class="form-control" id="inputGmail">
                    </div>
                    <div class="form-group col-md-3">
                    <label for="inputInstagram">Instagram</label>
                    <input type="text" class="form-control" id="inputInstagram">
                    </div>

                </div>
                
                <div class="col-12">
                        <table class="table my-4">
                                
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Imagen</th>
                                    <th scope="col">Acción</th>
                                    <th scope="col">Opción</th>                                                                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">1</th>
                                    <td><img src="https://preview.keenthemes.com/metronic/demo1/crud/datatables/advanced/assets/media/users/100_9.jpg" alt="photo" style="width: 55px;  height: 55px;"></td>

                                    <td><form>
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                    </div>

                                    <td><button class="btn btn-outline-primary"  type="button">Guardar</button></td>

                                    </form></td>
                                
                                    </tr>
                                    
                                </tbody>
                        </table> 
                </div>

            </form>                                                    
            </div>
       </div>

--> 
</div>
