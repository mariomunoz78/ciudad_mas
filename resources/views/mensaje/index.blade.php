@extends('layouts.adminsinvue')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3>Mensajes recibido de la pagina web<a href="/mensajes/create"></a></h3>
	</div>
</div>


<div class="container">
               <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>Email</th>
								<th>Telefono</th>
								<th>Descripcion</th>
								<th>Fecha</th>
								<th>Opciones</th>
                            </tr>
                        </thead>
						
                        <tbody>
						@foreach ($contacto as $contacto)
								<tr> 
											<td>{{ $contacto->id}}</td>
											<td>{{ $contacto->nombre}}</td>
											<td>{{ $contacto->correo}}</td>
											<td>{{ $contacto->telefono }}</td>
											<td>{{ $contacto->descripcion }}</td>
											<td>{{ $contacto->created_at}}</td>
											<td>
											<a class="btn btn-outline-danger" href="" data-target="#modal-delete-{{$contacto->id}}"   data-toggle="modal" style="color:red"><i class="fas fa-trash-alt"></i></a>
											</td>
										</tr>
										@include('mensaje.modal')

			             	@endforeach
                                                      
                        </tbody>   
						  
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>  





@endsection  
<!--aqui finaliza la session-->