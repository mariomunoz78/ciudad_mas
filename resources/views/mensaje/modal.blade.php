<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$contacto->id}}">
{{Form::Open(array('action'=>array('ContactoController@destroy',$contacto->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Eliminar Mensaje </h4>
			</div>
			<div class="modal-body">
				<h4><b>Confirme si desea Eliminar el Mensaje</b></h4>
			</div>

			<inpu type="hidden" name="id_user" value="{{ $contacto->id }}">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-info">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>	