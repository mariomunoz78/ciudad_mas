@extends('web.header')
@section('contenedor')
<!--Breadcrumb Banner Area Start-->
                <div class="breadcrumb-banner-area fixed-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumb-text">
                                    <h1 class="text-center">Nuestros Eventos</h1>
                                    <div class="breadcrumb-bar">
                                        <ul class="breadcrumb text-center">
                                            <li><a href="">Inicio</a></li>
                                            <li>Eventos </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--End of Breadcrumb Banner Area-->

<!--Event Area Start-->
             <div class="event-area section-padding event-page">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-event-item">
                                    <div class="single-event-image">
                                        <a href="#">
                                            <img src="{{asset('web/img/event/online.jpg')}}" alt="">
                                            <span><span>25</span>Ene</span>
                                        </a>
                                    </div>
                                    <div class="single-event-text">
                                        <h3><a href="#">Periodo de Inscripción</a></h3>
                                        <div class="single-item-comment-view">
                                           <span><i class="zmdi zmdi-time"></i>24 Hrs.</span>
                                           <span><i class="zmdi zmdi-pin"></i>Página web Ciudad+</span>
                                       </div>
                                       <p>Desde el 25 de enero hasta el 24 de febrero podrán inscribirse al foro Ciudad más, en Registro de esta página. </p>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-event-item">
                                    <div class="single-event-image">
                                        <a href="#">
                                            <img src="{{asset('web/img/event/limite.jpg')}}" alt="">
                                            <span><span>03</span>Mar</span>
                                        </a>
                                    </div>
                                    <div class="single-event-text">
                                        <h3><a href="#">Fecha límite información de Ponencia</a></h3>
                                        <div class="single-item-comment-view">
                                           <span><i class="zmdi zmdi-time"></i>6.00 pm</span>
                                           <span><i class="zmdi zmdi-pin"></i>Correo</span>
                                       </div>
                                       <p>La fecha límite para devolución del documento Información General de la Ponencia es el 3 de Marzo de 2021.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-event-item">
                                    <div class="single-event-image">
                                        <a href="#">
                                            <img src="{{asset('web/img/event/work.jpg')}}" alt="">
                                            <span><span>15</span>Mar</span>
                                        </a>
                                    </div>
                                    <div class="single-event-text">
                                        <h3><a href="#">Entrega trabajo ponencia</a></h3>
                                        <div class="single-item-comment-view">
                                           <span><i class="zmdi zmdi-time"></i>11.59 pm</span>
                                           <span><i class="zmdi zmdi-pin"></i>Correo</span>
                                       </div>
                                       <p>La entrega del trabajo (Ponencia) completo es para 15 de marzo de 2021.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-event-item">
                                    <div class="single-event-image">
                                        <a href="#">
                                            <img src="{{asset('web/img/event/op.jpg')}}" alt="">
                                            <span><span>19</span>Mar</span>
                                        </a>
                                    </div>
                                    <div class="single-event-text">
                                        <h3><a href="#">Inauguración e inicio del foro Ciudad más</a></h3>
                                        <div class="single-item-comment-view">
                                           <span><i class="zmdi zmdi-time"></i>8.00 am </span>
                                           <span><i class="zmdi zmdi-pin"></i>Online</span>
                                       </div>
                                       <p>El 19 de marzo comienza el foro Ciudada más III, esta vez en modalidad virtual.</p>
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                </div>
<!--End of Event Area-->

@endsection