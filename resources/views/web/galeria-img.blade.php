@extends('web.header')
@section('contenedor')

<!--Breadcrumb Banner Area Start-->
                <div class="breadcrumb-banner-area fixed-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumb-text">
                                    <h1 class="text-center">Galería de imágenes </h1>
                                    <div class="breadcrumb-bar">
                                        <ul class="breadcrumb text-center">
                                            <li><a href="#">Inicio</a></li>
                                            <li>Galería imágenes</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--End of Breadcrumb Banner Area-->

<!--Gallery Area Start-->
 <div class="gallery-area pt-110 pb-130">
                    <div class="container">
                        <div class="row">
                           
                            <div class="col-lg-4 col-md-6 mb-30" v-for="imagenes in showimagenesarray" :key="imagenes.id">
                                <div class="gallery-img">
                                    <img :src="'/aplica/img/paginaweb/' + imagenes.imagen" alt="">
                                    <div class="hover-effect">
                                        <div class="zoom-icon">
                                            <a class="popup-image" :href="'/aplica/img/paginaweb/'+imagenes.imagen"><i class="fa fa-search-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--End of Gallery Area-->
@endsection