@extends('web.header')
@section('contenedor')
<!--Breadcrumb Banner Area Start-->
                <div class="breadcrumb-banner-area fixed-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumb-text">
                                    <h1 class="text-center">¿Tienes preguntas o dudas? Escríbenos</h1>
                                    <div class="breadcrumb-bar">
                                        <ul class="breadcrumb text-center">
                                            <li><a href="#">Inicio</a></li>
                                            <li>Contactos</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--End of Breadcrumb Banner Area-->

<!--Contact Form Area Start-->
        <div class="contact-form-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <h4 class="contact-title">Información de contactos</h4>
                        <div class="contact-text">
                            <p><span class="c-icon"><i class="zmdi zmdi-phone"></i></span><span class="c-text">(034) 781 83 21</span></p>
                            <p><span class="c-icon"><i class="zmdi zmdi-phone"></i></span><span class="c-text">+57 305 455 3972</span></p>
                            <p><span class="c-icon"><i class="zmdi zmdi-email"></i></span><span class="c-text">ciudadmas@gnsm.edu.co</span></p>
                            <p><span class="c-icon"><i class="zmdi zmdi-pin"></i></span><span class="c-text">Cra 6 N 78-70 Barrio Sevilla</p>
                        </div>                        
                    </div>
                        
                    <div class="col-lg-8 col-md-12">
                        <h4 class="contact-title">Envíenos su mensaje</h4>
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="nombre">Nombre <span  style=" color: red; font-weight: 900;"   v-show="name==0">*</span> </label>
                                    <input type="text" v-model="name" placeholder="Nombre y apellidos" value="{{old('nombre')}}">
                                </div>
                                <div class="col-md-6">
                                    <label for="email">Email <span  style=" color: red; font-weight: 900;"   v-show="email==0">*</span></label>
                                    <input type="email" v-model="email" placeholder="Correo@correo.com" value="{{old('email')}}">
                                </div>
                                <div class="col-md-6">
                                    <label for="telefono">Teléfono <span  style=" color: red; font-weight: 900;"   v-show="phone==0">*</span></label>                                    
                                    <input type="email" v-model="phone" placeholder="315 555 5555" value="{{old('telefono')}}">
                                </div>                                
                                <div class="col-md-12">
                                    <label for="descripcion">Descripción <span  style=" color: red; font-weight: 900;"   v-show="description==0">*</span></label>
                                    <textarea  v-model="description" cols="30" rows="10" placeholder="Escribe alguna inquietud o pregunta"  value="{{old('descripcion')}}"></textarea>



                                    <button type="button" class="button-default"  @click="registerContact()">Enviar</button>
                                    
                                </div>


                                <div style="color:red" v-show="errorContact==1" class="form-group row div-error">
                                    <div class="text-center text-error">
                                        <div v-for="error in errorMensajeContacteArray" :key="error" v-text="error">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
            </div>
        </div>
<!--End of Contact Form-->
<!--Google Map Area Start-->
 <div class="google-map-area"></div>
        <!--  Map Section -->
        <div>
        <iframe class="map-area" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3721.6917009186836!2d-75.85676391698779!3d8.786535481061314!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x61f403287836d1e2!2sGeorge&#39;s%20Noble%20School!5e0!3m2!1ses-419!2sco!4v1606834185295!5m2!1ses-419!2sco" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
<!--Google Map Area End-->
@endsection