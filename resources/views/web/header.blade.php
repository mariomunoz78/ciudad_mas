<!doctype html>
<html class="no-js" lang="es">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ciudad más || George's Nobel School</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        
		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('web/img/logo/logo.webp')}}">
		 <!-- Google Fonts
		============================================ -->		
        <link href="{{asset('web/css/font-Raleway.css')}}" rel='stylesheet' type='text/css'>
	
		<!-- Fontawsome CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('web/css/font-awesome.min.css')}}" type="text/css">
        
		<!-- Metarial Iconic Font CSS
		============================================ -->
        
        <link rel="stylesheet" href="{{asset('web/css/material-design-iconic-font.min.css')}}">
        
        <!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="{{asset('web/css/bootstrap.min.css')}}">
        
		<!-- Plugins CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('web/css/plugins.css')}}">
        
		<!-- Style CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
        
		<!-- Color CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('web/css/color.css')}}">
        
		<!-- Responsive CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('web/css/responsive.css')}}">
        
	
   

        
    </head>
    
     <body>
             <!--Main Wrapper Start-->
             <div class="as-mainwrapper">
            <!--Bg White Start-->
            <div class="bg-white">
                <!--Header Area Start-->
                <header>
                    <div class="header-top">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 col-md-5 d-none d-lg-block d-md-block">
                                    <span>Tienes alguna pregunta?    +57 305 455 3972</span>
                                </div>
                                <div class="col-lg-5 col-md-7 col-12">
                                    <div class="header-top-right">
                                        <div class="content"><a href="{{url('nueva/cuenta')}}" target="_blank"><i class="zmdi zmdi-account"></i> ¡Inscríbete!</a></div>
                                        <div class="content"><a href="{{url('index')}}" target="_blank"><i class="zmdi zmdi-sign-in"></i> Ingresar</a></div>                                                                               
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-logo-menu sticker">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-12">
                                    <div class="logo py-3 border-right ">
                                        <a href="{{url('inicio')}}"><img src="{{asset('web/img/logo/logo.webp')}}"  style="height: 90px;width: 105 px;" alt="ciudad más"></a>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-12">
                                    <div class="mainmenu-area pull-right">
                                        <div class="mainmenu d-none d-lg-block">
                                            <nav>
                                                <ul id="nav">
                                                    <li><a href="{{url('inicio')}}">Inicio</a></li>         
                                                    <li><a href="{{url('ciudadmas')}}">Ciudad más</a></li>     
                                                    <li><a href="#">guiás</a>
                                                        <ul class="sub-menu">
                                                            <li><a target="_blank" href="https://drive.google.com/drive/folders/1qfnkoZE-y53Mi-zZhBTdINRdOk7pMFqu?usp=sharing">- guiás Pdf</a>
                                                            </li>
                                                            <li><a target="_blank" href="https://docs.google.com/document/d/159xLr8FzGnSv5J2HQd8bE8TSNQ44DHeJ6U3brYyqGAg/edit?usp=sharing">- guiás web</a>
                                                            </li>
                                                           
                                                        </ul>
                                                    </li>
                                                    <li><a href="{{url('eventos')}}">fechas importantes</a></li>
                                                    <li><a href="#">galería</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{url('gallery-img')}}">- Imágenes</a></li>
                                                            <li><a href="{{url('gallery-videos')}}">- Videos</a></li>                                                      
                                                        </ul>
                                                    </li>
                                                    <li><a href="{{url('contactos')}}">Contactos</a></li>
                                                </ul>
                                            </nav>
                                        </div>                                      
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>  
                    <!-- Mobile Menu Area start -->
                    <div class="mobile-menu-area">
                        <div class="container clearfix">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="mobile-menu">
                                        <nav id="dropdown">
                                            <ul id="nav">
                                                    <li><a href="{{url('inicio')}}">Inicio</a></li>         
                                                    <li><a href="{{url('ciudadmas')}}">Ciudad más</a></li>     
                                                     <li><a href="#">guiás</a>
                                                        <ul class="sub-menu">
                                                            <li><a target="_blank" href="https://drive.google.com/drive/folders/1qfnkoZE-y53Mi-zZhBTdINRdOk7pMFqu?usp=sharing">- guiás Pdf</a>
                                                            </li>
                                                            <li><a target="_blank" href="https://docs.google.com/document/d/159xLr8FzGnSv5J2HQd8bE8TSNQ44DHeJ6U3brYyqGAg/edit?usp=sharing">- guiás web</a>
                                                            </li>
                                                           
                                                        </ul> 
                                                    </li>
                                                    <li><a href="{{url('eventos')}}">fechas importantes</a></li>
                                                    <li><a href="#">galería</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="{{url('gallery-img')}}">-Imágenes</a></li>
                                                            <li><a href="{{url('gallery-videos')}}">- Videos</a></li>                                                            
                                                        </ul>
                                                    </li>
                                                    <li><a href="{{url('contactos')}}">Contactos</a></li>
                                                </ul>
                                        </nav>
                                    </div>					
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Mobile Menu Area end -->    
                </header>
                <!--End of Header Area-->
               
               <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />    
               <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
               
               <div class="box-body" id="app">
                    <div class="row">
                      <div class="col-md-12">
                @yield('contenedor')
                      </div>
                    </div>
                 </div>
              
               
              <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
              <script src="/componenteweb.js"></script>

                           
                <!--Spoonsor Area Start-->
                 <!--        <div class="slider py-1 my-2">
                            <div class="slide-track">
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/1.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/2.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/3.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/4.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/5.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/6.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/7.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/1.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/2.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/3.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/4.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/5.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/6.png" height="100" width="250" alt="" />
                                </div>
                                <div class="slide">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/7.png" height="100" width="250" alt="" />
                                </div>
                            </div>
                        </div> -->
                <!--End of Spoonsor Area-->  
                <!--Footer Widget Area Start-->
                <div class="footer-widget-area">
                    <div class="container">
                        <div class="row">
                        <div class="col-lg-3 col-md-6">
                                <div class="single-footer-widget">
                                    <h3>Coordinador del foro</h3>
                                    <span><i class="zmdi zmdi-account-circle"></i>Carlos Burgos</span>
                                    <!-- <a href="tel:311-649-3990"><i class="fa fa-phone"></i>311-111-3990</a> -->
                                    <span><i class="zmdi zmdi-account-circle"></i>Ana Vega</span>
                                   
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-footer-widget">
                                    <h3>Coordinador general</h3>
                                    <span><i class="zmdi zmdi-account-circle"></i>Jaime Ballestas</span>
                                    <a href="tel:305-455-3972"><i class="fa fa-phone"></i>305-455-3972</a>
                                    <a href="mailto:jballestas@gnsm.edu.co"><i class="fa fa-envelope"></i>jballestas@gnsm.edu.co</a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-footer-widget">
                                    <h3>Finanzas</h3>
                                    <a href="mailto:Finanzas@gnsm.edu.co"><i class="fa fa-envelope"></i>Finanzas@gnsm.edu.co</a>
                                                                                                                                                            
                                    <a href="#"><i class="zmdi zmdi-hospital"></i>#QUEDATE EN CASA</a>
                                    <a href="#"><i class="zmdi zmdi-file"></i>Terminos &amp; Condiciones</a>
                                    <a href="#"><i class="zmdi zmdi-file"></i>Politicas de privacidad</a>
                                    
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-footer-widget">
                                    <h3>próximamente</h3>
                                    <div class="instagram-image">
                                        <div class="footer-img">
                                            <a href="#"><img src="{{asset('web/img/footer/play.png')}}" alt=""></a>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of Footer Widget Area-->
                <!--Footer Area Start-->
                <footer class="footer-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-7 col-12">                        
                            <span>Copyright &copy; <a target="_blank" href="https://www.gnsm.edu.co/"> George's Noble School 2020. </a> Todos los derechos reservados.  </span>
                            </div>
                            <div class="col-lg-6 col-md-5 col-12">
                                <div class="column-right">
                                    <span>Diseñado por <a target="_blank" href="https://www.facebook.com/mario.f.rivera.96"> Mario Muñoz</a> &amp; <a target="_blank" href="mailto:luisfernandofo2015@gmail.com">Luis Farco</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!--End of Footer Area-->
            </div>   
            <!--End of Bg White--> 
         </div>    
        <!--End of Main Wrapper Area--> 
           <!-- Color Switcher -->
           <div class="ec-colorswitcher">
                <a class="ec-handle" href="#"><i class="zmdi zmdi-settings"></i></a>
                <h3>Style Switcher</h3>
                <div class="ec-switcherarea">
                    <h6>Chose Color</h6>
                    <ul class="ec-switcher">
                        <li><a href="#" class="cs-color-1 styleswitch" data-rel="color-one"></a></li>
                        <li><a href="#" class="cs-color-2 styleswitch" data-rel="color-two"></a></li>
                        <li><a href="#" class="cs-color-3 styleswitch" data-rel="color-three"></a></li>
                        <li><a href="#" class="cs-color-4 styleswitch" data-rel="color-four"></a></li>
                        <li><a href="#" class="cs-color-5 styleswitch" data-rel="color-five"></a></li>
                        <li><a href="#" class="cs-color-6 styleswitch" data-rel="color-six"></a></li>
                        <li><a href="#" class="cs-color-7 styleswitch" data-rel="color-seven"></a></li>
                        <li><a href="#" class="cs-color-8 styleswitch" data-rel="color-eight"></a></li>
                        <li><a href="#" class="cs-color-9 styleswitch" data-rel="color-nine"></a></li>
                        <li><a href="#" class="cs-color-10 styleswitch" data-rel="color-ten"></a></li>
                    </ul>               
                </div>
           </div>
        <!-- Color Switcher end -->	

      
		<!-- Color Swithcer CSS
		============================================ -->        
        <link rel="stylesheet" href="{{asset('web/css/color-switcher.css')}}" type="text/css">
     <!-- Color Css Files
		============================================ -->	
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-one.css')}}" title="color-one" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-three.css')}}" title="color-three" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-two.css')}}" title="color-two" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-four.css')}}" title="color-four" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-five.css')}}" title="color-five" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-six.css')}}" title="color-six" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-seven.css')}}" title="color-seven" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-eight.css')}}" title="color-eight" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-nine.css')}}" title="color-nine" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-ten.css')}}" title="color-ten" media="screen" />
        <link rel="alternate stylesheet" type="text/css" href="{{asset('web/switcher/color-ten.css')}}" title="color-ten" media="screen" />

        	<!-- Modernizr JS
		============================================ -->		
        <script src="{{asset('web/js/vendor/modernizr-2.8.3.min.js')}}"></script>
	    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

        <!-- jquery
		============================================ -->		
        <script src="{{asset('web/js/vendor/jquery-1.12.4.min.js')}}"></script>
        
		<!-- popper JS
		============================================ -->		
        <script src="{{asset('web/js/popper.min.js')}}"></script>
        
		<!-- bootstrap JS
		============================================ -->		
        <script src="{{asset('web/js/bootstrap.min.js')}}"></script>
        
		<!-- AJax Mail JS
		============================================ -->		
        <script src="{{asset('web/js/ajax-mail.js')}}"></script>
        
		<!-- plugins JS
		============================================ -->		
        <script src="{{asset('web/js/plugins.js')}}"></script>
        
		<!-- StyleSwitch JS
		============================================ -->	
        <script src="{{asset('web/js/styleswitch.js')}}"></script>
        
		<!-- main JS
		============================================ -->		
        <script src="{{asset('web/js/main.js')}}"></script>

        
        <!-- Instagram JS
		============================================ -->		
        <div id="fb-root"></div>
        <script async="" src="//www.instagram.com/embed.js"></script>
     </body>
    </html>

