@extends('web.header')
@section('contenedor')
<!--Breadcrumb Banner Area Start-->
                <div class="breadcrumb-banner-area fixed-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumb-text">
                                    <h1 class="text-center">Galería de videos </h1>
                                    <div class="breadcrumb-bar">
                                        <ul class="breadcrumb text-center">
                                            <li><a href="#">Inicio</a></li>
                                            <li>Galería videos</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--End of Breadcrumb Banner Area-->
<!--Gallery Area Start-->
     <div class="gallery-area pt-110 pb-130">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-4 col-md-6 mb-30" v-for="video in showurlarray" :key="video.id">
                                <div class="fix video-gallery">
                                     <iframe class="videoresp" v-bind:src="video.url" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
 <!--End of Gallery Area-->
@endsection