@extends('web.header')

@section('contenedor')
         <!--Breadcrumb Banner Area Start-->
                <div class="breadcrumb-banner-area fixed-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumb-text">
                                    <h1 class="text-center">más sobre ciudad más</h1>
                                    <div class="breadcrumb-bar">
                                        <ul class="breadcrumb text-center">
                                            <li><a href="#">Inicio</a></li>
                                            <li>ciudad más</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         <!--End of Breadcrumb Banner Area-->
  
         <!--About Page Area Start-->
         <section class="enfoque">       
                <div class="about-page-area py-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-title-wrapper">
                                            <div class="section-title">
                                                <h3>Enfoque</h3>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                <div class="col-lg-6 col-md-12 col-12">
                                        <div class="about-text-container">
                                        <div class="text-scroll">
                                        <p class="text-justify">Incentivar a los jóvenes a participar en la discusión de tópicos relativos a:</p> <p class="font-weight-bolder">"Ciudades y Ciudadanos Adaptables: Retos y Cambios Con el Covid 19".</p>
                                        <p class="text-justify">Nuesto foco sigue teniendo como referente los ejes de sostenibilidad propuestos en los ODS (Objetivos de Desarrollo Sostenible). <br>
                                        <br>Los Estudiantes tendrán un espacio donde presentarán ponencias, debatirán ideas y expondrán proyectos que giren entorno a qué cambió, qué está cambiando y qué se ha mantenido, cómo enfrentó cada ciudad la pandemia, 
                                        qué faltó por hacer y cuáles rutas eran viables.<br>
                                        <br>Buscamos, aprovechar el pensamiento crítico y la capacidad de análisis con la que cuentan estos jóvenes, que peden ejercer sus habilidades comunicativas y exhibir el liderazgo que se necesita hoy más que nunca.</p>
                                        
                                        <p class="font-weight-bolder">Sugeriencias de líneas de trabajo:</p>
                                       
                                        <p class="text-justify">Ciudades Adaptables; cambios con el Covid19. ¿Qué cambió, qué está cambiando y qué se mantuvo?<br>
                                        
                                        <br> - Reto Covid19: La ciudad - Las personas - Las instituciones - Las empresas - Los negocios. Cambios y retos a corto, mediano y largo plazo.
                                       <br> - Luego de un año de pandemia, ¿Cómo enfrentó tu ciudad, u otra, la pandemia y qué otras rutas eran viables?¿Qué ha debido hacerce?
                                       <br> - Otro tema de su interés que vaya en concordancia el tema marco.</p>
                                        </p>
                                        </div>
                                        </div>                          
                                </div>
                                <div class="col-lg-6 col-md-12 col-12">
                                        <div class="skill-image">
                                            <img :src="'/aplica/img/paginaweb/' + imagen"  class="border border-dark"alt="">
                                        </div>
                                    </div>

                                
                            </div>
                        </div>                
         </section>
         <!--End of About Page Area-->

         <!--About Page Area Start-->
          <section class="participar">       
                    <div class="about-page-area background py-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="section-title-wrapper white">
                                                <div class="section-title">
                                                    <h3>¿cómo participar?</h3>
                                                    <p>Entérese de cómo participar</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row justify-content-center">
                                            <div class="col-lg-6 col-md-8 col-12">
                                                <div class="skill-image">
                                                <img :src="'/aplica/img/paginaweb/' + imagenparticipar" alt="" class="border border-light">
                                                </div>
                                            </div>

                                           <div class="col-lg-6 col-md-10 col-12">
                                                <div class="about-text-container ">                                                    
                                                    <div class="text-scroll">
                                                    <p class="font-weight-bolder mb-0">La dinámica de participación será:</p>    
                                                    <p class="text-justify">
                                                    La modalidad del Foro es:<br>
                                                   <br> A)	presentación de ponencias con una ronda de preguntas posteriores.
                                                   <br> B)	presentación de ponencias con una ronda de preguntas y debate posterior.</p>
                                                    
                                                       <p class="font-weight-bolder mb-0">Los tiempos para estos dos estilos de ponencia serán:</p>
                                                    <p class="text-justify">
                                                   <br> <u>Ponencias tipo A)</u> Tiempo de ponencia: Mínimo 15 máximo 20. Tiempo total por ponente de 25 minutos con preguntas incluidas. 
                                                    <br><u>Ponencias tipo B)</u> una sesión de 20 a 25 minutos de ponencia con debate posterior. Tiempo total por ponente 45 minutos.</p>
                                                    </div>
                                                </div>                          
                                          </div>

                                    </div>
                                
                            </div>                
         </section>
         <!--End of About Page Area-->

               <!--Teachers Area Start-->
             <section class="ponentes">
                <div class="teachers-area pt-5 pb-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title-wrapper">
                                    <div class="section-title">
                                        <h3>ponentes centrales</h3>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-start">
                            <div class="jumbotron jumbotron-fluid">
                              <div class="container">
                                <h1 class="display-4 text-center "><strong>Aviso</strong></h1>
                                <hr class="my-3">
                                <p class="h4 text-center ">Los ponentes invitados serán publicados próximamente en este espacio.</p>
                              </div>
                            </div>
                        <!--<div class="col-lg-4 col-md-6 col-12">
                                <div class="single-teacher-item">
                                    <div class="single-teacher-image">
                                        <a href="#"><img src="web/img/ponentes/1.jpeg" alt=""></a>
                                    </div>
                                    <div class="single-teacher-text">
                                        <h3><a href="#">Fabían Díaz</a></h3>
                                        <h4>Ingeniero Geólogo</h4>
                                        <p>Gerente Comercial Sol&Cielo, Profesor de La universidad de Córdoba en áreas de Climatología y Planificación ambiental. Gestor de proyectos en el área de la sostenibilidad, (en temas como Campus verde, movilidad sostenible, Energía Solar).</p>
                                        <div class="social-links">
                                            <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                            <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                            <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                            <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-teacher-item">
                                    <div class="single-teacher-image">
                                        <a href="#"><img src="web/img/ponentes/2.jpg" alt=""></a>
                                    </div>
                                    <div class="single-teacher-text">
                                        <h3><a href="#">Daniela L & María E</a></h3>
                                        <h4>Emprendedoras</h4>
                                        <p>Festivo es un proyecto social que rinde homenaje a la cultura, la tradición oral y la identidad local , reflejando el sentir de los lugares, de las personas y las palabras a través de piezas de moda atemporal. </p>
                                        <div class="social-links">
                                            <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                            <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                            <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                            <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-teacher-item">
                                    <div class="single-teacher-image">
                                        <a href="#"><img src="web/img/ponentes/3.jpg" alt=""></a>
                                    </div>
                                    <div class="single-teacher-text">
                                        <h3><a href="#">Andrés Montoya</a></h3>
                                        <h4>Director Ejecutivo</h4>
                                        <p> Su propósito es empoderar a las personas a través de la tecnología y el emprendimiento. En el 2006 fundó Medellín Digital, un programa de ciudad cuyo objetivo fue cerrar la brecha digital en Medellín, centrado principalmente en la educación y emprendimiento.</p>
                                        <div class="social-links">
                                            <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                            <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                            <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                            <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>-->                       
                        </div>
                    </div>
                </div>
            </section>


            <!--Pagos Page Area Start-->
        <!--    <section class="pagos">       
                    <div class="about-page-area py-5">
                                <div class="container">
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="section-title-wrapper">
                                                    <div class="section-title">
                                                        <h3>Pagos</h3>
                                                        <p>Para más información diríjase a contacto y escríbenos </p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="row justify-content-center">                                            

                                           <div class="col-lg-6 col-md-10 col-12">
                                                <div class="about-text-container ">                                                    
                                                    <div class="text-scroll">
                                                    <p class="text-justify text-dark" v-text="enfoquepago">
                                                    </p>
                                                    </div>
                                                </div>                          
                                          </div>

                                          <div class="col-lg-6 col-md-8 col-12 ">
                                                <div class="skill-image text-center">
                                                    <img class="img-fluid" :src="'/aplica/img/paginaweb/' + imagenpago" alt="">
                                                </div>
                                            </div>

                                    </div>
                                    

                                            


                               </div>              
                    </div>  

            </section> -->
            <!--End of Pagos Page Area-->
                   
            <!--hoteles area-->
          <!--  <section class="hotel">
                            <div class="hotel-area section-padding background ">
                            <div class="container">
                                    <div class="row">
                                                <div class="col-md-12">
                                                    <div class="section-title-wrapper white">
                                                        <div class="section-title">
                                                            <h3>Hoteles</h3>
                                                            <p>Considere estos hoteles para su instancia en la ciudad</p>
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/3.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Parque del Sol</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/4.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Santorini Loft</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/5.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Sites</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/6.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Unión Plaza</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/1.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Ghl</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 my-3">
                                                <div class="single-product-item">
                                                    <div class="single-product-image">
                                                        <a href="#"><img src="{{asset('web/img/hoteles/2.jpg')}}" alt=""></a>
                                                    </div>
                                                    <div class="single-product-text">
                                                        <h4><a href="#">Hotel Miraval</a></h4>
                                                        <div class="product-buttons">
                                                            <button type="button" class="button-default cart-btn">Ver cotización</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </section>               
            <!--hoteles area-->

                <!--Start of reglas Area-->
        <section class="reglas section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title-wrapper">
                            <div class="section-title">
                                <h3>Reglas &amp; CÓDIGOS</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="container">
                <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                        <li class="nav-item nav_cod regla_cod" role="presentation">
                            <a class="nav-link  node active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Vestir Hombre</a>
                        </li>
                        <li class="nav-item nav_cod regla_cod" role="presentation">
                            <a class="nav-link node " id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Vestir Mujer</a>
                        </li>
                        <li class="nav-item nav_cod regla_cod" role="presentation">
                            <a class="nav-link node " id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Conducta</a>
                        </li>
                </ul>
                    <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="single-course-details">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="overlay-effect">
                                                                <a href="#"><img alt="" src="{{asset('web/img/others/1.jpg')}}"></a>
                                                            </div>
                                                        </div>  
                                                        <div class="col-md-6">
                                                            <div class="single-item-text">
                                                                <h4>Código de vestir hombres</h4><br>
                                                                <div class="course-text-content">
                                                                    <p class="h6 text-justify">Por favor tenga en cuenta lo siguiente.</p>                                                                         
                                                                    <ul class="list">
                                                                        <li><p class="text-regla my-1 text-justify">- Camisa clásica manga larga</p></li>
                                                                        <li><p class="text-regla my-1 text-justify">- Pantalón clásico con correa</p></li>
                                                                        <li><p class="text-regla my-1 text-justify">- Zapatos cerrados de vestir con medias</p></li>                              
                                                                    </ul>
                                                                    <br>
                                                                    <div class="single-item-text-info">
                                                                        <span>*: <span>No se aceptan bermudas ni jeans</span></span><br>
                                                                        <span>*: <span>Color: Blanco, negro, pasteles y neutrales.</span></span>
                                                                    </div>
                                                                </div>    
                                                                <div class="single-item-content">
                                                                                                                                        
                                                                </div>   
                                                            </div>
                                                        </div> 
                                                    </div>     
                                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="single-course-details">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="overlay-effect">
                                                                <a href="#"><img alt="" src="{{asset('web/img/others/2.jpg')}}"></a>
                                                            </div>
                                                        </div>  
                                                        <div class="col-md-6">
                                                            <div class="single-item-text">
                                                                <h4>Código de vestir mujeres</h4><br>                                                                        
                                                                <div class="course-text-content">
                                                                    <p class="h6 text-justify text-justify">Por favor tenga en cuenta lo siguiente.</p>                                                                         
                                                                        <ul class="list">
                                                                            <li><p class="text-regla my-1 text-justify">- Blusa y/o Camisa clásica</p></li>
                                                                            <li><p class="text-regla my-1 text-justify">- Faldas y vestidos de largo apropiado, máx 4 dedos encima de la rodilla</p></li>
                                                                            <li><p class="text-regla my-1 text-justify">- Pantalón formal.</p></li>
                                                                            <li><p class="text-regla my-1 text-justify">- Zapatos cerrados.</p></li>   
                                                                            <li><p class="text-regla my-1 text-justify">- Maquillaje discreto.</p></li>                                 
                                                                        </ul>
                                                                        <br>
                                                                        <div class="single-item-text-info">
                                                                        <span>*: <span>No se aceptan shorts ni jeans</span></span><br>
                                                                        <span>*: <span>No se aceptan tenis ni sandalias.</span></span><br>
                                                                        <span>*: <span>En caso de ser tacones, con una altura apropiada para el evento.</span></span>
                                                                    </div>
                                                                </div>    
                                                                <div class="single-item-content">
                                                                                                                                        
                                                                </div>   
                                                            </div>
                                                        </div> 
                                                    </div>     
                                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="single-course-details">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="overlay-effect">
                                                                <a href="#"><img alt="" src="{{asset('web/img/others/3.jpg')}}"></a>
                                                            </div>
                                                        </div>  
                                                        <div class="col-md-6">
                                                            <div class="single-item-text">
                                                                <h4>Código de conducta</h4> <br>
                                                                <div class="course-text-content">
                                                                        <p class="h6 text-justify">Se establecerán las siguientes normas de conducta para evitar cualquier tipo de inconvenientes,
                                                                                y que pueda disfrutar de un espacio agradable y dinámico.<br> El incumplimiento de alguna de estas tendrá un 
                                                                                llamado de atención, o una sanción mayor si el caso lo amerita.</p>                                                                         
                                                                                <ul class="list">
                                                                                    <li><p class="text-regla my-1 text-justify">- El uso del lenguaje parlamentario es necesario en todo momento.</p></li>
                                                                                    <li><p class="text-regla my-1 text-justify">- Por respeto, se deben mantener las cámaras de las participantes encendidas en todo tiempo.</p></li>
                                                                                    <li><p class="text-regla my-1 text-justify">- No se aceptan agresiones de ningún tipo.</p></li>
                                                                                    <li><p class="text-regla my-1 text-justify">- El uso de dispositivos electrónicos es de uso académico exclusivamente.</p></li>
                                                                                    <li><p class="text-regla my-1 text-justify">- No ausentarse de la sala sin consentimiento o autorización de los presidentes.</p></li>                              
                                                                                </ul>
                                                                                <br>
                                                                                <p class="h6 text-justify">Cualquier acción que se considere una violación al código de conducta podrá ser sancionada a pesar de no estar descrita en las anteriores.</p>
                                                                </div>    
                                                                <div class="single-item-content">
                                                                                                                                    
                                                                </div>   
                                                            </div>
                                                        </div> 
                                                    </div>     
                                                </div>
                            </div>
                    </div>
            </div>
        </section>  



  @endsection
  

 

                        
  
                                             