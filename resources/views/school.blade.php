@extends('layouts.adminsinvue')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  
		<h3>Lista de colegios </h3>
        <button type="button" class="btn btn-outline-success" href=""  data-target="#modalUpdateFamily", data-toggle="modal">
        <i class="icon-plus"></i>&nbsp;Nuevo Colegio
        </button>
	</div>
</div>
<br>


@if(session('correcto'))
<div class="alert alert-success" role="alert">
  {{session('correcto')}}
</div>
@endif


@if(session('update'))
<div class="alert alert-info" role="alert">
  {{session('update')}}
</div>
@endif





<div class="container">
               <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>Nombre</th>
                              <th>Estado</th>
                              <th>Logo</th>

                              <th>Opcion</th>
                            </tr>
                        </thead>
						
                        <tbody>
                        @foreach ($school as $school)

                        <tr>
                        <td scope="row">{{ $school->id }}</td>
                                <td> {{ $school->name }}</td>
                                @if($school->condition==1)
                                <td style="color:white" class="badge bg-success" style="color:white">Activo</td>
                                @else
                                <td class="badge bg-danger">Desactivado</td>
                                @endif

                                @if($school->logo=="")

                                <td><a href="" data-target="#modal-estado-" data-toggle="modal"><img class="img-profile rounded-circle" data-toggle="tooltip" title="No tiene foto" src="{{asset('/aplica/img/nofoto.png')}}"  width="70" height="70"></a></td>
                                @elseif($school->logo!="")

                                <td><a href="" data-target="#modal-estado-" data-toggle="modal"><img class="img-profile rounded-circle" data-toggle="tooltip" title="Colegio" src="{{asset('/aplica/img/logocolegios/'.$school->logo)}}"  width="70" height="70"></a></td>
                                @endif
                                <td>

                                <a class="btn btn-outline-danger"  href="{{ url('school/condition',['id' => $school->id, 'condicion' => $school->condition]) }}" style="color:red" data-toggle="tooltip" title=" "><i class="fas fa-trash-alt"></i></a>
                                <a class="btn btn-outline-info"  href="" data-target="#modal-estado-{{$school->id}}", data-toggle="modal" style="color:info" data-toggle="tooltip" title=" "><i class="fas fa-edit"></i></a>
                                
                               </td>
				               </tr>
                       @include('modalSchoolUpdate')   

			             	@endforeach
                                                      
                        </tbody>   
						  
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>  














    @include('modalSchool')   





    @endsection  
<!--aqui finaliza la session-->