@extends('layouts.estudiantehome')

@section('contenido')
 @if (empty($marioo->id))
           <div class="alert alert-danger" role="alert">
           <h2>  {{ Auth::user()->name }} {{ Auth::user()->apellido }} falta por inscribiirte al foro ciudad mas, <b>INGRESE AL MODULO INSCRIPCIONES, NUEVO</b></h2>
            </div>
            @elseif($marioo->id==Auth::user()->id)
            <div class="alert alert-success" role="alert">
             <h2>  {{ Auth::user()->name }} {{ Auth::user()->apellido }} <b>Inscrito </b> en el foro ciudad mas</h2>
             </div>
          @endif

          <div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-light" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>
 <!-- Earnings (Monthly) Card Example -->
 <div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
	
	</div>
	
</div>
 <div class="font_2" style="font-size:37px; line-height:1.2em; text-align:center;">
   <img src="https://static.wixstatic.com/media/926009_74d7a1adc9aa40dea86a231853552ea8~mv2_d_1506_1523_s_2.png/v1/fill/w_398,h_400,al_c,q_85,usm_0.66_1.00_0.01/LOGO%20CIUDAD%20%2B.webp">                
  </div>

<h2 class="font_2" style="font-size:37px; line-height:1.2em; text-align:center;"><span style="color:#3AB30E;"><span style="font-size:37px;"><span style="font-family:baskervillemtw01-smbdit,serif;"><span style="letter-spacing:0em;">III Foro Ciudad+,</span></span></span></span></h2>

<h2 class="font_2" style="font-size:37px; line-height:1.2em; text-align:center;"><span style="color:#3AB30E;"><span style="font-size:37px;"><span style="font-family:baskervillemtw01-smbdit,serif;"><span style="letter-spacing:0em;">La ciudad que somos</span></span></span></span></h2>



	<h3 class="font_3" style="font-size:23px; text-align:center;"><span style="color:#3AB30E;"><span style="font-size:23px;"><span style="font-family:baskervillemtw01-smbdit,serif;"><span style="text-shadow:#ffffff -1px -1px 0px, #ffffff -1px 1px 0px, #ffffff 1px 1px 0px, #ffffff 1px -1px 0px;">Marzo&nbsp;19 - 2021</span></span></span></span></h3>


@endsection
