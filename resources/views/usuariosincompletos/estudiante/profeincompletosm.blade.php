@extends('layouts.adminsinvue')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3  class="alert alert-info" role="alert"><b>Profesores Incompletos</b></h3>
	</div>
</div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif

 @if(session('mario'))
            <div class="alert alert-secondary" role="alert">
            {{session('mario')}}
            </div>
            @endif




			<div class="container">
               <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
								<th>#</th>
								<th>Identidad</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Telefono</th>
								<th>Direccion</th>
								<th>WhatApp</th>
                            </tr>
                        </thead>
						
                        <tbody>
                    @foreach ($usuarioincompletos as $usu)
						@if ($usu->tipo_persona=='profe')

                           <tr>
								<td>{{ $usu->id}}</td>
								<td>{{ $usu->num_documento}}</td>
								<td>{{ $usu->name}}</td>
								<td>{{ $usu->apellido}}</td>
								<td>{{ $usu->telefono}}</td>
								<td>{{ $usu->direccion}}</td>

								<td>
									<a href="https://api.whatsapp.com/send?phone=57{{ $usu->telefono}}&text=Le estamos escribiendo del foro ciudad mas, por favor inscribirse *1:* ingrese http://ciudad-mas.co/index  *2:* Ingrese con su correo y contrase���a  *3*:una vez ingresado presione el modulo Inscribir y presione el boton verde Nuevo y llene el formulario"> <img src="{{asset('/aplica/img/whatsapp.jpg')}} " style="width: 50px; height: 50px"  ></a>
									
								</td>
						  </tr>
					    @endif
			        @endforeach
                                                      
                        </tbody>   
						  
                       </table>                  
                    </div>
                </div>
        </div>  
    </div> 



@endsection  
<!--aqui finaliza la session-->