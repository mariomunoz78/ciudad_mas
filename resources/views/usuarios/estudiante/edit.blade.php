@extends('layouts.adminhome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
 
<div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Editar

                        @if(session('update'))
			               <div class="alert alert-success" role="alert">
			             	{{session('update')}}
                           </div>
                           <br><br>
                        @endif
 
       <form action="{{ url('inscripcionre/editarUsers')  }}" method="post">
          @csrf
                    <div class="row">

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Nombre</label>
                         <input type="text" name="nombre" class="form-control" value="{{  $user ->name}}" >
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Apellido</label>
                         <input type="text" name="lastname" class="form-control" value="{{  $user ->apellido}}" >
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Numero de documento</label>
                         <input type="number" name="num_documento" class="form-control" value="{{  $user ->num_documento}}" >
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Email</label>
                         <input type="text" name="email" class="form-control" value="{{  $user ->email}}" >
                       </div>
                      </div>



                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">

                        <label for="name">Tipo persona</label>

                        <select name="tipo_persona" class="form-control" style="width: 100%;">
                            @if ($user->tipo_persona=='estudiante')
                                <option value="estudiante" selected="selected">ESTUDIANTE</option>
                                <option value="padre">PADRE</option>
                                <option value="profe">PROFE</option>
                            @elseif ($user->tipo_persona=='padre')
                                <option value="estudiante">ESTUDIANTE</option>
                                <option value="padre" selected="selected">PADRE</option>
                                <option value="profe">PROFE</option>

                             @elseif ($user->tipo_persona=='profe')

                                <option value="estudiante">ESTUDIANTE</option>
                                <option value="padre">PADRE</option>
                                <option value="profe" selected="selected">PROFE</option>
                            @else
                                <option value="estudiante">ESTUDIANTE</option>
                                <option value="padre">PADRE</option>
                                <option value="profe">PROFE</option>
                            @endif
                         </select>
                       </div>
                      </div>


                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Telefono</label>
                         <input type="text" name="phone" class="form-control" value="{{  $user ->telefono}}" >
                       </div>
                      </div>

                      <input type="hidden" name="id_user" value="{{  $user ->id }}">


                      <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-warning btn-lg" >Guardar</button>
                        </div>
                        </div>

                    </form>

                    </div>

                </div>
                </div>
                </div>
            </div>   

                    
@endsection  
<!--aqui finaliza la session-->