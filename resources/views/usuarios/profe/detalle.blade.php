<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-mario-{{$usu->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><b>Detalle estudiante</b></h4>
			</div>
			<div class="modal-body">
				
				<h4 class="modal-title"><b>Nombre: </b> {{ $usu->name }}</h4>
				<h4 class="modal-title"><b>Apellido: </b> {{ $usu->apellido }}</h4>
				<h4 class="modal-title"><b>Tipo persona: </b>{{ $usu->tipo_persona }}</h4>
				<h4 class="modal-title"><b>Documento: </b>{{ $usu->tipo_documento }} : {{ $usu->num_documento }}</h4>
				<h4 class="modal-title"><b>Ciudad: </b>{{ $usu->ciudad }}</h4>
				<h4 class="modal-title"><b>Direccion: </b>{{ $usu->direccion }}</h4>
				<h4 class="modal-title"><b>Telefono: </b>{{ $usu->telefono }}</h4>
				<h4 class="modal-title"><b>Correo: </b>{{ $usu->email }}</h4>
				<h4 class="modal-title"><b>Fecha Nacimiento: </b>{{ $usu->fecha_nacimiento }}</h4>
				<h4 class="modal-title"><b>Fecha de registro: </b>{{ $usu->created_at }}</h4>
			</div>
			<hr/><hr/>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
	</div>


</div>