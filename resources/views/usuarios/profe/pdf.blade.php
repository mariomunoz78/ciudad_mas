<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>estudiante pdf</title>
</head>
<body>


	<div class="container">
     <div class="row row-cols-4">
          <div class="col"><b style="color: green">II Foro Ciudad más</b></div>
          <div class="col"><b>Monteria Cordoba</b></div>
          <div class="col"><b>Profesores Registrados<b></div>
       </div>
    </div><br>


	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Identidad</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Telefono</th>
					<th>Direccion</th>
					<th>Fecha Nacimiento</th>
					<th>Ciudad</th>
					<td>Correo</td>
					<br>
				</thead>
				<tbody>
               @foreach ($users as $usu)          
                   	@if($usu->tipo_persona=='profe') 
                   	<tr> 
				    <td>{{ $usu->num_documento}}</td> 
					<td>{{ $usu->name}}</td>
					<td>{{ $usu->apellido}}</td>
					<td>{{ $usu->telefono}}</td>
					<td>{{ $usu->direccion}}</td>
					<td>{{ $usu->fecha_nacimiento}}</td>
					<td>{{ $usu->ciudad}}</td>
					<td>{{ $usu->email}}</td>
				   
				@endif
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>