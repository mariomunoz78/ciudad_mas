@extends('layouts.adminsinvue')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3><b>Profesores Registrados</b></h3>
		
	</div>
</div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif

 @if(session('mario'))
            <div class="alert alert-success" role="alert">
            {{session('mario')}}
            </div>
            @endif








<div class="container">
               <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
							<th>#</th>
							<th>Identidad</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Telefono</th>
							<th>Direccion</th>
							<th>Foto</th>
							<th>Opciones</th>


								
                            </tr>
                        </thead>
						
                        <tbody>
						@foreach ($usuario as $usu)
						@if($usu->tipo_persona=='profe')

                            <tr>

							<td>{{ $usu->id}}</td>
				    <td>{{ $usu->num_documento}}</td>
					<td>{{ $usu->name}}</td>
					<td>{{ $usu->apellido}}</td>
					<td>{{ $usu->telefono}}</td>
					<td>{{ $usu->direccion}}</td>

					@if ($usu->foto!="")
					<td>
					<a href="" data-target="#modal-estado-{{$usu->id}}" data-toggle="modal"><img class="img-profile rounded-circle" data-toggle="tooltip" title="Foto ampliar" src="{{asset('/aplica/img/fotos/'.$usu->foto)}}"  width="70" height="70"></a>

					</td>
					@elseif($usu->foto=="")
					<td><a href="" data-target="#modal-estado-{{$usu->id}}" data-toggle="modal"><img class="img-profile rounded-circle" data-toggle="tooltip" title="No tiene foto" src="{{asset('/aplica/img/nofoto.png')}}"  width="70" height="70"></a></td>
					@endif

					 <td>
					 	<a class="btn btn-outline-info" href="" data-target="#modal-mario-{{$usu->id}}" data-toggle="modal" style="color:blue"><i class="far fa-eye"></i></a>
					 <a class="btn btn-outline-danger" href="{{ url('inscripcionre/delete',$usu->id) }}" style="color:red" data-toggle="tooltip" title="  estudiante"><i class="fas fa-trash-alt"></i></a>
					 
					 <a class="btn btn-outline-success" href="{{ url('inscripcionre/viewEditUser',$usu->id) }}" style="color:green" data-toggle="tooltip" title="estudiante"><i class="fas fa-edit"></i></a>
					 </td>

				</tr>
				 @include('usuarios.estudiante.detalle')
                 @include('usuarios.estudiante.modal')
                 @endif
				@endforeach
                                                      
                        </tbody>   
						  
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>  




@endsection  
<!--aqui finaliza la session-->