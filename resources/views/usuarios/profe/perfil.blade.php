@extends('layouts.estudiantehome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
 
 	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="alert alert-warning">
            <h3 class="alert-link">PERFIL:</h3>
           
      </div>
     </div>
	</div><!--cierro la columna del div-->

          <!-- agrego una fila -->
      <div class="row">

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Nombre</label>
                         <h2> {{ \Auth::user()->name }}</h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Apellido</label>
                         <h2> {{ \Auth::user()->apellido }}</h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Documento</label>
                         <h2>{{ \Auth::user()->tipo_documento }} : {{ \Auth::user()->num_documento }} </h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Ciudad</label>
                         <h2> {{ \Auth::user()->ciudad }}</h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Direccion</label>
                         <h2> {{ \Auth::user()->direccion }}</h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Telefono</label>
                         <h2> {{ \Auth::user()->telefono }}</h2>
                       </div>
                      </div>

                       <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Correo Electronico</label>
                         <h2> {{ \Auth::user()->email }}</h2>
                       </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                         <label for="name">Fecha de nacimiento</label>
                         <h2> {{ \Auth::user()->fecha_nacimiento }}</h2>
                       </div>
                      </div>

                      	{!!Form::open(array('url'=>'/inscripcion/evento','method'=>'POST','autocomplete'=>'off'))!!}
                        {{Form::token()}}

                          <div class="col-lg-6 col-md-6 col-xs-12">
                         <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                          <label for="foto">Subir foto</label>
                          <input type="file" name="foto" class="form-control">

                                    @if ($errors->has('foto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('foto') }}</strong>
                                    </span>
                                @endif  
                           </div>
                           </div>
                    </div>
          
      </div>
            <div class="col-lg-6">
            <div class="col-lg-6 col-md-6 col-xs-12">
            <div class="form-group">
            	 <a href="/inscripcion/estudiante"><button class="btn btn-warning">Atras</button></a> 
            </div>
            </div>
@endsection  
<!--aqui finaliza la session-->