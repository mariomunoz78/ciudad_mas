<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-estado-{{$usu->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="Closee" data-dismiss="modal" 
				aria-label="Close">
                     <span class="btn btn-danger" aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><b>{{ $usu->name }} {{ $usu->apellido }}</b></h4>
			</div>
			<div class="modal-body">
					@if ($usu->foto!="")
					<img  data-toggle="tooltip" title="No tiene foto" src="{{asset('/aplica/img/fotos/'.$usu->foto)}}"  width="300" height="350">
					@elseif($usu->foto=="")
					<img  data-toggle="tooltip" title="Foto ampliar" src="{{asset('/aplica/img/nofoto.png')}}"  width="300" height="350">
					@endif
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>


</div>