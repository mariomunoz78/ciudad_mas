@extends('layouts.juradohome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->

<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h4><b>Respuestas </b><br>Nueva Calificacion   <a href="/calificaciones/create"><button class="btn btn-success">click</button></a></h4>
		<div class="form-group">
       <input type="text" class="form-control pull-right" style="width:70%" id="search" placeholder="Buscar...">
    </div>
	</div>
</div>
           @if(session('mario'))
            <div class="alert alert-success" role="alert">
            {{session('mario')}}
            </div>
            @endif

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table  class="table-bordered table pull-right" id="mytable" cellspacing="0" style="width: 100%;">
				<thead class="thead-dark">
					<th>Id</th>
					<th>Estudiante</th>
					<th>Jurado</th>
					<th>Doc Jurado</th>
					<th>Ponencia</th>
					<th>Observacion</th>
					<th>Nota</th>
					<th>Opcion</th>
				</thead>
                 @foreach ($DetalleRespuesta as $det)
                   	<tr>
                   	    @if ($det->idJurado== \Auth::user()->id)
                     
				    <td>{{ $det->id}}</td>
				    <td>{{ $det->nomjurado}} {{ $det->apejurado}}</td>
					<td>{{ $det->nombre_jurado}}</td>
					<td>{{ $det->num_documento_jurado}}</td>
					<td>{{ $det->titulo_ponencia}}</td>
					<td>{{ $det->observacion}}</td>
					<td>{{ $det->total_puntuacion/16}}</td>
					<td>
						<a  href="{{URL::action('ControllerCalificacion@show',$det->id)}}"><button class="btn btn-danger">Detalles</button></a>
					</td>
					</tr>
			       @endif
				@endforeach
				 
			</table>
		</div>
		
	</div>
</div>
<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>


@endsection  
<!--aqui finaliza la session-->

