@extends('layouts.juradohome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<div class="row">
  <img data-toggle="tooltip" title="Descargar archivo EXCEL" src="{{asset('aplica/img/evaluacion.gif')}}" height="70" width="130">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Calificacion Ponencia</h3>
			@if (count($errors)>0)  <!-- si tengo mas de un error -->
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

      @if(session('mario'))
            <div class="alert alert-success" role="alert">
            {{session('mario')}}
            </div>
            @endif
            

       </div>  <!-- cerramos nuestra primera columna -->
   </div>  <!-- cerramos nuestra primera fila -->
			{!!Form::open(array('url'=>'/calificaciones','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
      <!-- agrego una fila -->
      <div class="row">

          <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
              <label for="uss">Identidad:</label>
              <input type="text" disabled class="form-control"  value="{{ Auth::user()->num_documento}}">
            
      </div>
      </div>

      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
              <label for="uss">Nombre Jurado</label>
              <input type="num" disabled class="form-control"  value="{{ Auth::user()->name}} {{ Auth::user()->apellido}}">
            
      </div>
      </div>

      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
         
         <div class="form-group">
              <label for="idEstudiante">Estudiante</label>
             <select name="Estudiante"  class="form-control selectpicker" data-live-search="true">
            @foreach($estudiante as $estudiante)
            <option value="{{ $estudiante->id}}">{{$estudiante->name}} {{$estudiante->apellido}} ( {{ $estudiante->titulo_ponencia }})</option>
            @endforeach
             </select>     
      </div>
       </div>

    </div>
<!--cierro la fiila e igualmente abro una  nueva fila  encerrar todo el detalle y los botones -->

   <div class="row">

   <div class="panel panel-primary">

    <div class="panel-body">

      <div class="col-lg-8 col-sm-2 col-md-2 col-xs-12"> 
        <div class="form-group">
          <label for="idpregunta">Criterio</label>
          <select name="idpregunta" id="idpregunta" class="form-control selectpicker" data-live-search="true">
            @foreach($preguntas as $pre)
            <option value="{{ $pre->id}}">{{$pre->id}} {{$pre->pregunta}} </option>
          @endforeach
          </select>     
    </div>
    </div>


 <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
         <div class="form-group">
          <label>Puntuacion</label>
          <select name="puntuacion" id="puntuacion" class="form-control">
            <option value="1">UNO</option>
            <option value="2">DOS</option>
            <option value="3">TRES</option>
            <option value="4">CUATRO</option>
            <option value="5">CINCO</option>
          </select>
 
         </div>
      </div>


<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
<div class="form-group">
<label for="cantidad">Calificar: </label> <br> 
<button type="button" id="bt_add" class="btn btn-primary">Presione</button> 
</div>
</div>

<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12"></div>
<table  id="detalles" class="table table-striped table-bordered table-condensed table-hover">
  <thead style="background-color:#A9D0F5">
    <th>Eliminar</th>
    <th>Criterio</th>
    <th>Puntuacion</th>

  </thead>
  <!--pie de la tabala-->
   <tfoot>
     
    <th>Puntos Total</th>
    <th></th>
    <th> <h4 id="total" style="color: black" ><b>#. 0</b></h4> <input type="hidden"  value="1"> </th>
    </tfoot>
  
     <h4  style="color: black">Nota: <b id="nota">0</b></h4>
   <tbody>
   </tbody>
</table>
 
</div>
</div>





            <div class="col-lg-6 col-md-6 col-xs-12" id="guardar">
            <div class="form-group">

              <div class="form-group">
              <label for="uss">Observacion  de la ponencia</label>
              <textarea type="text" name="observacion"  class="form-control" placeholder="Observacion de la ponencia"></textarea>
              </div>

              <input type="hidden" name="_token" value="{{ csrf_token() }}">              
              <input type="hidden" name="idjuradoo" value="{{ Auth::user()->id}}">
              <input type="hidden" name="nombre_jurado" value="{{ Auth::user()->name}}">
              <input type="hidden" name="num_documento_jurado" value="{{ Auth::user()->num_documento}}">
              
              <button class="btn btn-primary" type="submit">Guardar Calificacion</button>
              <button class="btn btn-danger" type="reset">Cancelar</button>

            </div>
             </div>


      {!!Form::close()!!}   
        
  

<script>

//evalua el evento click del boton agregar que tiene un id llamado bt_add en el cual toda la informacion lo manda al array del java scri`pt
  $(document).ready(function(){
    
    $('#bt_add').click(function(){
   
    agregar();
    });
     evaluar();
    });
  var cont=0;
  var validarNum=0;
    total=0;
    nota=0;
  //agrego un array que esta vacio capturar todos los subtotales
  subtotal=[];


  $("#guardar").hide();
//codigo jquery que cuando se modifique que me muestre sus valores
  $("#pidarticulo").change(mostrarValores);


//funcion mostrar valores

function mostrarValores()
{
  datosArticulo=document.getElementById('pidarticulo').value.split('_'); // voy a obtener los valores pidarticulo y voy a separar los valores con un value split
  $("#pprecio_venta").val(datosArticulo[2]);
  $("#pstock").val(datosArticulo[1]);  

}
//voy a leer todos los datos que tengo 
function agregar(){

if(validarNum<=15){

// tomo el valor que esta en el id
idpregunta=$("#idpregunta").val();
nombrepregunta=$("#idpregunta option:selected").text();
puntuacion=$("#puntuacion").val();

//valido si idarticulo es diferente a vacio
if (idpregunta!=""  && puntuacion!="") {
parsearpuntuacion=parseInt (puntuacion);
subtotal[cont]=parsearpuntuacion;
total=(total+subtotal[cont]);


//agregoo una fila a mi tabla
var fila='<tr class="selected" id="fila'+cont+'"> <td> <button type="button" class="btn btn-danger"  onClick="eliminar('+cont+'); ">X</td>   <td><input type="hidden" name="idpregunta[]" value="'+idpregunta+'" > '+nombrepregunta+' </td>   <td><input type="number" min="1"    max="5" step="1"  name="puntu[]"  value="'+parsearpuntuacion+'"></td>        <td><input type="hidden"   name="subtotal[]" value="'+subtotal[cont]+'" ></td>   <input type="hidden"  name="totalpuntuacion" value="'+total+'" >  </tr>';

cont++;
validarNum++;

console.log(cont);
nota=total/16;
if(validarNum==16){
$("#nota").html(". "+nota);
}
//despues de oasar todo a la fila llamo a mi funcion limpiar para dejar las cajas vacias
limpiar();
$("#total").html("S/. "+total);
//paso al id total conhtml la variable total
//despues llamo a la funcion evaluar par que me muestre los detalles
evaluar();
//id de l tabla agrega la fila que esta aqui
$("#detalles").append(fila);
//hace la operacion para mostrar la nota final


}else{
  alert("Error al ingresar el detalle del ingreso, revisar los datos de la venta");
}

}else{
   swal("{{ Auth::user()->name }} {{ Auth::user()->apellido }} numero de criterio evaluado terminado");
}

}
//funcion que me permita limpiar la caja de texto
function limpiar(){
$("#pcantidad").val("");
$("#pdescuento").val("");
$("#pprecio_venta").val("");

}

// declaro una funcion para esconder los botones de guardar si no tengo nada escrito en el formulario

function evaluar(){
  //si el total es mayor a 0 muestro los botones
  if (validarNum==16) {
    $("#guardar").show();
  }
  else{
    //si no guardar lo escondo
    $("#guardar").hide();

  }
}

function eliminar(index){
  total=total-subtotal[index];
  $("#total").html("S/. "+total);

   $("#total_venta").val(total);
 validarNum=validarNum-1;
 console.log('pruebavalidar'+validarNum);
  $("#fila"+index).remove();
  evaluar();
}

</script>



<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
   

@endsection  