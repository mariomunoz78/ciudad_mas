@extends('layouts.juradohome')  <!-- esta plantilla la voy a extender-->
@section ('contenido') <!--este contenido se va a mostrar en el lay que esta en admin -->
<div class="row">  <!--agregamos una fila -->
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">  <!-- colomna cuado el dispositivo seal grande tenga 8 columna aho de 8 y cuando el dispositovo sea pequeño que me muestre todo la pantalla completa-->
		<h3><b>Detalle de Calificacion de </b></h3>
		

	</div>
</div>

      <!-- agrego una fila -->
      <div class="row"> 
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
         
         <div class="form-group">
              <label for="proveedor"><b>Nombre Estudiante</b></label>
           <p>{{ $respuesta->nomestudiante}} {{ $respuesta->apeestudiante}}</p>  
      </div>
       </div>

       <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
         
         <div class="form-group">
              <label for="proveedor"><b>Titulo de la ponencia</b></label>
           <p>{{ $respuesta->titulo_ponencia}}</p>  
      </div>
       </div>


   
    
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
         <div class="form-group">
          <label><b>Jurado</b></label>
             <p>{{ $respuesta->nombre_jurado}} <b>  CC: </b>{{ $respuesta->num_documento_jurado}}</p>  
         </div>
      </div>


      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
              <label for="serie_comprobante"><b>Puntaje</b></label>
                <p>{{ $respuesta->total_puntuacion}}</p>      
      </div>
      </div>



     <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
              <label ><b>Nota</b></label>
                 <p>{{ $respuesta->total_puntuacion/16}}</p> 
            
      </div>
      </div>

       <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
              <label for="observacion"><b>Observacion</b></label>
                 <p>{{ $respuesta->observacion}}</p> 
            
      </div>
      </div>



    </div>

@if(session('exitoso'))
<div class="alert alert-success" role="alert">
  {{session('exitoso')}}
</div>
@endif


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="thead-dark" style="border-collapse: collapse;" border="1"; width="420">
				<thead >
			<!--	<th>Id</th>
					<th>idrespuesta</th>  -->
					<th>Pregunta</th>
					<th>Puntaje</th>
					<th>Criterio</th>
					<th>Fecha</th>
				</thead>
               @foreach ($detalleRespuesta as $det)
				
                   	<tr> 
				    
					<td>{{ $det->pregunta}}</td>
					<td>{{ $det->puntaje}}</td>
					 @if ($det->puntaje==1)
					<td style="background: red"><b style="color: black">No cumple</b></td>
					 @endif
					 @if ($det->puntaje==2)
					<td style="background: orange"><b style="color: black">Insuficiente</b></td>
					 @endif
					 @if ($det->puntaje==3)
					<td style="background: yellow"><b style="color: black">Minimo</b></td>
					 @endif
					 @if ($det->puntaje==4)
					<td style="background: blue"><b style="color: black">Destacado</b></td>
					 @endif
					 @if ($det->puntaje==5)
					<td style="background: green"><b style="color: black">Excelente</b></td>
					 @endif
					<td>{{ $det->created_at}}</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@endsection  
<!--aqui finaliza la session-->