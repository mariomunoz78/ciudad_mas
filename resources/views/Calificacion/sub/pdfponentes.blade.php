
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ponentes</title>
</head>
<body>


	<div class="container">
     <div class="row row-cols-4">
          <div class="col"><b style="color: green">II Foro Ciudad más  Ponentes</b></div>
          <div class="col"><b>Monteria Cordoba  Ponentes</b></div>
          <div class="col"><b>Ponentes<b></div>
       </div>
    </div><br>


	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					
					
				</thead>
				<tbody>
                  

				 @foreach ($ponente as $pon)
				   	@if($pon->categoria=='Ponente') 
                   	<tr> 
				    <td>{{ $pon->doc_identidad}}</td>
					<td>{{ $pon->nombre}}</td>
					<td>{{ $pon->apellido}}</td>
					<td>{{ $pon->titulo_ponencia}}</td>
					<td>{{ $pon->institucion_educatica}}</td>
					<td>{{ $pon->ciudad}}</td>

					@endif
					</tr>
				 
                
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>