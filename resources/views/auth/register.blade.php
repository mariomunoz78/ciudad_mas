<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registrate | Monteria</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('aplica/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('aplica/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
           <div> <img src="{{asset('aplica/img/escudo.png')}}" height="400" width="300"> </div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><b style="color:blue">Inscribete!</b></h1>
              </div>
              
                 <form class="user" method="POST" action="{{ route('register') }}">
                        @csrf

                <div class="form-group row">
                   <div class="col-md-6">
                    <label for="name" >Nombre</label>
                                <input id="name" type="text" class="form-control form-control-user{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  autofocus placeholder="Nombre...">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>


                  <div class="col-sm-6">
                    <label>Apellido</label>
                    <input id="apellido" type="text" class="form-control form-control-user{{ $errors->has('apellido') ? ' is-invalid' : '' }}" name="apellido" value="{{ old('apellido') }}"  autofocus placeholder="Apellido...">

                    @if ($errors->has('apellido'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                @endif
                  </div>
                </div>


                <div class="form-group row">

                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <label>Email</label>
                      <input id="email" type="email" class="form-control form-control-user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus placeholder="Email...">
                         @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                  </div>

                   <div class="col-sm-6">
                    <label>Telefono</label>
                    <input id="telefono" type="number" class="form-control form-control-user{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}"  autofocus placeholder="Telefono...">
                    @if ($errors->has('telefono'))
                            <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                      @endif
              </div>

           
            </div>
            
               <div class="form-group row">

                    <div class="col-sm-6">
            <label>Tipo de persona</label>
            <select name="tipo_persona" class="form-control-user" required>
           
            <option value="estudiante">ESTUDIANTE</option>
            <option value="padre">PADRE DE FAMILIA</option>
            <option value="profe">PROFESORES</option>
            </select>
            </div>

               <div class="col-sm-6">
            <label>Tipo de documento</label>
            <select name="tipo_documento" class="form-control-user" required>
            <option value="cedula">CEDULA</option>
            <option value="tarjeta de identidad">TARJETA DE IDENTIDAD</option>
            
            </select>
            </div>

              </div>

            <div class="form-group row">
              <div class="col-sm-6">
                    <label>Numero de Documento</label>
                    <input name="num_documento" type="number" class="form-control form-control-user{{ $errors->has('num_documento') ? ' is-invalid' : '' }}" name="num_documento" value="{{ old('num_documento') }}"  autofocus placeholder="Numero de Identidad...">

                    @if ($errors->has('num_documento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('num_documento') }}</strong>
                                    </span>
                             @endif
              </div>

                <div class="col-sm-6">
                    <label>Fecha de Nacimiento</label>
                    <input id="fecha_nacimiento" type="date" class="form-control form-control-user{{ $errors->has('fecha_nacimiento') ? ' is-invalid' : '' }}" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}"  autofocus >
                    

                    @if ($errors->has('fecha_nacimiento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                    </span>
                             @endif
              </div>
              </div>

               <div class="form-group row">
              <div class="col-sm-6">
                    <label>Ciudad</label>
                     <input id="ciudad" type="text" class="form-control form-control-user{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="ciudad" value="{{ old('ciudad') }}"  autofocus placeholder="Ciudad...">

                    @if ($errors->has('ciudad'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ciudad') }}</strong>
                                    </span>
                             @endif
              </div>

                <div class="col-sm-6">
                    <label>Dirección</label>
                    <input id="direccion" type="text" class="form-control form-control-user{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}"  autofocus placeholder="Dirección...">

                    @if ($errors->has('direccion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                             @endif
              </div>
              </div>


               <div class="form-group row">
              <div class="col-sm-6">
                    <label>Password</label>
                   <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password...">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              </div>

                <div class="col-sm-6">
                    <label>Confirmar su password</label>
                   <input id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" required placeholder="Repita su Password">
              </div>
              </div>

              <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Register') }}
              </button>

                <hr>
                

              </form>
             
              <div class="text-center">
                    <a class="text-secondary" href="{{url('/olvidocuenta')}}"><b>¿Se te olvidó tu contraseña?</b></a>
                  </div>
                  <div class="text-center">
                    <a class="text-secondary" href="{{url('/nueva/cuenta')}}"><b>¡Crea una cuenta!</b></a>
                  </div>
                  <div class="text-center">
                    <a class="text-secondary" href="{{url('index')}}"><b>¡Ya tengo cuenta!</b></a>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('aplica/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('aplica/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('aplica/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('aplica/js/sb-admin-2.min.js')}}"></script>

</body>

</html>
